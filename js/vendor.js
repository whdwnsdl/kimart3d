/*! Shapespark - v1.0.0 - 2020-12-21
 * https://www.shapespark.com/
 * Copyright (c) 2020; */
! function (a, b) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = a.document ? b(a, !0) : function (a) {
        if (!a.document) throw new Error("jQuery requires a window with a document");
        return b(a)
    } : b(a)
}("undefined" != typeof window ? window : this, function (a, b) {
    function c(a) {
        var b = "length" in a && a.length,
            c = ea.type(a);
        return "function" !== c && !ea.isWindow(a) && (!(1 !== a.nodeType || !b) || ("array" === c || 0 === b || "number" == typeof b && b > 0 && b - 1 in a))
    }

    function d(a, b, c) {
        if (ea.isFunction(b)) return ea.grep(a, function (a, d) {
            return !!b.call(a, d, a) !== c
        });
        if (b.nodeType) return ea.grep(a, function (a) {
            return a === b !== c
        });
        if ("string" == typeof b) {
            if (ma.test(b)) return ea.filter(b, a, c);
            b = ea.filter(b, a)
        }
        return ea.grep(a, function (a) {
            return ea.inArray(a, b) >= 0 !== c
        })
    }

    function e(a, b) {
        do {
            a = a[b]
        } while (a && 1 !== a.nodeType);
        return a
    }

    function f(a) {
        var b = ta[a] = {};
        return ea.each(a.match(sa) || [], function (a, c) {
            b[c] = !0
        }), b
    }

    function g() {
        oa.addEventListener ? (oa.removeEventListener("DOMContentLoaded", h, !1), a.removeEventListener("load", h, !1)) : (oa.detachEvent("onreadystatechange", h), a.detachEvent("onload", h))
    }

    function h() {
        (oa.addEventListener || "load" === event.type || "complete" === oa.readyState) && (g(), ea.ready())
    }

    function i(a, b, c) {
        if (void 0 === c && 1 === a.nodeType) {
            var d = "data-" + b.replace(ya, "-$1").toLowerCase();
            if ("string" == typeof (c = a.getAttribute(d))) {
                try {
                    c = "true" === c || "false" !== c && ("null" === c ? null : +c + "" === c ? +c : xa.test(c) ? ea.parseJSON(c) : c)
                } catch (e) {}
                ea.data(a, b, c)
            } else c = void 0
        }
        return c
    }

    function j(a) {
        var b;
        for (b in a)
            if (("data" !== b || !ea.isEmptyObject(a[b])) && "toJSON" !== b) return !1;
        return !0
    }

    function k(a, b, c, d) {
        if (ea.acceptData(a)) {
            var e, f, g = ea.expando,
                h = a.nodeType,
                i = h ? ea.cache : a,
                j = h ? a[g] : a[g] && g;
            if (j && i[j] && (d || i[j].data) || void 0 !== c || "string" != typeof b) return j || (j = h ? a[g] = W.pop() || ea.guid++ : g), i[j] || (i[j] = h ? {} : {
                toJSON: ea.noop
            }), "object" != typeof b && "function" != typeof b || (d ? i[j] = ea.extend(i[j], b) : i[j].data = ea.extend(i[j].data, b)), f = i[j], d || (f.data || (f.data = {}), f = f.data), void 0 !== c && (f[ea.camelCase(b)] = c), "string" == typeof b ? null == (e = f[b]) && (e = f[ea.camelCase(b)]) : e = f, e
        }
    }

    function l(a, b, c) {
        if (ea.acceptData(a)) {
            var d, e, f = a.nodeType,
                g = f ? ea.cache : a,
                h = f ? a[ea.expando] : ea.expando;
            if (g[h]) {
                if (b && (d = c ? g[h] : g[h].data)) {
                    ea.isArray(b) ? b = b.concat(ea.map(b, ea.camelCase)) : b in d ? b = [b] : (b = ea.camelCase(b), b = b in d ? [b] : b.split(" ")), e = b.length;
                    for (; e--;) delete d[b[e]];
                    if (c ? !j(d) : !ea.isEmptyObject(d)) return
                }(c || (delete g[h].data, j(g[h]))) && (f ? ea.cleanData([a], !0) : ca.deleteExpando || g != g.window ? delete g[h] : g[h] = null)
            }
        }
    }

    function m() {
        return !0
    }

    function n() {
        return !1
    }

    function o() {
        try {
            return oa.activeElement
        } catch (a) {}
    }

    function p(a) {
        var b = Ja.split("|"),
            c = a.createDocumentFragment();
        if (c.createElement)
            for (; b.length;) c.createElement(b.pop());
        return c
    }

    function q(a, b) {
        var c, d, e = 0,
            f = typeof a.getElementsByTagName !== wa ? a.getElementsByTagName(b || "*") : typeof a.querySelectorAll !== wa ? a.querySelectorAll(b || "*") : void 0;
        if (!f)
            for (f = [], c = a.childNodes || a; null != (d = c[e]); e++)!b || ea.nodeName(d, b) ? f.push(d) : ea.merge(f, q(d, b));
        return void 0 === b || b && ea.nodeName(a, b) ? ea.merge([a], f) : f
    }

    function r(a) {
        Da.test(a.type) && (a.defaultChecked = a.checked)
    }

    function s(a, b) {
        return ea.nodeName(a, "table") && ea.nodeName(11 !== b.nodeType ? b : b.firstChild, "tr") ? a.getElementsByTagName("tbody")[0] || a.appendChild(a.ownerDocument.createElement("tbody")) : a
    }

    function t(a) {
        return a.type = (null !== ea.find.attr(a, "type")) + "/" + a.type, a
    }

    function u(a) {
        var b = Ua.exec(a.type);
        return b ? a.type = b[1] : a.removeAttribute("type"), a
    }

    function v(a, b) {
        for (var c, d = 0; null != (c = a[d]); d++) ea._data(c, "globalEval", !b || ea._data(b[d], "globalEval"))
    }

    function w(a, b) {
        if (1 === b.nodeType && ea.hasData(a)) {
            var c, d, e, f = ea._data(a),
                g = ea._data(b, f),
                h = f.events;
            if (h) {
                delete g.handle, g.events = {};
                for (c in h)
                    for (d = 0, e = h[c].length; d < e; d++) ea.event.add(b, c, h[c][d])
            }
            g.data && (g.data = ea.extend({}, g.data))
        }
    }

    function x(a, b) {
        var c, d, e;
        if (1 === b.nodeType) {
            if (c = b.nodeName.toLowerCase(), !ca.noCloneEvent && b[ea.expando]) {
                e = ea._data(b);
                for (d in e.events) ea.removeEvent(b, d, e.handle);
                b.removeAttribute(ea.expando)
            }
            "script" === c && b.text !== a.text ? (t(b).text = a.text, u(b)) : "object" === c ? (b.parentNode && (b.outerHTML = a.outerHTML), ca.html5Clone && a.innerHTML && !ea.trim(b.innerHTML) && (b.innerHTML = a.innerHTML)) : "input" === c && Da.test(a.type) ? (b.defaultChecked = b.checked = a.checked, b.value !== a.value && (b.value = a.value)) : "option" === c ? b.defaultSelected = b.selected = a.defaultSelected : "input" !== c && "textarea" !== c || (b.defaultValue = a.defaultValue)
        }
    }

    function y(b, c) {
        var d, e = ea(c.createElement(b)).appendTo(c.body),
            f = a.getDefaultComputedStyle && (d = a.getDefaultComputedStyle(e[0])) ? d.display : ea.css(e[0], "display");
        return e.detach(), f
    }

    function z(a) {
        var b = oa,
            c = $a[a];
        return c || (c = y(a, b), "none" !== c && c || (Za = (Za || ea("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement), b = (Za[0].contentWindow || Za[0].contentDocument).document, b.write(), b.close(), c = y(a, b), Za.detach()), $a[a] = c), c
    }

    function A(a, b) {
        return {
            get: function () {
                var c = a();
                if (null != c) return c ? void delete this.get : (this.get = b).apply(this, arguments)
            }
        }
    }

    function B(a, b) {
        if (b in a) return b;
        for (var c = b.charAt(0).toUpperCase() + b.slice(1), d = b, e = lb.length; e--;)
            if ((b = lb[e] + c) in a) return b;
        return d
    }

    function C(a, b) {
        for (var c, d, e, f = [], g = 0, h = a.length; g < h; g++) d = a[g], d.style && (f[g] = ea._data(d, "olddisplay"), c = d.style.display, b ? (f[g] || "none" !== c || (d.style.display = ""), "" === d.style.display && Ba(d) && (f[g] = ea._data(d, "olddisplay", z(d.nodeName)))) : (e = Ba(d), (c && "none" !== c || !e) && ea._data(d, "olddisplay", e ? c : ea.css(d, "display"))));
        for (g = 0; g < h; g++) d = a[g], d.style && (b && "none" !== d.style.display && "" !== d.style.display || (d.style.display = b ? f[g] || "" : "none"));
        return a
    }

    function D(a, b, c) {
        var d = hb.exec(b);
        return d ? Math.max(0, d[1] - (c || 0)) + (d[2] || "px") : b
    }

    function E(a, b, c, d, e) {
        for (var f = c === (d ? "border" : "content") ? 4 : "width" === b ? 1 : 0, g = 0; f < 4; f += 2) "margin" === c && (g += ea.css(a, c + Aa[f], !0, e)), d ? ("content" === c && (g -= ea.css(a, "padding" + Aa[f], !0, e)), "margin" !== c && (g -= ea.css(a, "border" + Aa[f] + "Width", !0, e))) : (g += ea.css(a, "padding" + Aa[f], !0, e), "padding" !== c && (g += ea.css(a, "border" + Aa[f] + "Width", !0, e)));
        return g
    }

    function F(a, b, c) {
        var d = !0,
            e = "width" === b ? a.offsetWidth : a.offsetHeight,
            f = _a(a),
            g = ca.boxSizing && "border-box" === ea.css(a, "boxSizing", !1, f);
        if (e <= 0 || null == e) {
            if (e = ab(a, b, f), (e < 0 || null == e) && (e = a.style[b]), cb.test(e)) return e;
            d = g && (ca.boxSizingReliable() || e === a.style[b]), e = parseFloat(e) || 0
        }
        return e + E(a, b, c || (g ? "border" : "content"), d, f) + "px"
    }

    function G(a, b, c, d, e) {
        return new G.prototype.init(a, b, c, d, e)
    }

    function H() {
        return setTimeout(function () {
            mb = void 0
        }), mb = ea.now()
    }

    function I(a, b) {
        var c, d = {
                height: a
            },
            e = 0;
        for (b = b ? 1 : 0; e < 4; e += 2 - b) c = Aa[e], d["margin" + c] = d["padding" + c] = a;
        return b && (d.opacity = d.width = a), d
    }

    function J(a, b, c) {
        for (var d, e = (sb[b] || []).concat(sb["*"]), f = 0, g = e.length; f < g; f++)
            if (d = e[f].call(c, b, a)) return d
    }

    function K(a, b, c) {
        var d, e, f, g, h, i, j, k = this,
            l = {},
            m = a.style,
            n = a.nodeType && Ba(a),
            o = ea._data(a, "fxshow");
        c.queue || (h = ea._queueHooks(a, "fx"), null == h.unqueued && (h.unqueued = 0, i = h.empty.fire, h.empty.fire = function () {
            h.unqueued || i()
        }), h.unqueued++, k.always(function () {
            k.always(function () {
                h.unqueued--, ea.queue(a, "fx").length || h.empty.fire()
            })
        })), 1 === a.nodeType && ("height" in b || "width" in b) && (c.overflow = [m.overflow, m.overflowX, m.overflowY], j = ea.css(a, "display"), "inline" === ("none" === j ? ea._data(a, "olddisplay") || z(a.nodeName) : j) && "none" === ea.css(a, "float") && (ca.inlineBlockNeedsLayout && "inline" !== z(a.nodeName) ? m.zoom = 1 : m.display = "inline-block")), c.overflow && (m.overflow = "hidden", ca.shrinkWrapBlocks() || k.always(function () {
            m.overflow = c.overflow[0], m.overflowX = c.overflow[1], m.overflowY = c.overflow[2]
        }));
        for (d in b)
            if (e = b[d], ob.exec(e)) {
                if (delete b[d], f = f || "toggle" === e, e === (n ? "hide" : "show")) {
                    if ("show" !== e || !o || void 0 === o[d]) continue;
                    n = !0
                }
                l[d] = o && o[d] || ea.style(a, d)
            } else j = void 0;
        if (ea.isEmptyObject(l)) "inline" === ("none" === j ? z(a.nodeName) : j) && (m.display = j);
        else {
            o ? "hidden" in o && (n = o.hidden) : o = ea._data(a, "fxshow", {}), f && (o.hidden = !n), n ? ea(a).show() : k.done(function () {
                ea(a).hide()
            }), k.done(function () {
                var b;
                ea._removeData(a, "fxshow");
                for (b in l) ea.style(a, b, l[b])
            });
            for (d in l) g = J(n ? o[d] : 0, d, k), d in o || (o[d] = g.start, n && (g.end = g.start, g.start = "width" === d || "height" === d ? 1 : 0))
        }
    }

    function L(a, b) {
        var c, d, e, f, g;
        for (c in a)
            if (d = ea.camelCase(c), e = b[d], f = a[c], ea.isArray(f) && (e = f[1], f = a[c] = f[0]), c !== d && (a[d] = f, delete a[c]), (g = ea.cssHooks[d]) && "expand" in g) {
                f = g.expand(f), delete a[d];
                for (c in f) c in a || (a[c] = f[c], b[c] = e)
            } else b[d] = e
    }

    function M(a, b, c) {
        var d, e, f = 0,
            g = rb.length,
            h = ea.Deferred().always(function () {
                delete i.elem
            }),
            i = function () {
                if (e) return !1;
                for (var b = mb || H(), c = Math.max(0, j.startTime + j.duration - b), d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length; g < i; g++) j.tweens[g].run(f);
                return h.notifyWith(a, [j, f, c]), f < 1 && i ? c : (h.resolveWith(a, [j]), !1)
            },
            j = h.promise({
                elem: a,
                props: ea.extend({}, b),
                opts: ea.extend(!0, {
                    specialEasing: {}
                }, c),
                originalProperties: b,
                originalOptions: c,
                startTime: mb || H(),
                duration: c.duration,
                tweens: [],
                createTween: function (b, c) {
                    var d = ea.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing);
                    return j.tweens.push(d), d
                },
                stop: function (b) {
                    var c = 0,
                        d = b ? j.tweens.length : 0;
                    if (e) return this;
                    for (e = !0; c < d; c++) j.tweens[c].run(1);
                    return b ? h.resolveWith(a, [j, b]) : h.rejectWith(a, [j, b]), this
                }
            }),
            k = j.props;
        for (L(k, j.opts.specialEasing); f < g; f++)
            if (d = rb[f].call(j, a, k, j.opts)) return d;
        return ea.map(k, J, j), ea.isFunction(j.opts.start) && j.opts.start.call(a, j), ea.fx.timer(ea.extend(i, {
            elem: a,
            anim: j,
            queue: j.opts.queue
        })), j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always)
    }

    function N(a) {
        return function (b, c) {
            "string" != typeof b && (c = b, b = "*");
            var d, e = 0,
                f = b.toLowerCase().match(sa) || [];
            if (ea.isFunction(c))
                for (; d = f[e++];) "+" === d.charAt(0) ? (d = d.slice(1) || "*", (a[d] = a[d] || []).unshift(c)) : (a[d] = a[d] || []).push(c)
        }
    }

    function O(a, b, c, d) {
        function e(h) {
            var i;
            return f[h] = !0, ea.each(a[h] || [], function (a, h) {
                var j = h(b, c, d);
                return "string" != typeof j || g || f[j] ? g ? !(i = j) : void 0 : (b.dataTypes.unshift(j), e(j), !1)
            }), i
        }
        var f = {},
            g = a === Qb;
        return e(b.dataTypes[0]) || !f["*"] && e("*")
    }

    function P(a, b) {
        var c, d, e = ea.ajaxSettings.flatOptions || {};
        for (d in b) void 0 !== b[d] && ((e[d] ? a : c || (c = {}))[d] = b[d]);
        return c && ea.extend(!0, a, c), a
    }

    function Q(a, b, c) {
        for (var d, e, f, g, h = a.contents, i = a.dataTypes;
             "*" === i[0];) i.shift(), void 0 === e && (e = a.mimeType || b.getResponseHeader("Content-Type"));
        if (e)
            for (g in h)
                if (h[g] && h[g].test(e)) {
                    i.unshift(g);
                    break
                }
        if (i[0] in c) f = i[0];
        else {
            for (g in c) {
                if (!i[0] || a.converters[g + " " + i[0]]) {
                    f = g;
                    break
                }
                d || (d = g)
            }
            f = f || d
        } if (f) return f !== i[0] && i.unshift(f), c[f]
    }

    function R(a, b, c, d) {
        var e, f, g, h, i, j = {},
            k = a.dataTypes.slice();
        if (k[1])
            for (g in a.converters) j[g.toLowerCase()] = a.converters[g];
        for (f = k.shift(); f;)
            if (a.responseFields[f] && (c[a.responseFields[f]] = b), !i && d && a.dataFilter && (b = a.dataFilter(b, a.dataType)), i = f, f = k.shift())
                if ("*" === f) f = i;
                else if ("*" !== i && i !== f) {
                    if (!(g = j[i + " " + f] || j["* " + f]))
                        for (e in j)
                            if (h = e.split(" "), h[1] === f && (g = j[i + " " + h[0]] || j["* " + h[0]])) {
                                !0 === g ? g = j[e] : !0 !== j[e] && (f = h[0], k.unshift(h[1]));
                                break
                            }
                    if (!0 !== g)
                        if (g && a.throws) b = g(b);
                        else try {
                            b = g(b)
                        } catch (l) {
                            return {
                                state: "parsererror",
                                error: g ? l : "No conversion from " + i + " to " + f
                            }
                        }
                }
        return {
            state: "success",
            data: b
        }
    }

    function S(a, b, c, d) {
        var e;
        if (ea.isArray(b)) ea.each(b, function (b, e) {
            c || Tb.test(a) ? d(a, e) : S(a + "[" + ("object" == typeof e ? b : "") + "]", e, c, d)
        });
        else if (c || "object" !== ea.type(b)) d(a, b);
        else
            for (e in b) S(a + "[" + e + "]", b[e], c, d)
    }

    function T() {
        try {
            return new a.XMLHttpRequest
        } catch (b) {}
    }

    function U() {
        try {
            return new a.ActiveXObject("Microsoft.XMLHTTP")
        } catch (b) {}
    }

    function V(a) {
        return ea.isWindow(a) ? a : 9 === a.nodeType && (a.defaultView || a.parentWindow)
    }
    var W = [],
        X = W.slice,
        Y = W.concat,
        Z = W.push,
        $ = W.indexOf,
        _ = {},
        aa = _.toString,
        ba = _.hasOwnProperty,
        ca = {},
        da = "1.11.3",
        ea = function (a, b) {
            return new ea.fn.init(a, b)
        },
        fa = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        ga = /^-ms-/,
        ha = /-([\da-z])/gi,
        ia = function (a, b) {
            return b.toUpperCase()
        };
    ea.fn = ea.prototype = {
        jquery: da,
        constructor: ea,
        selector: "",
        length: 0,
        toArray: function () {
            return X.call(this)
        },
        get: function (a) {
            return null != a ? a < 0 ? this[a + this.length] : this[a] : X.call(this)
        },
        pushStack: function (a) {
            var b = ea.merge(this.constructor(), a);
            return b.prevObject = this, b.context = this.context, b
        },
        each: function (a, b) {
            return ea.each(this, a, b)
        },
        map: function (a) {
            return this.pushStack(ea.map(this, function (b, c) {
                return a.call(b, c, b)
            }))
        },
        slice: function () {
            return this.pushStack(X.apply(this, arguments))
        },
        first: function () {
            return this.eq(0)
        },
        last: function () {
            return this.eq(-1)
        },
        eq: function (a) {
            var b = this.length,
                c = +a + (a < 0 ? b : 0);
            return this.pushStack(c >= 0 && c < b ? [this[c]] : [])
        },
        end: function () {
            return this.prevObject || this.constructor(null)
        },
        push: Z,
        sort: W.sort,
        splice: W.splice
    }, ea.extend = ea.fn.extend = function () {
        var a, b, c, d, e, f, g = arguments[0] || {},
            h = 1,
            i = arguments.length,
            j = !1;
        for ("boolean" == typeof g && (j = g, g = arguments[h] || {}, h++), "object" == typeof g || ea.isFunction(g) || (g = {}), h === i && (g = this, h--); h < i; h++)
            if (null != (e = arguments[h]))
                for (d in e) a = g[d], c = e[d], g !== c && (j && c && (ea.isPlainObject(c) || (b = ea.isArray(c))) ? (b ? (b = !1, f = a && ea.isArray(a) ? a : []) : f = a && ea.isPlainObject(a) ? a : {}, g[d] = ea.extend(j, f, c)) : void 0 !== c && (g[d] = c));
        return g
    }, ea.extend({
        expando: "jQuery" + (da + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function (a) {
            throw new Error(a)
        },
        noop: function () {},
        isFunction: function (a) {
            return "function" === ea.type(a)
        },
        isArray: Array.isArray || function (a) {
            return "array" === ea.type(a)
        },
        isWindow: function (a) {
            return null != a && a == a.window
        },
        isNumeric: function (a) {
            return !ea.isArray(a) && a - parseFloat(a) + 1 >= 0
        },
        isEmptyObject: function (a) {
            var b;
            for (b in a) return !1;
            return !0
        },
        isPlainObject: function (a) {
            var b;
            if (!a || "object" !== ea.type(a) || a.nodeType || ea.isWindow(a)) return !1;
            try {
                if (a.constructor && !ba.call(a, "constructor") && !ba.call(a.constructor.prototype, "isPrototypeOf")) return !1
            } catch (c) {
                return !1
            }
            if (ca.ownLast)
                for (b in a) return ba.call(a, b);
            for (b in a);
            return void 0 === b || ba.call(a, b)
        },
        type: function (a) {
            return null == a ? a + "" : "object" == typeof a || "function" == typeof a ? _[aa.call(a)] || "object" : typeof a
        },
        globalEval: function (b) {
            b && ea.trim(b) && (a.execScript || function (b) {
                a.eval.call(a, b)
            })(b)
        },
        camelCase: function (a) {
            return a.replace(ga, "ms-").replace(ha, ia)
        },
        nodeName: function (a, b) {
            return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase()
        },
        each: function (a, b, d) {
            var e = 0,
                f = a.length,
                g = c(a);
            if (d) {
                if (g)
                    for (; e < f && !1 !== b.apply(a[e], d); e++);
                else
                    for (e in a)
                        if (!1 === b.apply(a[e], d)) break
            } else if (g)
                for (; e < f && !1 !== b.call(a[e], e, a[e]); e++);
            else
                for (e in a)
                    if (!1 === b.call(a[e], e, a[e])) break; return a
        },
        trim: function (a) {
            return null == a ? "" : (a + "").replace(fa, "")
        },
        makeArray: function (a, b) {
            var d = b || [];
            return null != a && (c(Object(a)) ? ea.merge(d, "string" == typeof a ? [a] : a) : Z.call(d, a)), d
        },
        inArray: function (a, b, c) {
            var d;
            if (b) {
                if ($) return $.call(b, a, c);
                for (d = b.length, c = c ? c < 0 ? Math.max(0, d + c) : c : 0; c < d; c++)
                    if (c in b && b[c] === a) return c
            }
            return -1
        },
        merge: function (a, b) {
            for (var c = +b.length, d = 0, e = a.length; d < c;) a[e++] = b[d++];
            if (c !== c)
                for (; void 0 !== b[d];) a[e++] = b[d++];
            return a.length = e, a
        },
        grep: function (a, b, c) {
            for (var d = [], e = 0, f = a.length, g = !c; e < f; e++)!b(a[e], e) !== g && d.push(a[e]);
            return d
        },
        map: function (a, b, d) {
            var e, f = 0,
                g = a.length,
                h = c(a),
                i = [];
            if (h)
                for (; f < g; f++) null != (e = b(a[f], f, d)) && i.push(e);
            else
                for (f in a) null != (e = b(a[f], f, d)) && i.push(e);
            return Y.apply([], i)
        },
        guid: 1,
        proxy: function (a, b) {
            var c, d, e;
            if ("string" == typeof b && (e = a[b], b = a, a = e), ea.isFunction(a)) return c = X.call(arguments, 2), d = function () {
                return a.apply(b || this, c.concat(X.call(arguments)))
            }, d.guid = a.guid = a.guid || ea.guid++, d
        },
        now: function () {
            return +new Date
        },
        support: ca
    }), ea.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function (a, b) {
        _["[object " + b + "]"] = b.toLowerCase()
    });
    var ja = function (a) {
        function b(a, b, c, d) {
            var e, f, g, h, i, j, l, n, o, p;
            if ((b ? b.ownerDocument || b : O) !== G && F(b), b = b || G, c = c || [], h = b.nodeType, "string" != typeof a || !a || 1 !== h && 9 !== h && 11 !== h) return c;
            if (!d && I) {
                if (11 !== h && (e = sa.exec(a)))
                    if (g = e[1]) {
                        if (9 === h) {
                            if (!(f = b.getElementById(g)) || !f.parentNode) return c;
                            if (f.id === g) return c.push(f), c
                        } else if (b.ownerDocument && (f = b.ownerDocument.getElementById(g)) && M(b, f) && f.id === g) return c.push(f), c
                    } else {
                        if (e[2]) return $.apply(c, b.getElementsByTagName(a)), c;
                        if ((g = e[3]) && v.getElementsByClassName) return $.apply(c, b.getElementsByClassName(g)), c
                    }
                if (v.qsa && (!J || !J.test(a))) {
                    if (n = l = N, o = b, p = 1 !== h && a, 1 === h && "object" !== b.nodeName.toLowerCase()) {
                        for (j = z(a), (l = b.getAttribute("id")) ? n = l.replace(ua, "\\$&") : b.setAttribute("id", n), n = "[id='" + n + "'] ", i = j.length; i--;) j[i] = n + m(j[i]);
                        o = ta.test(a) && k(b.parentNode) || b, p = j.join(",")
                    }
                    if (p) try {
                        return $.apply(c, o.querySelectorAll(p)), c
                    } catch (q) {} finally {
                        l || b.removeAttribute("id")
                    }
                }
            }
            return B(a.replace(ia, "$1"), b, c, d)
        }

        function c() {
            function a(c, d) {
                return b.push(c + " ") > w.cacheLength && delete a[b.shift()], a[c + " "] = d
            }
            var b = [];
            return a
        }

        function d(a) {
            return a[N] = !0, a
        }

        function e(a) {
            var b = G.createElement("div");
            try {
                return !!a(b)
            } catch (c) {
                return !1
            } finally {
                b.parentNode && b.parentNode.removeChild(b), b = null
            }
        }

        function f(a, b) {
            for (var c = a.split("|"), d = a.length; d--;) w.attrHandle[c[d]] = b
        }

        function g(a, b) {
            var c = b && a,
                d = c && 1 === a.nodeType && 1 === b.nodeType && (~b.sourceIndex || V) - (~a.sourceIndex || V);
            if (d) return d;
            if (c)
                for (; c = c.nextSibling;)
                    if (c === b) return -1;
            return a ? 1 : -1
        }

        function h(a) {
            return function (b) {
                return "input" === b.nodeName.toLowerCase() && b.type === a
            }
        }

        function i(a) {
            return function (b) {
                var c = b.nodeName.toLowerCase();
                return ("input" === c || "button" === c) && b.type === a
            }
        }

        function j(a) {
            return d(function (b) {
                return b = +b, d(function (c, d) {
                    for (var e, f = a([], c.length, b), g = f.length; g--;) c[e = f[g]] && (c[e] = !(d[e] = c[e]))
                })
            })
        }

        function k(a) {
            return a && void 0 !== a.getElementsByTagName && a
        }

        function l() {}

        function m(a) {
            for (var b = 0, c = a.length, d = ""; b < c; b++) d += a[b].value;
            return d
        }

        function n(a, b, c) {
            var d = b.dir,
                e = c && "parentNode" === d,
                f = Q++;
            return b.first ? function (b, c, f) {
                for (; b = b[d];)
                    if (1 === b.nodeType || e) return a(b, c, f)
            } : function (b, c, g) {
                var h, i, j = [P, f];
                if (g) {
                    for (; b = b[d];)
                        if ((1 === b.nodeType || e) && a(b, c, g)) return !0
                } else
                    for (; b = b[d];)
                        if (1 === b.nodeType || e) {
                            if (i = b[N] || (b[N] = {}), (h = i[d]) && h[0] === P && h[1] === f) return j[2] = h[2];
                            if (i[d] = j, j[2] = a(b, c, g)) return !0
                        }
            }
        }

        function o(a) {
            return a.length > 1 ? function (b, c, d) {
                for (var e = a.length; e--;)
                    if (!a[e](b, c, d)) return !1;
                return !0
            } : a[0]
        }

        function p(a, c, d) {
            for (var e = 0, f = c.length; e < f; e++) b(a, c[e], d);
            return d
        }

        function q(a, b, c, d, e) {
            for (var f, g = [], h = 0, i = a.length, j = null != b; h < i; h++)(f = a[h]) && (c && !c(f, d, e) || (g.push(f), j && b.push(h)));
            return g
        }

        function r(a, b, c, e, f, g) {
            return e && !e[N] && (e = r(e)), f && !f[N] && (f = r(f, g)), d(function (d, g, h, i) {
                var j, k, l, m = [],
                    n = [],
                    o = g.length,
                    r = d || p(b || "*", h.nodeType ? [h] : h, []),
                    s = !a || !d && b ? r : q(r, m, a, h, i),
                    t = c ? f || (d ? a : o || e) ? [] : g : s;
                if (c && c(s, t, h, i), e)
                    for (j = q(t, n), e(j, [], h, i), k = j.length; k--;)(l = j[k]) && (t[n[k]] = !(s[n[k]] = l));
                if (d) {
                    if (f || a) {
                        if (f) {
                            for (j = [], k = t.length; k--;)(l = t[k]) && j.push(s[k] = l);
                            f(null, t = [], j, i)
                        }
                        for (k = t.length; k--;)(l = t[k]) && (j = f ? aa(d, l) : m[k]) > -1 && (d[j] = !(g[j] = l))
                    }
                } else t = q(t === g ? t.splice(o, t.length) : t), f ? f(null, g, t, i) : $.apply(g, t)
            })
        }

        function s(a) {
            for (var b, c, d, e = a.length, f = w.relative[a[0].type], g = f || w.relative[" "], h = f ? 1 : 0, i = n(function (a) {
                return a === b
            }, g, !0), j = n(function (a) {
                return aa(b, a) > -1
            }, g, !0), k = [
                function (a, c, d) {
                    var e = !f && (d || c !== C) || ((b = c).nodeType ? i(a, c, d) : j(a, c, d));
                    return b = null, e
                }
            ]; h < e; h++)
                if (c = w.relative[a[h].type]) k = [n(o(k), c)];
                else {
                    if (c = w.filter[a[h].type].apply(null, a[h].matches), c[N]) {
                        for (d = ++h; d < e && !w.relative[a[d].type]; d++);
                        return r(h > 1 && o(k), h > 1 && m(a.slice(0, h - 1).concat({
                            value: " " === a[h - 2].type ? "*" : ""
                        })).replace(ia, "$1"), c, h < d && s(a.slice(h, d)), d < e && s(a = a.slice(d)), d < e && m(a))
                    }
                    k.push(c)
                }
            return o(k)
        }

        function t(a, c) {
            var e = c.length > 0,
                f = a.length > 0,
                g = function (d, g, h, i, j) {
                    var k, l, m, n = 0,
                        o = "0",
                        p = d && [],
                        r = [],
                        s = C,
                        t = d || f && w.find.TAG("*", j),
                        u = P += null == s ? 1 : Math.random() || .1,
                        v = t.length;
                    for (j && (C = g !== G && g); o !== v && null != (k = t[o]); o++) {
                        if (f && k) {
                            for (l = 0; m = a[l++];)
                                if (m(k, g, h)) {
                                    i.push(k);
                                    break
                                }
                            j && (P = u)
                        }
                        e && ((k = !m && k) && n--, d && p.push(k))
                    }
                    if (n += o, e && o !== n) {
                        for (l = 0; m = c[l++];) m(p, r, g, h);
                        if (d) {
                            if (n > 0)
                                for (; o--;) p[o] || r[o] || (r[o] = Y.call(i));
                            r = q(r)
                        }
                        $.apply(i, r), j && !d && r.length > 0 && n + c.length > 1 && b.uniqueSort(i)
                    }
                    return j && (P = u, C = s), p
                };
            return e ? d(g) : g
        }
        var u, v, w, x, y, z, A, B, C, D, E, F, G, H, I, J, K, L, M, N = "sizzle" + 1 * new Date,
            O = a.document,
            P = 0,
            Q = 0,
            R = c(),
            S = c(),
            T = c(),
            U = function (a, b) {
                return a === b && (E = !0), 0
            },
            V = 1 << 31,
            W = {}.hasOwnProperty,
            X = [],
            Y = X.pop,
            Z = X.push,
            $ = X.push,
            _ = X.slice,
            aa = function (a, b) {
                for (var c = 0, d = a.length; c < d; c++)
                    if (a[c] === b) return c;
                return -1
            },
            ba = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            ca = "[\\x20\\t\\r\\n\\f]",
            da = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
            ea = da.replace("w", "w#"),
            fa = "\\[" + ca + "*(" + da + ")(?:" + ca + "*([*^$|!~]?=)" + ca + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + ea + "))|)" + ca + "*\\]",
            ga = ":(" + da + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + fa + ")*)|.*)\\)|)",
            ha = new RegExp(ca + "+", "g"),
            ia = new RegExp("^" + ca + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ca + "+$", "g"),
            ja = new RegExp("^" + ca + "*," + ca + "*"),
            ka = new RegExp("^" + ca + "*([>+~]|" + ca + ")" + ca + "*"),
            la = new RegExp("=" + ca + "*([^\\]'\"]*?)" + ca + "*\\]", "g"),
            ma = new RegExp(ga),
            na = new RegExp("^" + ea + "$"),
            oa = {
                ID: new RegExp("^#(" + da + ")"),
                CLASS: new RegExp("^\\.(" + da + ")"),
                TAG: new RegExp("^(" + da.replace("w", "w*") + ")"),
                ATTR: new RegExp("^" + fa),
                PSEUDO: new RegExp("^" + ga),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ca + "*(even|odd|(([+-]|)(\\d*)n|)" + ca + "*(?:([+-]|)" + ca + "*(\\d+)|))" + ca + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + ba + ")$", "i"),
                needsContext: new RegExp("^" + ca + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ca + "*((?:-\\d)?\\d*)" + ca + "*\\)|)(?=[^-]|$)", "i")
            },
            pa = /^(?:input|select|textarea|button)$/i,
            qa = /^h\d$/i,
            ra = /^[^{]+\{\s*\[native \w/,
            sa = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            ta = /[+~]/,
            ua = /'|\\/g,
            va = new RegExp("\\\\([\\da-f]{1,6}" + ca + "?|(" + ca + ")|.)", "ig"),
            wa = function (a, b, c) {
                var d = "0x" + b - 65536;
                return d !== d || c ? b : d < 0 ? String.fromCharCode(d + 65536) : String.fromCharCode(d >> 10 | 55296, 1023 & d | 56320)
            },
            xa = function () {
                F()
            };
        try {
            $.apply(X = _.call(O.childNodes), O.childNodes), X[O.childNodes.length].nodeType
        } catch (ya) {
            $ = {
                apply: X.length ? function (a, b) {
                    Z.apply(a, _.call(b))
                } : function (a, b) {
                    for (var c = a.length, d = 0; a[c++] = b[d++];);
                    a.length = c - 1
                }
            }
        }
        v = b.support = {}, y = b.isXML = function (a) {
            var b = a && (a.ownerDocument || a).documentElement;
            return !!b && "HTML" !== b.nodeName
        }, F = b.setDocument = function (a) {
            var b, c, d = a ? a.ownerDocument || a : O;
            return d !== G && 9 === d.nodeType && d.documentElement ? (G = d, H = d.documentElement, c = d.defaultView, c && c !== c.top && (c.addEventListener ? c.addEventListener("unload", xa, !1) : c.attachEvent && c.attachEvent("onunload", xa)), I = !y(d), v.attributes = e(function (a) {
                return a.className = "i", !a.getAttribute("className")
            }), v.getElementsByTagName = e(function (a) {
                return a.appendChild(d.createComment("")), !a.getElementsByTagName("*").length
            }), v.getElementsByClassName = ra.test(d.getElementsByClassName), v.getById = e(function (a) {
                return H.appendChild(a).id = N, !d.getElementsByName || !d.getElementsByName(N).length
            }), v.getById ? (w.find.ID = function (a, b) {
                if (void 0 !== b.getElementById && I) {
                    var c = b.getElementById(a);
                    return c && c.parentNode ? [c] : []
                }
            }, w.filter.ID = function (a) {
                var b = a.replace(va, wa);
                return function (a) {
                    return a.getAttribute("id") === b
                }
            }) : (delete w.find.ID, w.filter.ID = function (a) {
                var b = a.replace(va, wa);
                return function (a) {
                    var c = void 0 !== a.getAttributeNode && a.getAttributeNode("id");
                    return c && c.value === b
                }
            }), w.find.TAG = v.getElementsByTagName ? function (a, b) {
                return void 0 !== b.getElementsByTagName ? b.getElementsByTagName(a) : v.qsa ? b.querySelectorAll(a) : void 0
            } : function (a, b) {
                var c, d = [],
                    e = 0,
                    f = b.getElementsByTagName(a);
                if ("*" === a) {
                    for (; c = f[e++];) 1 === c.nodeType && d.push(c);
                    return d
                }
                return f
            }, w.find.CLASS = v.getElementsByClassName && function (a, b) {
                if (I) return b.getElementsByClassName(a)
            }, K = [], J = [], (v.qsa = ra.test(d.querySelectorAll)) && (e(function (a) {
                H.appendChild(a).innerHTML = "<a id='" + N + "'></a><select id='" + N + "-\f]' msallowcapture=''><option selected=''></option></select>", a.querySelectorAll("[msallowcapture^='']").length && J.push("[*^$]=" + ca + "*(?:''|\"\")"), a.querySelectorAll("[selected]").length || J.push("\\[" + ca + "*(?:value|" + ba + ")"), a.querySelectorAll("[id~=" + N + "-]").length || J.push("~="), a.querySelectorAll(":checked").length || J.push(":checked"), a.querySelectorAll("a#" + N + "+*").length || J.push(".#.+[+~]")
            }), e(function (a) {
                var b = d.createElement("input");
                b.setAttribute("type", "hidden"), a.appendChild(b).setAttribute("name", "D"), a.querySelectorAll("[name=d]").length && J.push("name" + ca + "*[*^$|!~]?="), a.querySelectorAll(":enabled").length || J.push(":enabled", ":disabled"), a.querySelectorAll("*,:x"), J.push(",.*:")
            })), (v.matchesSelector = ra.test(L = H.matches || H.webkitMatchesSelector || H.mozMatchesSelector || H.oMatchesSelector || H.msMatchesSelector)) && e(function (a) {
                v.disconnectedMatch = L.call(a, "div"), L.call(a, "[s!='']:x"), K.push("!=", ga)
            }), J = J.length && new RegExp(J.join("|")), K = K.length && new RegExp(K.join("|")), b = ra.test(H.compareDocumentPosition), M = b || ra.test(H.contains) ? function (a, b) {
                var c = 9 === a.nodeType ? a.documentElement : a,
                    d = b && b.parentNode;
                return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d)))
            } : function (a, b) {
                if (b)
                    for (; b = b.parentNode;)
                        if (b === a) return !0;
                return !1
            }, U = b ? function (a, b) {
                if (a === b) return E = !0, 0;
                var c = !a.compareDocumentPosition - !b.compareDocumentPosition;
                return c || (c = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1, 1 & c || !v.sortDetached && b.compareDocumentPosition(a) === c ? a === d || a.ownerDocument === O && M(O, a) ? -1 : b === d || b.ownerDocument === O && M(O, b) ? 1 : D ? aa(D, a) - aa(D, b) : 0 : 4 & c ? -1 : 1)
            } : function (a, b) {
                if (a === b) return E = !0, 0;
                var c, e = 0,
                    f = a.parentNode,
                    h = b.parentNode,
                    i = [a],
                    j = [b];
                if (!f || !h) return a === d ? -1 : b === d ? 1 : f ? -1 : h ? 1 : D ? aa(D, a) - aa(D, b) : 0;
                if (f === h) return g(a, b);
                for (c = a; c = c.parentNode;) i.unshift(c);
                for (c = b; c = c.parentNode;) j.unshift(c);
                for (; i[e] === j[e];) e++;
                return e ? g(i[e], j[e]) : i[e] === O ? -1 : j[e] === O ? 1 : 0
            }, d) : G
        }, b.matches = function (a, c) {
            return b(a, null, null, c)
        }, b.matchesSelector = function (a, c) {
            if ((a.ownerDocument || a) !== G && F(a), c = c.replace(la, "='$1']"), v.matchesSelector && I && (!K || !K.test(c)) && (!J || !J.test(c))) try {
                var d = L.call(a, c);
                if (d || v.disconnectedMatch || a.document && 11 !== a.document.nodeType) return d
            } catch (ya) {}
            return b(c, G, null, [a]).length > 0
        }, b.contains = function (a, b) {
            return (a.ownerDocument || a) !== G && F(a), M(a, b)
        }, b.attr = function (a, b) {
            (a.ownerDocument || a) !== G && F(a);
            var c = w.attrHandle[b.toLowerCase()],
                d = c && W.call(w.attrHandle, b.toLowerCase()) ? c(a, b, !I) : void 0;
            return void 0 !== d ? d : v.attributes || !I ? a.getAttribute(b) : (d = a.getAttributeNode(b)) && d.specified ? d.value : null
        }, b.error = function (a) {
            throw new Error("Syntax error, unrecognized expression: " + a)
        }, b.uniqueSort = function (a) {
            var b, c = [],
                d = 0,
                e = 0;
            if (E = !v.detectDuplicates, D = !v.sortStable && a.slice(0), a.sort(U), E) {
                for (; b = a[e++];) b === a[e] && (d = c.push(e));
                for (; d--;) a.splice(c[d], 1)
            }
            return D = null, a
        }, x = b.getText = function (a) {
            var b, c = "",
                d = 0,
                e = a.nodeType;
            if (e) {
                if (1 === e || 9 === e || 11 === e) {
                    if ("string" == typeof a.textContent) return a.textContent;
                    for (a = a.firstChild; a; a = a.nextSibling) c += x(a)
                } else if (3 === e || 4 === e) return a.nodeValue
            } else
                for (; b = a[d++];) c += x(b);
            return c
        }, w = b.selectors = {
            cacheLength: 50,
            createPseudo: d,
            match: oa,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function (a) {
                    return a[1] = a[1].replace(va, wa), a[3] = (a[3] || a[4] || a[5] || "").replace(va, wa), "~=" === a[2] && (a[3] = " " + a[3] + " "), a.slice(0, 4)
                },
                CHILD: function (a) {
                    return a[1] = a[1].toLowerCase(), "nth" === a[1].slice(0, 3) ? (a[3] || b.error(a[0]), a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3])), a[5] = +(a[7] + a[8] || "odd" === a[3])) : a[3] && b.error(a[0]), a
                },
                PSEUDO: function (a) {
                    var b, c = !a[6] && a[2];
                    return oa.CHILD.test(a[0]) ? null : (a[3] ? a[2] = a[4] || a[5] || "" : c && ma.test(c) && (b = z(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && (a[0] = a[0].slice(0, b), a[2] = c.slice(0, b)), a.slice(0, 3))
                }
            },
            filter: {
                TAG: function (a) {
                    var b = a.replace(va, wa).toLowerCase();
                    return "*" === a ? function () {
                        return !0
                    } : function (a) {
                        return a.nodeName && a.nodeName.toLowerCase() === b
                    }
                },
                CLASS: function (a) {
                    var b = R[a + " "];
                    return b || (b = new RegExp("(^|" + ca + ")" + a + "(" + ca + "|$)")) && R(a, function (a) {
                        return b.test("string" == typeof a.className && a.className || void 0 !== a.getAttribute && a.getAttribute("class") || "")
                    })
                },
                ATTR: function (a, c, d) {
                    return function (e) {
                        var f = b.attr(e, a);
                        return null == f ? "!=" === c : !c || (f += "", "=" === c ? f === d : "!=" === c ? f !== d : "^=" === c ? d && 0 === f.indexOf(d) : "*=" === c ? d && f.indexOf(d) > -1 : "$=" === c ? d && f.slice(-d.length) === d : "~=" === c ? (" " + f.replace(ha, " ") + " ").indexOf(d) > -1 : "|=" === c && (f === d || f.slice(0, d.length + 1) === d + "-"))
                    }
                },
                CHILD: function (a, b, c, d, e) {
                    var f = "nth" !== a.slice(0, 3),
                        g = "last" !== a.slice(-4),
                        h = "of-type" === b;
                    return 1 === d && 0 === e ? function (a) {
                        return !!a.parentNode
                    } : function (b, c, i) {
                        var j, k, l, m, n, o, p = f !== g ? "nextSibling" : "previousSibling",
                            q = b.parentNode,
                            r = h && b.nodeName.toLowerCase(),
                            s = !i && !h;
                        if (q) {
                            if (f) {
                                for (; p;) {
                                    for (l = b; l = l[p];)
                                        if (h ? l.nodeName.toLowerCase() === r : 1 === l.nodeType) return !1;
                                    o = p = "only" === a && !o && "nextSibling"
                                }
                                return !0
                            }
                            if (o = [g ? q.firstChild : q.lastChild], g && s) {
                                for (k = q[N] || (q[N] = {}), j = k[a] || [], n = j[0] === P && j[1], m = j[0] === P && j[2], l = n && q.childNodes[n]; l = ++n && l && l[p] || (m = n = 0) || o.pop();)
                                    if (1 === l.nodeType && ++m && l === b) {
                                        k[a] = [P, n, m];
                                        break
                                    }
                            } else if (s && (j = (b[N] || (b[N] = {}))[a]) && j[0] === P) m = j[1];
                            else
                                for (;
                                    (l = ++n && l && l[p] || (m = n = 0) || o.pop()) && ((h ? l.nodeName.toLowerCase() !== r : 1 !== l.nodeType) || !++m || (s && ((l[N] || (l[N] = {}))[a] = [P, m]), l !== b)););
                            return (m -= e) === d || m % d == 0 && m / d >= 0
                        }
                    }
                },
                PSEUDO: function (a, c) {
                    var e, f = w.pseudos[a] || w.setFilters[a.toLowerCase()] || b.error("unsupported pseudo: " + a);
                    return f[N] ? f(c) : f.length > 1 ? (e = [a, a, "", c], w.setFilters.hasOwnProperty(a.toLowerCase()) ? d(function (a, b) {
                        for (var d, e = f(a, c), g = e.length; g--;) d = aa(a, e[g]), a[d] = !(b[d] = e[g])
                    }) : function (a) {
                        return f(a, 0, e)
                    }) : f
                }
            },
            pseudos: {
                not: d(function (a) {
                    var b = [],
                        c = [],
                        e = A(a.replace(ia, "$1"));
                    return e[N] ? d(function (a, b, c, d) {
                        for (var f, g = e(a, null, d, []), h = a.length; h--;)(f = g[h]) && (a[h] = !(b[h] = f))
                    }) : function (a, d, f) {
                        return b[0] = a, e(b, null, f, c), b[0] = null, !c.pop()
                    }
                }),
                has: d(function (a) {
                    return function (c) {
                        return b(a, c).length > 0
                    }
                }),
                contains: d(function (a) {
                    return a = a.replace(va, wa),
                        function (b) {
                            return (b.textContent || b.innerText || x(b)).indexOf(a) > -1
                        }
                }),
                lang: d(function (a) {
                    return na.test(a || "") || b.error("unsupported lang: " + a), a = a.replace(va, wa).toLowerCase(),
                        function (b) {
                            var c;
                            do {
                                if (c = I ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang")) return (c = c.toLowerCase()) === a || 0 === c.indexOf(a + "-")
                            } while ((b = b.parentNode) && 1 === b.nodeType);
                            return !1
                        }
                }),
                target: function (b) {
                    var c = a.location && a.location.hash;
                    return c && c.slice(1) === b.id
                },
                root: function (a) {
                    return a === H
                },
                focus: function (a) {
                    return a === G.activeElement && (!G.hasFocus || G.hasFocus()) && !!(a.type || a.href || ~a.tabIndex)
                },
                enabled: function (a) {
                    return !1 === a.disabled
                },
                disabled: function (a) {
                    return !0 === a.disabled
                },
                checked: function (a) {
                    var b = a.nodeName.toLowerCase();
                    return "input" === b && !!a.checked || "option" === b && !!a.selected
                },
                selected: function (a) {
                    return a.parentNode && a.parentNode.selectedIndex, !0 === a.selected
                },
                empty: function (a) {
                    for (a = a.firstChild; a; a = a.nextSibling)
                        if (a.nodeType < 6) return !1;
                    return !0
                },
                parent: function (a) {
                    return !w.pseudos.empty(a)
                },
                header: function (a) {
                    return qa.test(a.nodeName)
                },
                input: function (a) {
                    return pa.test(a.nodeName)
                },
                button: function (a) {
                    var b = a.nodeName.toLowerCase();
                    return "input" === b && "button" === a.type || "button" === b
                },
                text: function (a) {
                    var b;
                    return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute("type")) || "text" === b.toLowerCase())
                },
                first: j(function () {
                    return [0]
                }),
                last: j(function (a, b) {
                    return [b - 1]
                }),
                eq: j(function (a, b, c) {
                    return [c < 0 ? c + b : c]
                }),
                even: j(function (a, b) {
                    for (var c = 0; c < b; c += 2) a.push(c);
                    return a
                }),
                odd: j(function (a, b) {
                    for (var c = 1; c < b; c += 2) a.push(c);
                    return a
                }),
                lt: j(function (a, b, c) {
                    for (var d = c < 0 ? c + b : c; --d >= 0;) a.push(d);
                    return a
                }),
                gt: j(function (a, b, c) {
                    for (var d = c < 0 ? c + b : c; ++d < b;) a.push(d);
                    return a
                })
            }
        }, w.pseudos.nth = w.pseudos.eq;
        for (u in {
            radio: !0,
            checkbox: !0,
            file: !0,
            password: !0,
            image: !0
        }) w.pseudos[u] = h(u);
        for (u in {
            submit: !0,
            reset: !0
        }) w.pseudos[u] = i(u);
        return l.prototype = w.filters = w.pseudos, w.setFilters = new l, z = b.tokenize = function (a, c) {
            var d, e, f, g, h, i, j, k = S[a + " "];
            if (k) return c ? 0 : k.slice(0);
            for (h = a, i = [], j = w.preFilter; h;) {
                d && !(e = ja.exec(h)) || (e && (h = h.slice(e[0].length) || h), i.push(f = [])), d = !1, (e = ka.exec(h)) && (d = e.shift(), f.push({
                    value: d,
                    type: e[0].replace(ia, " ")
                }), h = h.slice(d.length));
                for (g in w.filter)!(e = oa[g].exec(h)) || j[g] && !(e = j[g](e)) || (d = e.shift(), f.push({
                    value: d,
                    type: g,
                    matches: e
                }), h = h.slice(d.length));
                if (!d) break
            }
            return c ? h.length : h ? b.error(a) : S(a, i).slice(0)
        }, A = b.compile = function (a, b) {
            var c, d = [],
                e = [],
                f = T[a + " "];
            if (!f) {
                for (b || (b = z(a)), c = b.length; c--;) f = s(b[c]), f[N] ? d.push(f) : e.push(f);
                f = T(a, t(e, d)), f.selector = a
            }
            return f
        }, B = b.select = function (a, b, c, d) {
            var e, f, g, h, i, j = "function" == typeof a && a,
                l = !d && z(a = j.selector || a);
            if (c = c || [], 1 === l.length) {
                if (f = l[0] = l[0].slice(0), f.length > 2 && "ID" === (g = f[0]).type && v.getById && 9 === b.nodeType && I && w.relative[f[1].type]) {
                    if (!(b = (w.find.ID(g.matches[0].replace(va, wa), b) || [])[0])) return c;
                    j && (b = b.parentNode), a = a.slice(f.shift().value.length)
                }
                for (e = oa.needsContext.test(a) ? 0 : f.length; e-- && (g = f[e], !w.relative[h = g.type]);)
                    if ((i = w.find[h]) && (d = i(g.matches[0].replace(va, wa), ta.test(f[0].type) && k(b.parentNode) || b))) {
                        if (f.splice(e, 1), !(a = d.length && m(f))) return $.apply(c, d), c;
                        break
                    }
            }
            return (j || A(a, l))(d, b, !I, c, ta.test(a) && k(b.parentNode) || b), c
        }, v.sortStable = N.split("").sort(U).join("") === N, v.detectDuplicates = !!E, F(), v.sortDetached = e(function (a) {
            return 1 & a.compareDocumentPosition(G.createElement("div"))
        }), e(function (a) {
            return a.innerHTML = "<a href='#'></a>", "#" === a.firstChild.getAttribute("href")
        }) || f("type|href|height|width", function (a, b, c) {
            if (!c) return a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2)
        }), v.attributes && e(function (a) {
            return a.innerHTML = "<input/>", a.firstChild.setAttribute("value", ""), "" === a.firstChild.getAttribute("value")
        }) || f("value", function (a, b, c) {
            if (!c && "input" === a.nodeName.toLowerCase()) return a.defaultValue
        }), e(function (a) {
            return null == a.getAttribute("disabled")
        }) || f(ba, function (a, b, c) {
            var d;
            if (!c) return !0 === a[b] ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null
        }), b
    }(a);
    ea.find = ja, ea.expr = ja.selectors, ea.expr[":"] = ea.expr.pseudos, ea.unique = ja.uniqueSort, ea.text = ja.getText, ea.isXMLDoc = ja.isXML, ea.contains = ja.contains;
    var ka = ea.expr.match.needsContext,
        la = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
        ma = /^.[^:#\[\.,]*$/;
    ea.filter = function (a, b, c) {
        var d = b[0];
        return c && (a = ":not(" + a + ")"), 1 === b.length && 1 === d.nodeType ? ea.find.matchesSelector(d, a) ? [d] : [] : ea.find.matches(a, ea.grep(b, function (a) {
            return 1 === a.nodeType
        }))
    }, ea.fn.extend({
        find: function (a) {
            var b, c = [],
                d = this,
                e = d.length;
            if ("string" != typeof a) return this.pushStack(ea(a).filter(function () {
                for (b = 0; b < e; b++)
                    if (ea.contains(d[b], this)) return !0
            }));
            for (b = 0; b < e; b++) ea.find(a, d[b], c);
            return c = this.pushStack(e > 1 ? ea.unique(c) : c), c.selector = this.selector ? this.selector + " " + a : a, c
        },
        filter: function (a) {
            return this.pushStack(d(this, a || [], !1))
        },
        not: function (a) {
            return this.pushStack(d(this, a || [], !0))
        },
        is: function (a) {
            return !!d(this, "string" == typeof a && ka.test(a) ? ea(a) : a || [], !1).length
        }
    });
    var na, oa = a.document,
        pa = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/;
    (ea.fn.init = function (a, b) {
        var c, d;
        if (!a) return this;
        if ("string" == typeof a) {
            if (!(c = "<" === a.charAt(0) && ">" === a.charAt(a.length - 1) && a.length >= 3 ? [null, a, null] : pa.exec(a)) || !c[1] && b) return !b || b.jquery ? (b || na).find(a) : this.constructor(b).find(a);
            if (c[1]) {
                if (b = b instanceof ea ? b[0] : b, ea.merge(this, ea.parseHTML(c[1], b && b.nodeType ? b.ownerDocument || b : oa, !0)), la.test(c[1]) && ea.isPlainObject(b))
                    for (c in b) ea.isFunction(this[c]) ? this[c](b[c]) : this.attr(c, b[c]);
                return this
            }
            if ((d = oa.getElementById(c[2])) && d.parentNode) {
                if (d.id !== c[2]) return na.find(a);
                this.length = 1, this[0] = d
            }
            return this.context = oa, this.selector = a, this
        }
        return a.nodeType ? (this.context = this[0] = a, this.length = 1, this) : ea.isFunction(a) ? void 0 !== na.ready ? na.ready(a) : a(ea) : (void 0 !== a.selector && (this.selector = a.selector, this.context = a.context), ea.makeArray(a, this))
    }).prototype = ea.fn, na = ea(oa);
    var qa = /^(?:parents|prev(?:Until|All))/,
        ra = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    ea.extend({
        dir: function (a, b, c) {
            for (var d = [], e = a[b]; e && 9 !== e.nodeType && (void 0 === c || 1 !== e.nodeType || !ea(e).is(c));) 1 === e.nodeType && d.push(e), e = e[b];
            return d
        },
        sibling: function (a, b) {
            for (var c = []; a; a = a.nextSibling) 1 === a.nodeType && a !== b && c.push(a);
            return c
        }
    }), ea.fn.extend({
        has: function (a) {
            var b, c = ea(a, this),
                d = c.length;
            return this.filter(function () {
                for (b = 0; b < d; b++)
                    if (ea.contains(this, c[b])) return !0
            })
        },
        closest: function (a, b) {
            for (var c, d = 0, e = this.length, f = [], g = ka.test(a) || "string" != typeof a ? ea(a, b || this.context) : 0; d < e; d++)
                for (c = this[d]; c && c !== b; c = c.parentNode)
                    if (c.nodeType < 11 && (g ? g.index(c) > -1 : 1 === c.nodeType && ea.find.matchesSelector(c, a))) {
                        f.push(c);
                        break
                    }
            return this.pushStack(f.length > 1 ? ea.unique(f) : f)
        },
        index: function (a) {
            return a ? "string" == typeof a ? ea.inArray(this[0], ea(a)) : ea.inArray(a.jquery ? a[0] : a, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function (a, b) {
            return this.pushStack(ea.unique(ea.merge(this.get(), ea(a, b))))
        },
        addBack: function (a) {
            return this.add(null == a ? this.prevObject : this.prevObject.filter(a))
        }
    }), ea.each({
        parent: function (a) {
            var b = a.parentNode;
            return b && 11 !== b.nodeType ? b : null
        },
        parents: function (a) {
            return ea.dir(a, "parentNode")
        },
        parentsUntil: function (a, b, c) {
            return ea.dir(a, "parentNode", c)
        },
        next: function (a) {
            return e(a, "nextSibling")
        },
        prev: function (a) {
            return e(a, "previousSibling")
        },
        nextAll: function (a) {
            return ea.dir(a, "nextSibling")
        },
        prevAll: function (a) {
            return ea.dir(a, "previousSibling")
        },
        nextUntil: function (a, b, c) {
            return ea.dir(a, "nextSibling", c)
        },
        prevUntil: function (a, b, c) {
            return ea.dir(a, "previousSibling", c)
        },
        siblings: function (a) {
            return ea.sibling((a.parentNode || {}).firstChild, a)
        },
        children: function (a) {
            return ea.sibling(a.firstChild)
        },
        contents: function (a) {
            return ea.nodeName(a, "iframe") ? a.contentDocument || a.contentWindow.document : ea.merge([], a.childNodes)
        }
    }, function (a, b) {
        ea.fn[a] = function (c, d) {
            var e = ea.map(this, b, c);
            return "Until" !== a.slice(-5) && (d = c), d && "string" == typeof d && (e = ea.filter(d, e)), this.length > 1 && (ra[a] || (e = ea.unique(e)), qa.test(a) && (e = e.reverse())), this.pushStack(e)
        }
    });
    var sa = /\S+/g,
        ta = {};
    ea.Callbacks = function (a) {
        a = "string" == typeof a ? ta[a] || f(a) : ea.extend({}, a);
        var b, c, d, e, g, h, i = [],
            j = !a.once && [],
            k = function (f) {
                for (c = a.memory && f, d = !0, g = h || 0, h = 0, e = i.length, b = !0; i && g < e; g++)
                    if (!1 === i[g].apply(f[0], f[1]) && a.stopOnFalse) {
                        c = !1;
                        break
                    }
                b = !1, i && (j ? j.length && k(j.shift()) : c ? i = [] : l.disable())
            },
            l = {
                add: function () {
                    if (i) {
                        var d = i.length;
                        ! function b(c) {
                            ea.each(c, function (c, d) {
                                var e = ea.type(d);
                                "function" === e ? a.unique && l.has(d) || i.push(d) : d && d.length && "string" !== e && b(d)
                            })
                        }(arguments), b ? e = i.length : c && (h = d, k(c))
                    }
                    return this
                },
                remove: function () {
                    return i && ea.each(arguments, function (a, c) {
                        for (var d;
                             (d = ea.inArray(c, i, d)) > -1;) i.splice(d, 1), b && (d <= e && e--, d <= g && g--)
                    }), this
                },
                has: function (a) {
                    return a ? ea.inArray(a, i) > -1 : !(!i || !i.length)
                },
                empty: function () {
                    return i = [], e = 0, this
                },
                disable: function () {
                    return i = j = c = void 0, this
                },
                disabled: function () {
                    return !i
                },
                lock: function () {
                    return j = void 0, c || l.disable(), this
                },
                locked: function () {
                    return !j
                },
                fireWith: function (a, c) {
                    return !i || d && !j || (c = c || [], c = [a, c.slice ? c.slice() : c], b ? j.push(c) : k(c)), this
                },
                fire: function () {
                    return l.fireWith(this, arguments), this
                },
                fired: function () {
                    return !!d
                }
            };
        return l
    }, ea.extend({
        Deferred: function (a) {
            var b = [
                    ["resolve", "done", ea.Callbacks("once memory"), "resolved"],
                    ["reject", "fail", ea.Callbacks("once memory"), "rejected"],
                    ["notify", "progress", ea.Callbacks("memory")]
                ],
                c = "pending",
                d = {
                    state: function () {
                        return c
                    },
                    always: function () {
                        return e.done(arguments).fail(arguments), this
                    },
                    then: function () {
                        var a = arguments;
                        return ea.Deferred(function (c) {
                            ea.each(b, function (b, f) {
                                var g = ea.isFunction(a[b]) && a[b];
                                e[f[1]](function () {
                                    var a = g && g.apply(this, arguments);
                                    a && ea.isFunction(a.promise) ? a.promise().done(c.resolve).fail(c.reject).progress(c.notify) : c[f[0] + "With"](this === d ? c.promise() : this, g ? [a] : arguments)
                                })
                            }), a = null
                        }).promise()
                    },
                    promise: function (a) {
                        return null != a ? ea.extend(a, d) : d
                    }
                },
                e = {};
            return d.pipe = d.then, ea.each(b, function (a, f) {
                var g = f[2],
                    h = f[3];
                d[f[1]] = g.add, h && g.add(function () {
                    c = h
                }, b[1 ^ a][2].disable, b[2][2].lock), e[f[0]] = function () {
                    return e[f[0] + "With"](this === e ? d : this, arguments), this
                }, e[f[0] + "With"] = g.fireWith
            }), d.promise(e), a && a.call(e, e), e
        },
        when: function (a) {
            var b, c, d, e = 0,
                f = X.call(arguments),
                g = f.length,
                h = 1 !== g || a && ea.isFunction(a.promise) ? g : 0,
                i = 1 === h ? a : ea.Deferred(),
                j = function (a, c, d) {
                    return function (e) {
                        c[a] = this, d[a] = arguments.length > 1 ? X.call(arguments) : e, d === b ? i.notifyWith(c, d) : --h || i.resolveWith(c, d)
                    }
                };
            if (g > 1)
                for (b = new Array(g), c = new Array(g), d = new Array(g); e < g; e++) f[e] && ea.isFunction(f[e].promise) ? f[e].promise().done(j(e, d, f)).fail(i.reject).progress(j(e, c, b)) : --h;
            return h || i.resolveWith(d, f), i.promise()
        }
    });
    var ua;
    ea.fn.ready = function (a) {
        return ea.ready.promise().done(a), this
    }, ea.extend({
        isReady: !1,
        readyWait: 1,
        holdReady: function (a) {
            a ? ea.readyWait++ : ea.ready(!0)
        },
        ready: function (a) {
            if (!0 === a ? !--ea.readyWait : !ea.isReady) {
                if (!oa.body) return setTimeout(ea.ready);
                ea.isReady = !0, !0 !== a && --ea.readyWait > 0 || (ua.resolveWith(oa, [ea]), ea.fn.triggerHandler && (ea(oa).triggerHandler("ready"), ea(oa).off("ready")))
            }
        }
    }), ea.ready.promise = function (b) {
        if (!ua)
            if (ua = ea.Deferred(), "complete" === oa.readyState) setTimeout(ea.ready);
            else if (oa.addEventListener) oa.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1);
            else {
                oa.attachEvent("onreadystatechange", h), a.attachEvent("onload", h);
                var c = !1;
                try {
                    c = null == a.frameElement && oa.documentElement
                } catch (d) {}
                c && c.doScroll && function a() {
                    if (!ea.isReady) {
                        try {
                            c.doScroll("left")
                        } catch (d) {
                            return setTimeout(a, 50)
                        }
                        g(), ea.ready()
                    }
                }()
            }
        return ua.promise(b)
    };
    var va, wa = "undefined";
    for (va in ea(ca)) break;
    ca.ownLast = "0" !== va, ca.inlineBlockNeedsLayout = !1, ea(function () {
        var a, b, c, d;
        (c = oa.getElementsByTagName("body")[0]) && c.style && (b = oa.createElement("div"), d = oa.createElement("div"), d.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", c.appendChild(d).appendChild(b), typeof b.style.zoom !== wa && (b.style.cssText = "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1", ca.inlineBlockNeedsLayout = a = 3 === b.offsetWidth, a && (c.style.zoom = 1)), c.removeChild(d))
    }),
        function () {
            var a = oa.createElement("div");
            if (null == ca.deleteExpando) {
                ca.deleteExpando = !0;
                try {
                    delete a.test
                } catch (b) {
                    ca.deleteExpando = !1
                }
            }
            a = null
        }(), ea.acceptData = function (a) {
        var b = ea.noData[(a.nodeName + " ").toLowerCase()],
            c = +a.nodeType || 1;
        return (1 === c || 9 === c) && (!b || !0 !== b && a.getAttribute("classid") === b)
    };
    var xa = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
        ya = /([A-Z])/g;
    ea.extend({
        cache: {},
        noData: {
            "applet ": !0,
            "embed ": !0,
            "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
        },
        hasData: function (a) {
            return !!(a = a.nodeType ? ea.cache[a[ea.expando]] : a[ea.expando]) && !j(a)
        },
        data: function (a, b, c) {
            return k(a, b, c)
        },
        removeData: function (a, b) {
            return l(a, b)
        },
        _data: function (a, b, c) {
            return k(a, b, c, !0)
        },
        _removeData: function (a, b) {
            return l(a, b, !0)
        }
    }), ea.fn.extend({
        data: function (a, b) {
            var c, d, e, f = this[0],
                g = f && f.attributes;
            if (void 0 === a) {
                if (this.length && (e = ea.data(f), 1 === f.nodeType && !ea._data(f, "parsedAttrs"))) {
                    for (c = g.length; c--;) g[c] && (d = g[c].name, 0 === d.indexOf("data-") && (d = ea.camelCase(d.slice(5)), i(f, d, e[d])));
                    ea._data(f, "parsedAttrs", !0)
                }
                return e
            }
            return "object" == typeof a ? this.each(function () {
                ea.data(this, a)
            }) : arguments.length > 1 ? this.each(function () {
                ea.data(this, a, b)
            }) : f ? i(f, a, ea.data(f, a)) : void 0
        },
        removeData: function (a) {
            return this.each(function () {
                ea.removeData(this, a)
            })
        }
    }), ea.extend({
        queue: function (a, b, c) {
            var d;
            if (a) return b = (b || "fx") + "queue", d = ea._data(a, b), c && (!d || ea.isArray(c) ? d = ea._data(a, b, ea.makeArray(c)) : d.push(c)), d || []
        },
        dequeue: function (a, b) {
            b = b || "fx";
            var c = ea.queue(a, b),
                d = c.length,
                e = c.shift(),
                f = ea._queueHooks(a, b),
                g = function () {
                    ea.dequeue(a, b)
                };
            "inprogress" === e && (e = c.shift(), d--), e && ("fx" === b && c.unshift("inprogress"), delete f.stop, e.call(a, g, f)), !d && f && f.empty.fire()
        },
        _queueHooks: function (a, b) {
            var c = b + "queueHooks";
            return ea._data(a, c) || ea._data(a, c, {
                empty: ea.Callbacks("once memory").add(function () {
                    ea._removeData(a, b + "queue"), ea._removeData(a, c)
                })
            })
        }
    }), ea.fn.extend({
        queue: function (a, b) {
            var c = 2;
            return "string" != typeof a && (b = a, a = "fx", c--), arguments.length < c ? ea.queue(this[0], a) : void 0 === b ? this : this.each(function () {
                var c = ea.queue(this, a, b);
                ea._queueHooks(this, a), "fx" === a && "inprogress" !== c[0] && ea.dequeue(this, a)
            })
        },
        dequeue: function (a) {
            return this.each(function () {
                ea.dequeue(this, a)
            })
        },
        clearQueue: function (a) {
            return this.queue(a || "fx", [])
        },
        promise: function (a, b) {
            var c, d = 1,
                e = ea.Deferred(),
                f = this,
                g = this.length,
                h = function () {
                    --d || e.resolveWith(f, [f])
                };
            for ("string" != typeof a && (b = a, a = void 0), a = a || "fx"; g--;)(c = ea._data(f[g], a + "queueHooks")) && c.empty && (d++, c.empty.add(h));
            return h(), e.promise(b)
        }
    });
    var za = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        Aa = ["Top", "Right", "Bottom", "Left"],
        Ba = function (a, b) {
            return a = b || a, "none" === ea.css(a, "display") || !ea.contains(a.ownerDocument, a)
        },
        Ca = ea.access = function (a, b, c, d, e, f, g) {
            var h = 0,
                i = a.length,
                j = null == c;
            if ("object" === ea.type(c)) {
                e = !0;
                for (h in c) ea.access(a, b, h, c[h], !0, f, g)
            } else if (void 0 !== d && (e = !0, ea.isFunction(d) || (g = !0), j && (g ? (b.call(a, d), b = null) : (j = b, b = function (a, b, c) {
                return j.call(ea(a), c)
            })), b))
                for (; h < i; h++) b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));
            return e ? a : j ? b.call(a) : i ? b(a[0], c) : f
        },
        Da = /^(?:checkbox|radio)$/i;
    ! function () {
        var a = oa.createElement("input"),
            b = oa.createElement("div"),
            c = oa.createDocumentFragment();
        if (b.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", ca.leadingWhitespace = 3 === b.firstChild.nodeType, ca.tbody = !b.getElementsByTagName("tbody").length, ca.htmlSerialize = !!b.getElementsByTagName("link").length, ca.html5Clone = "<:nav></:nav>" !== oa.createElement("nav").cloneNode(!0).outerHTML, a.type = "checkbox", a.checked = !0, c.appendChild(a), ca.appendChecked = a.checked, b.innerHTML = "<textarea>x</textarea>", ca.noCloneChecked = !!b.cloneNode(!0).lastChild.defaultValue, c.appendChild(b), b.innerHTML = "<input type='radio' checked='checked' name='t'/>", ca.checkClone = b.cloneNode(!0).cloneNode(!0).lastChild.checked, ca.noCloneEvent = !0, b.attachEvent && (b.attachEvent("onclick", function () {
            ca.noCloneEvent = !1
        }), b.cloneNode(!0).click()), null == ca.deleteExpando) {
            ca.deleteExpando = !0;
            try {
                delete b.test
            } catch (d) {
                ca.deleteExpando = !1
            }
        }
    }(),
        function () {
            var b, c, d = oa.createElement("div");
            for (b in {
                submit: !0,
                change: !0,
                focusin: !0
            }) c = "on" + b, (ca[b + "Bubbles"] = c in a) || (d.setAttribute(c, "t"), ca[b + "Bubbles"] = !1 === d.attributes[c].expando);
            d = null
        }();
    var Ea = /^(?:input|select|textarea)$/i,
        Fa = /^key/,
        Ga = /^(?:mouse|pointer|contextmenu)|click/,
        Ha = /^(?:focusinfocus|focusoutblur)$/,
        Ia = /^([^.]*)(?:\.(.+)|)$/;
    ea.event = {
        global: {},
        add: function (a, b, c, d, e) {
            var f, g, h, i, j, k, l, m, n, o, p, q = ea._data(a);
            if (q) {
                for (c.handler && (i = c, c = i.handler, e = i.selector), c.guid || (c.guid = ea.guid++), (g = q.events) || (g = q.events = {}), (k = q.handle) || (k = q.handle = function (a) {
                    return typeof ea === wa || a && ea.event.triggered === a.type ? void 0 : ea.event.dispatch.apply(k.elem, arguments)
                }, k.elem = a), b = (b || "").match(sa) || [""], h = b.length; h--;) f = Ia.exec(b[h]) || [], n = p = f[1], o = (f[2] || "").split(".").sort(), n && (j = ea.event.special[n] || {}, n = (e ? j.delegateType : j.bindType) || n, j = ea.event.special[n] || {}, l = ea.extend({
                    type: n,
                    origType: p,
                    data: d,
                    handler: c,
                    guid: c.guid,
                    selector: e,
                    needsContext: e && ea.expr.match.needsContext.test(e),
                    namespace: o.join(".")
                }, i), (m = g[n]) || (m = g[n] = [], m.delegateCount = 0, j.setup && !1 !== j.setup.call(a, d, o, k) || (a.addEventListener ? a.addEventListener(n, k, !1) : a.attachEvent && a.attachEvent("on" + n, k))), j.add && (j.add.call(a, l), l.handler.guid || (l.handler.guid = c.guid)), e ? m.splice(m.delegateCount++, 0, l) : m.push(l), ea.event.global[n] = !0);
                a = null
            }
        },
        remove: function (a, b, c, d, e) {
            var f, g, h, i, j, k, l, m, n, o, p, q = ea.hasData(a) && ea._data(a);
            if (q && (k = q.events)) {
                for (b = (b || "").match(sa) || [""], j = b.length; j--;)
                    if (h = Ia.exec(b[j]) || [], n = p = h[1], o = (h[2] || "").split(".").sort(), n) {
                        for (l = ea.event.special[n] || {}, n = (d ? l.delegateType : l.bindType) || n, m = k[n] || [], h = h[2] && new RegExp("(^|\\.)" + o.join("\\.(?:.*\\.|)") + "(\\.|$)"), i = f = m.length; f--;) g = m[f], !e && p !== g.origType || c && c.guid !== g.guid || h && !h.test(g.namespace) || d && d !== g.selector && ("**" !== d || !g.selector) || (m.splice(f, 1), g.selector && m.delegateCount--, l.remove && l.remove.call(a, g));
                        i && !m.length && (l.teardown && !1 !== l.teardown.call(a, o, q.handle) || ea.removeEvent(a, n, q.handle), delete k[n])
                    } else
                        for (n in k) ea.event.remove(a, n + b[j], c, d, !0);
                ea.isEmptyObject(k) && (delete q.handle, ea._removeData(a, "events"))
            }
        },
        trigger: function (b, c, d, e) {
            var f, g, h, i, j, k, l, m = [d || oa],
                n = ba.call(b, "type") ? b.type : b,
                o = ba.call(b, "namespace") ? b.namespace.split(".") : [];
            if (h = k = d = d || oa, 3 !== d.nodeType && 8 !== d.nodeType && !Ha.test(n + ea.event.triggered) && (n.indexOf(".") >= 0 && (o = n.split("."), n = o.shift(), o.sort()), g = n.indexOf(":") < 0 && "on" + n, b = b[ea.expando] ? b : new ea.Event(n, "object" == typeof b && b), b.isTrigger = e ? 2 : 3, b.namespace = o.join("."), b.namespace_re = b.namespace ? new RegExp("(^|\\.)" + o.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, b.result = void 0, b.target || (b.target = d), c = null == c ? [b] : ea.makeArray(c, [b]), j = ea.event.special[n] || {}, e || !j.trigger || !1 !== j.trigger.apply(d, c))) {
                if (!e && !j.noBubble && !ea.isWindow(d)) {
                    for (i = j.delegateType || n, Ha.test(i + n) || (h = h.parentNode); h; h = h.parentNode) m.push(h), k = h;
                    k === (d.ownerDocument || oa) && m.push(k.defaultView || k.parentWindow || a)
                }
                for (l = 0;
                     (h = m[l++]) && !b.isPropagationStopped();) b.type = l > 1 ? i : j.bindType || n, f = (ea._data(h, "events") || {})[b.type] && ea._data(h, "handle"), f && f.apply(h, c), (f = g && h[g]) && f.apply && ea.acceptData(h) && (b.result = f.apply(h, c), !1 === b.result && b.preventDefault());
                if (b.type = n, !e && !b.isDefaultPrevented() && (!j._default || !1 === j._default.apply(m.pop(), c)) && ea.acceptData(d) && g && d[n] && !ea.isWindow(d)) {
                    k = d[g], k && (d[g] = null), ea.event.triggered = n;
                    try {
                        d[n]()
                    } catch (p) {}
                    ea.event.triggered = void 0, k && (d[g] = k)
                }
                return b.result
            }
        },
        dispatch: function (a) {
            a = ea.event.fix(a);
            var b, c, d, e, f, g = [],
                h = X.call(arguments),
                i = (ea._data(this, "events") || {})[a.type] || [],
                j = ea.event.special[a.type] || {};
            if (h[0] = a, a.delegateTarget = this, !j.preDispatch || !1 !== j.preDispatch.call(this, a)) {
                for (g = ea.event.handlers.call(this, a, i), b = 0;
                     (e = g[b++]) && !a.isPropagationStopped();)
                    for (a.currentTarget = e.elem, f = 0;
                         (d = e.handlers[f++]) && !a.isImmediatePropagationStopped();) a.namespace_re && !a.namespace_re.test(d.namespace) || (a.handleObj = d, a.data = d.data, void 0 !== (c = ((ea.event.special[d.origType] || {}).handle || d.handler).apply(e.elem, h)) && !1 === (a.result = c) && (a.preventDefault(), a.stopPropagation()));
                return j.postDispatch && j.postDispatch.call(this, a), a.result
            }
        },
        handlers: function (a, b) {
            var c, d, e, f, g = [],
                h = b.delegateCount,
                i = a.target;
            if (h && i.nodeType && (!a.button || "click" !== a.type))
                for (; i != this; i = i.parentNode || this)
                    if (1 === i.nodeType && (!0 !== i.disabled || "click" !== a.type)) {
                        for (e = [], f = 0; f < h; f++) d = b[f], c = d.selector + " ", void 0 === e[c] && (e[c] = d.needsContext ? ea(c, this).index(i) >= 0 : ea.find(c, this, null, [i]).length), e[c] && e.push(d);
                        e.length && g.push({
                            elem: i,
                            handlers: e
                        })
                    }
            return h < b.length && g.push({
                elem: this,
                handlers: b.slice(h)
            }), g
        },
        fix: function (a) {
            if (a[ea.expando]) return a;
            var b, c, d, e = a.type,
                f = a,
                g = this.fixHooks[e];
            for (g || (this.fixHooks[e] = g = Ga.test(e) ? this.mouseHooks : Fa.test(e) ? this.keyHooks : {}), d = g.props ? this.props.concat(g.props) : this.props, a = new ea.Event(f), b = d.length; b--;) c = d[b], a[c] = f[c];
            return a.target || (a.target = f.srcElement || oa), 3 === a.target.nodeType && (a.target = a.target.parentNode), a.metaKey = !!a.metaKey, g.filter ? g.filter(a, f) : a
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function (a, b) {
                return null == a.which && (a.which = null != b.charCode ? b.charCode : b.keyCode), a
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function (a, b) {
                var c, d, e, f = b.button,
                    g = b.fromElement;
                return null == a.pageX && null != b.clientX && (d = a.target.ownerDocument || oa, e = d.documentElement, c = d.body, a.pageX = b.clientX + (e && e.scrollLeft || c && c.scrollLeft || 0) - (e && e.clientLeft || c && c.clientLeft || 0), a.pageY = b.clientY + (e && e.scrollTop || c && c.scrollTop || 0) - (e && e.clientTop || c && c.clientTop || 0)), !a.relatedTarget && g && (a.relatedTarget = g === a.target ? b.toElement : g), a.which || void 0 === f || (a.which = 1 & f ? 1 : 2 & f ? 3 : 4 & f ? 2 : 0), a
            }
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function () {
                    if (this !== o() && this.focus) try {
                        return this.focus(), !1
                    } catch (a) {}
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function () {
                    if (this === o() && this.blur) return this.blur(), !1
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function () {
                    if (ea.nodeName(this, "input") && "checkbox" === this.type && this.click) return this.click(), !1
                },
                _default: function (a) {
                    return ea.nodeName(a.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function (a) {
                    void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result)
                }
            }
        },
        simulate: function (a, b, c, d) {
            var e = ea.extend(new ea.Event, c, {
                type: a,
                isSimulated: !0,
                originalEvent: {}
            });
            d ? ea.event.trigger(e, null, b) : ea.event.dispatch.call(b, e), e.isDefaultPrevented() && c.preventDefault()
        }
    }, ea.removeEvent = oa.removeEventListener ? function (a, b, c) {
        a.removeEventListener && a.removeEventListener(b, c, !1)
    } : function (a, b, c) {
        var d = "on" + b;
        a.detachEvent && (typeof a[d] === wa && (a[d] = null), a.detachEvent(d, c))
    }, ea.Event = function (a, b) {
        if (!(this instanceof ea.Event)) return new ea.Event(a, b);
        a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || void 0 === a.defaultPrevented && !1 === a.returnValue ? m : n) : this.type = a, b && ea.extend(this, b), this.timeStamp = a && a.timeStamp || ea.now(), this[ea.expando] = !0
    }, ea.Event.prototype = {
        isDefaultPrevented: n,
        isPropagationStopped: n,
        isImmediatePropagationStopped: n,
        preventDefault: function () {
            var a = this.originalEvent;
            this.isDefaultPrevented = m, a && (a.preventDefault ? a.preventDefault() : a.returnValue = !1)
        },
        stopPropagation: function () {
            var a = this.originalEvent;
            this.isPropagationStopped = m, a && (a.stopPropagation && a.stopPropagation(), a.cancelBubble = !0)
        },
        stopImmediatePropagation: function () {
            var a = this.originalEvent;
            this.isImmediatePropagationStopped = m, a && a.stopImmediatePropagation && a.stopImmediatePropagation(), this.stopPropagation()
        }
    }, ea.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function (a, b) {
        ea.event.special[a] = {
            delegateType: b,
            bindType: b,
            handle: function (a) {
                var c, d = this,
                    e = a.relatedTarget,
                    f = a.handleObj;
                return e && (e === d || ea.contains(d, e)) || (a.type = f.origType, c = f.handler.apply(this, arguments), a.type = b), c
            }
        }
    }), ca.submitBubbles || (ea.event.special.submit = {
        setup: function () {
            if (ea.nodeName(this, "form")) return !1;
            ea.event.add(this, "click._submit keypress._submit", function (a) {
                var b = a.target,
                    c = ea.nodeName(b, "input") || ea.nodeName(b, "button") ? b.form : void 0;
                c && !ea._data(c, "submitBubbles") && (ea.event.add(c, "submit._submit", function (a) {
                    a._submit_bubble = !0
                }), ea._data(c, "submitBubbles", !0))
            })
        },
        postDispatch: function (a) {
            a._submit_bubble && (delete a._submit_bubble, this.parentNode && !a.isTrigger && ea.event.simulate("submit", this.parentNode, a, !0))
        },
        teardown: function () {
            if (ea.nodeName(this, "form")) return !1;
            ea.event.remove(this, "._submit")
        }
    }), ca.changeBubbles || (ea.event.special.change = {
        setup: function () {
            if (Ea.test(this.nodeName)) return "checkbox" !== this.type && "radio" !== this.type || (ea.event.add(this, "propertychange._change", function (a) {
                "checked" === a.originalEvent.propertyName && (this._just_changed = !0)
            }), ea.event.add(this, "click._change", function (a) {
                this._just_changed && !a.isTrigger && (this._just_changed = !1), ea.event.simulate("change", this, a, !0)
            })), !1;
            ea.event.add(this, "beforeactivate._change", function (a) {
                var b = a.target;
                Ea.test(b.nodeName) && !ea._data(b, "changeBubbles") && (ea.event.add(b, "change._change", function (a) {
                    !this.parentNode || a.isSimulated || a.isTrigger || ea.event.simulate("change", this.parentNode, a, !0)
                }), ea._data(b, "changeBubbles", !0))
            })
        },
        handle: function (a) {
            var b = a.target;
            if (this !== b || a.isSimulated || a.isTrigger || "radio" !== b.type && "checkbox" !== b.type) return a.handleObj.handler.apply(this, arguments)
        },
        teardown: function () {
            return ea.event.remove(this, "._change"), !Ea.test(this.nodeName)
        }
    }), ca.focusinBubbles || ea.each({
        focus: "focusin",
        blur: "focusout"
    }, function (a, b) {
        var c = function (a) {
            ea.event.simulate(b, a.target, ea.event.fix(a), !0)
        };
        ea.event.special[b] = {
            setup: function () {
                var d = this.ownerDocument || this,
                    e = ea._data(d, b);
                e || d.addEventListener(a, c, !0), ea._data(d, b, (e || 0) + 1)
            },
            teardown: function () {
                var d = this.ownerDocument || this,
                    e = ea._data(d, b) - 1;
                e ? ea._data(d, b, e) : (d.removeEventListener(a, c, !0), ea._removeData(d, b))
            }
        }
    }), ea.fn.extend({
        on: function (a, b, c, d, e) {
            var f, g;
            if ("object" == typeof a) {
                "string" != typeof b && (c = c || b, b = void 0);
                for (f in a) this.on(f, b, c, a[f], e);
                return this
            }
            if (null == c && null == d ? (d = b, c = b = void 0) : null == d && ("string" == typeof b ? (d = c, c = void 0) : (d = c, c = b, b = void 0)), !1 === d) d = n;
            else if (!d) return this;
            return 1 === e && (g = d, d = function (a) {
                return ea().off(a), g.apply(this, arguments)
            }, d.guid = g.guid || (g.guid = ea.guid++)), this.each(function () {
                ea.event.add(this, a, d, c, b)
            })
        },
        one: function (a, b, c, d) {
            return this.on(a, b, c, d, 1)
        },
        off: function (a, b, c) {
            var d, e;
            if (a && a.preventDefault && a.handleObj) return d = a.handleObj, ea(a.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler), this;
            if ("object" == typeof a) {
                for (e in a) this.off(e, b, a[e]);
                return this
            }
            return !1 !== b && "function" != typeof b || (c = b, b = void 0), !1 === c && (c = n), this.each(function () {
                ea.event.remove(this, a, c, b)
            })
        },
        trigger: function (a, b) {
            return this.each(function () {
                ea.event.trigger(a, b, this)
            })
        },
        triggerHandler: function (a, b) {
            var c = this[0];
            if (c) return ea.event.trigger(a, b, c, !0)
        }
    });
    var Ja = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
        Ka = / jQuery\d+="(?:null|\d+)"/g,
        La = new RegExp("<(?:" + Ja + ")[\\s/>]", "i"),
        Ma = /^\s+/,
        Na = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
        Oa = /<([\w:]+)/,
        Pa = /<tbody/i,
        Qa = /<|&#?\w+;/,
        Ra = /<(?:script|style|link)/i,
        Sa = /checked\s*(?:[^=]|=\s*.checked.)/i,
        Ta = /^$|\/(?:java|ecma)script/i,
        Ua = /^true\/(.*)/,
        Va = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
        Wa = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            legend: [1, "<fieldset>", "</fieldset>"],
            area: [1, "<map>", "</map>"],
            param: [1, "<object>", "</object>"],
            thead: [1, "<table>", "</table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            _default: ca.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
        },
        Xa = p(oa),
        Ya = Xa.appendChild(oa.createElement("div"));
    Wa.optgroup = Wa.option, Wa.tbody = Wa.tfoot = Wa.colgroup = Wa.caption = Wa.thead, Wa.th = Wa.td, ea.extend({
        clone: function (a, b, c) {
            var d, e, f, g, h, i = ea.contains(a.ownerDocument, a);
            if (ca.html5Clone || ea.isXMLDoc(a) || !La.test("<" + a.nodeName + ">") ? f = a.cloneNode(!0) : (Ya.innerHTML = a.outerHTML, Ya.removeChild(f = Ya.firstChild)), !(ca.noCloneEvent && ca.noCloneChecked || 1 !== a.nodeType && 11 !== a.nodeType || ea.isXMLDoc(a)))
                for (d = q(f), h = q(a), g = 0; null != (e = h[g]); ++g) d[g] && x(e, d[g]);
            if (b)
                if (c)
                    for (h = h || q(a), d = d || q(f), g = 0; null != (e = h[g]); g++) w(e, d[g]);
                else w(a, f);
            return d = q(f, "script"), d.length > 0 && v(d, !i && q(a, "script")), d = h = e = null, f
        },
        buildFragment: function (a, b, c, d) {
            for (var e, f, g, h, i, j, k, l = a.length, m = p(b), n = [], o = 0; o < l; o++)
                if ((f = a[o]) || 0 === f)
                    if ("object" === ea.type(f)) ea.merge(n, f.nodeType ? [f] : f);
                    else if (Qa.test(f)) {
                        for (h = h || m.appendChild(b.createElement("div")), i = (Oa.exec(f) || ["", ""])[1].toLowerCase(), k = Wa[i] || Wa._default, h.innerHTML = k[1] + f.replace(Na, "<$1></$2>") + k[2], e = k[0]; e--;) h = h.lastChild;
                        if (!ca.leadingWhitespace && Ma.test(f) && n.push(b.createTextNode(Ma.exec(f)[0])), !ca.tbody)
                            for (f = "table" !== i || Pa.test(f) ? "<table>" !== k[1] || Pa.test(f) ? 0 : h : h.firstChild, e = f && f.childNodes.length; e--;) ea.nodeName(j = f.childNodes[e], "tbody") && !j.childNodes.length && f.removeChild(j);
                        for (ea.merge(n, h.childNodes), h.textContent = ""; h.firstChild;) h.removeChild(h.firstChild);
                        h = m.lastChild
                    } else n.push(b.createTextNode(f));
            for (h && m.removeChild(h), ca.appendChecked || ea.grep(q(n, "input"), r), o = 0; f = n[o++];)
                if ((!d || -1 === ea.inArray(f, d)) && (g = ea.contains(f.ownerDocument, f), h = q(m.appendChild(f), "script"), g && v(h), c))
                    for (e = 0; f = h[e++];) Ta.test(f.type || "") && c.push(f);
            return h = null, m
        },
        cleanData: function (a, b) {
            for (var c, d, e, f, g = 0, h = ea.expando, i = ea.cache, j = ca.deleteExpando, k = ea.event.special; null != (c = a[g]); g++)
                if ((b || ea.acceptData(c)) && (e = c[h], f = e && i[e])) {
                    if (f.events)
                        for (d in f.events) k[d] ? ea.event.remove(c, d) : ea.removeEvent(c, d, f.handle);
                    i[e] && (delete i[e], j ? delete c[h] : typeof c.removeAttribute !== wa ? c.removeAttribute(h) : c[h] = null, W.push(e))
                }
        }
    }), ea.fn.extend({
        text: function (a) {
            return Ca(this, function (a) {
                return void 0 === a ? ea.text(this) : this.empty().append((this[0] && this[0].ownerDocument || oa).createTextNode(a))
            }, null, a, arguments.length)
        },
        append: function () {
            return this.domManip(arguments, function (a) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    s(this, a).appendChild(a)
                }
            })
        },
        prepend: function () {
            return this.domManip(arguments, function (a) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var b = s(this, a);
                    b.insertBefore(a, b.firstChild)
                }
            })
        },
        before: function () {
            return this.domManip(arguments, function (a) {
                this.parentNode && this.parentNode.insertBefore(a, this)
            })
        },
        after: function () {
            return this.domManip(arguments, function (a) {
                this.parentNode && this.parentNode.insertBefore(a, this.nextSibling)
            })
        },
        remove: function (a, b) {
            for (var c, d = a ? ea.filter(a, this) : this, e = 0; null != (c = d[e]); e++) b || 1 !== c.nodeType || ea.cleanData(q(c)), c.parentNode && (b && ea.contains(c.ownerDocument, c) && v(q(c, "script")), c.parentNode.removeChild(c));
            return this
        },
        empty: function () {
            for (var a, b = 0; null != (a = this[b]); b++) {
                for (1 === a.nodeType && ea.cleanData(q(a, !1)); a.firstChild;) a.removeChild(a.firstChild);
                a.options && ea.nodeName(a, "select") && (a.options.length = 0)
            }
            return this
        },
        clone: function (a, b) {
            return a = null != a && a, b = null == b ? a : b, this.map(function () {
                return ea.clone(this, a, b)
            })
        },
        html: function (a) {
            return Ca(this, function (a) {
                var b = this[0] || {},
                    c = 0,
                    d = this.length;
                if (void 0 === a) return 1 === b.nodeType ? b.innerHTML.replace(Ka, "") : void 0;
                if ("string" == typeof a && !Ra.test(a) && (ca.htmlSerialize || !La.test(a)) && (ca.leadingWhitespace || !Ma.test(a)) && !Wa[(Oa.exec(a) || ["", ""])[1].toLowerCase()]) {
                    a = a.replace(Na, "<$1></$2>");
                    try {
                        for (; c < d; c++) b = this[c] || {}, 1 === b.nodeType && (ea.cleanData(q(b, !1)), b.innerHTML = a);
                        b = 0
                    } catch (e) {}
                }
                b && this.empty().append(a)
            }, null, a, arguments.length)
        },
        replaceWith: function () {
            var a = arguments[0];
            return this.domManip(arguments, function (b) {
                a = this.parentNode, ea.cleanData(q(this)), a && a.replaceChild(b, this)
            }), a && (a.length || a.nodeType) ? this : this.remove()
        },
        detach: function (a) {
            return this.remove(a, !0)
        },
        domManip: function (a, b) {
            a = Y.apply([], a);
            var c, d, e, f, g, h, i = 0,
                j = this.length,
                k = this,
                l = j - 1,
                m = a[0],
                n = ea.isFunction(m);
            if (n || j > 1 && "string" == typeof m && !ca.checkClone && Sa.test(m)) return this.each(function (c) {
                var d = k.eq(c);
                n && (a[0] = m.call(this, c, d.html())), d.domManip(a, b)
            });
            if (j && (h = ea.buildFragment(a, this[0].ownerDocument, !1, this), c = h.firstChild, 1 === h.childNodes.length && (h = c), c)) {
                for (f = ea.map(q(h, "script"), t), e = f.length; i < j; i++) d = h, i !== l && (d = ea.clone(d, !0, !0), e && ea.merge(f, q(d, "script"))), b.call(this[i], d, i);
                if (e)
                    for (g = f[f.length - 1].ownerDocument, ea.map(f, u), i = 0; i < e; i++) d = f[i], Ta.test(d.type || "") && !ea._data(d, "globalEval") && ea.contains(g, d) && (d.src ? ea._evalUrl && ea._evalUrl(d.src) : ea.globalEval((d.text || d.textContent || d.innerHTML || "").replace(Va, "")));
                h = c = null
            }
            return this
        }
    }), ea.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function (a, b) {
        ea.fn[a] = function (a) {
            for (var c, d = 0, e = [], f = ea(a), g = f.length - 1; d <= g; d++) c = d === g ? this : this.clone(!0), ea(f[d])[b](c), Z.apply(e, c.get());
            return this.pushStack(e)
        }
    });
    var Za, $a = {};
    ! function () {
        var a;
        ca.shrinkWrapBlocks = function () {
            if (null != a) return a;
            a = !1;
            var b, c, d;
            return (c = oa.getElementsByTagName("body")[0]) && c.style ? (b = oa.createElement("div"), d = oa.createElement("div"), d.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", c.appendChild(d).appendChild(b), typeof b.style.zoom !== wa && (b.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1", b.appendChild(oa.createElement("div")).style.width = "5px", a = 3 !== b.offsetWidth), c.removeChild(d), a) : void 0
        }
    }();
    var _a, ab, bb = /^margin/,
        cb = new RegExp("^(" + za + ")(?!px)[a-z%]+$", "i"),
        db = /^(top|right|bottom|left)$/;
    a.getComputedStyle ? (_a = function (b) {
        return b.ownerDocument.defaultView.opener ? b.ownerDocument.defaultView.getComputedStyle(b, null) : a.getComputedStyle(b, null)
    }, ab = function (a, b, c) {
        var d, e, f, g, h = a.style;
        return c = c || _a(a), g = c ? c.getPropertyValue(b) || c[b] : void 0, c && ("" !== g || ea.contains(a.ownerDocument, a) || (g = ea.style(a, b)), cb.test(g) && bb.test(b) && (d = h.width, e = h.minWidth, f = h.maxWidth, h.minWidth = h.maxWidth = h.width = g, g = c.width, h.width = d, h.minWidth = e, h.maxWidth = f)), void 0 === g ? g : g + ""
    }) : oa.documentElement.currentStyle && (_a = function (a) {
        return a.currentStyle
    }, ab = function (a, b, c) {
        var d, e, f, g, h = a.style;
        return c = c || _a(a), g = c ? c[b] : void 0, null == g && h && h[b] && (g = h[b]), cb.test(g) && !db.test(b) && (d = h.left, e = a.runtimeStyle, f = e && e.left, f && (e.left = a.currentStyle.left), h.left = "fontSize" === b ? "1em" : g, g = h.pixelLeft + "px", h.left = d, f && (e.left = f)), void 0 === g ? g : g + "" || "auto"
    }),
        function () {
            function b() {
                var b, c, d, e;
                (c = oa.getElementsByTagName("body")[0]) && c.style && (b = oa.createElement("div"), d = oa.createElement("div"), d.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", c.appendChild(d).appendChild(b), b.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", f = g = !1, i = !0, a.getComputedStyle && (f = "1%" !== (a.getComputedStyle(b, null) || {}).top, g = "4px" === (a.getComputedStyle(b, null) || {
                    width: "4px"
                }).width, e = b.appendChild(oa.createElement("div")), e.style.cssText = b.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", e.style.marginRight = e.style.width = "0", b.style.width = "1px", i = !parseFloat((a.getComputedStyle(e, null) || {}).marginRight), b.removeChild(e)), b.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", e = b.getElementsByTagName("td"), e[0].style.cssText = "margin:0;border:0;padding:0;display:none", h = 0 === e[0].offsetHeight, h && (e[0].style.display = "", e[1].style.display = "none", h = 0 === e[0].offsetHeight), c.removeChild(d))
            }
            var c, d, e, f, g, h, i;
            c = oa.createElement("div"), c.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", e = c.getElementsByTagName("a")[0], (d = e && e.style) && (d.cssText = "float:left;opacity:.5", ca.opacity = "0.5" === d.opacity, ca.cssFloat = !!d.cssFloat, c.style.backgroundClip = "content-box", c.cloneNode(!0).style.backgroundClip = "", ca.clearCloneStyle = "content-box" === c.style.backgroundClip, ca.boxSizing = "" === d.boxSizing || "" === d.MozBoxSizing || "" === d.WebkitBoxSizing, ea.extend(ca, {
                reliableHiddenOffsets: function () {
                    return null == h && b(), h
                },
                boxSizingReliable: function () {
                    return null == g && b(), g
                },
                pixelPosition: function () {
                    return null == f && b(), f
                },
                reliableMarginRight: function () {
                    return null == i && b(), i
                }
            }))
        }(), ea.swap = function (a, b, c, d) {
        var e, f, g = {};
        for (f in b) g[f] = a.style[f], a.style[f] = b[f];
        e = c.apply(a, d || []);
        for (f in b) a.style[f] = g[f];
        return e
    };
    var eb = /alpha\([^)]*\)/i,
        fb = /opacity\s*=\s*([^)]*)/,
        gb = /^(none|table(?!-c[ea]).+)/,
        hb = new RegExp("^(" + za + ")(.*)$", "i"),
        ib = new RegExp("^([+-])=(" + za + ")", "i"),
        jb = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        kb = {
            letterSpacing: "0",
            fontWeight: "400"
        },
        lb = ["Webkit", "O", "Moz", "ms"];
    ea.extend({
        cssHooks: {
            opacity: {
                get: function (a, b) {
                    if (b) {
                        var c = ab(a, "opacity");
                        return "" === c ? "1" : c
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            float: ca.cssFloat ? "cssFloat" : "styleFloat"
        },
        style: function (a, b, c, d) {
            if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {
                var e, f, g, h = ea.camelCase(b),
                    i = a.style;
                if (b = ea.cssProps[h] || (ea.cssProps[h] = B(i, h)), g = ea.cssHooks[b] || ea.cssHooks[h], void 0 === c) return g && "get" in g && void 0 !== (e = g.get(a, !1, d)) ? e : i[b];
                if (f = typeof c, "string" === f && (e = ib.exec(c)) && (c = (e[1] + 1) * e[2] + parseFloat(ea.css(a, b)), f = "number"), null != c && c === c && ("number" !== f || ea.cssNumber[h] || (c += "px"), ca.clearCloneStyle || "" !== c || 0 !== b.indexOf("background") || (i[b] = "inherit"), !(g && "set" in g && void 0 === (c = g.set(a, c, d))))) try {
                    i[b] = c
                } catch (j) {}
            }
        },
        css: function (a, b, c, d) {
            var e, f, g, h = ea.camelCase(b);
            return b = ea.cssProps[h] || (ea.cssProps[h] = B(a.style, h)), g = ea.cssHooks[b] || ea.cssHooks[h], g && "get" in g && (f = g.get(a, !0, c)), void 0 === f && (f = ab(a, b, d)), "normal" === f && b in kb && (f = kb[b]), "" === c || c ? (e = parseFloat(f), !0 === c || ea.isNumeric(e) ? e || 0 : f) : f
        }
    }), ea.each(["height", "width"], function (a, b) {
        ea.cssHooks[b] = {
            get: function (a, c, d) {
                if (c) return gb.test(ea.css(a, "display")) && 0 === a.offsetWidth ? ea.swap(a, jb, function () {
                    return F(a, b, d)
                }) : F(a, b, d)
            },
            set: function (a, c, d) {
                var e = d && _a(a);
                return D(a, c, d ? E(a, b, d, ca.boxSizing && "border-box" === ea.css(a, "boxSizing", !1, e), e) : 0)
            }
        }
    }), ca.opacity || (ea.cssHooks.opacity = {
        get: function (a, b) {
            return fb.test((b && a.currentStyle ? a.currentStyle.filter : a.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : b ? "1" : ""
        },
        set: function (a, b) {
            var c = a.style,
                d = a.currentStyle,
                e = ea.isNumeric(b) ? "alpha(opacity=" + 100 * b + ")" : "",
                f = d && d.filter || c.filter || "";
            c.zoom = 1, (b >= 1 || "" === b) && "" === ea.trim(f.replace(eb, "")) && c.removeAttribute && (c.removeAttribute("filter"), "" === b || d && !d.filter) || (c.filter = eb.test(f) ? f.replace(eb, e) : f + " " + e)
        }
    }), ea.cssHooks.marginRight = A(ca.reliableMarginRight, function (a, b) {
        if (b) return ea.swap(a, {
            display: "inline-block"
        }, ab, [a, "marginRight"])
    }), ea.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function (a, b) {
        ea.cssHooks[a + b] = {
            expand: function (c) {
                for (var d = 0, e = {}, f = "string" == typeof c ? c.split(" ") : [c]; d < 4; d++) e[a + Aa[d] + b] = f[d] || f[d - 2] || f[0];
                return e
            }
        }, bb.test(a) || (ea.cssHooks[a + b].set = D)
    }), ea.fn.extend({
        css: function (a, b) {
            return Ca(this, function (a, b, c) {
                var d, e, f = {},
                    g = 0;
                if (ea.isArray(b)) {
                    for (d = _a(a), e = b.length; g < e; g++) f[b[g]] = ea.css(a, b[g], !1, d);
                    return f
                }
                return void 0 !== c ? ea.style(a, b, c) : ea.css(a, b)
            }, a, b, arguments.length > 1)
        },
        show: function () {
            return C(this, !0)
        },
        hide: function () {
            return C(this)
        },
        toggle: function (a) {
            return "boolean" == typeof a ? a ? this.show() : this.hide() : this.each(function () {
                Ba(this) ? ea(this).show() : ea(this).hide()
            })
        }
    }), ea.Tween = G, G.prototype = {
        constructor: G,
        init: function (a, b, c, d, e, f) {
            this.elem = a, this.prop = c, this.easing = e || "swing", this.options = b, this.start = this.now = this.cur(), this.end = d, this.unit = f || (ea.cssNumber[c] ? "" : "px")
        },
        cur: function () {
            var a = G.propHooks[this.prop];
            return a && a.get ? a.get(this) : G.propHooks._default.get(this)
        },
        run: function (a) {
            var b, c = G.propHooks[this.prop];
            return this.options.duration ? this.pos = b = ea.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration) : this.pos = b = a, this.now = (this.end - this.start) * b + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), c && c.set ? c.set(this) : G.propHooks._default.set(this), this
        }
    }, G.prototype.init.prototype = G.prototype, G.propHooks = {
        _default: {
            get: function (a) {
                var b;
                return null == a.elem[a.prop] || a.elem.style && null != a.elem.style[a.prop] ? (b = ea.css(a.elem, a.prop, ""), b && "auto" !== b ? b : 0) : a.elem[a.prop]
            },
            set: function (a) {
                ea.fx.step[a.prop] ? ea.fx.step[a.prop](a) : a.elem.style && (null != a.elem.style[ea.cssProps[a.prop]] || ea.cssHooks[a.prop]) ? ea.style(a.elem, a.prop, a.now + a.unit) : a.elem[a.prop] = a.now
            }
        }
    }, G.propHooks.scrollTop = G.propHooks.scrollLeft = {
        set: function (a) {
            a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now)
        }
    }, ea.easing = {
        linear: function (a) {
            return a
        },
        swing: function (a) {
            return .5 - Math.cos(a * Math.PI) / 2
        }
    }, ea.fx = G.prototype.init, ea.fx.step = {};
    var mb, nb, ob = /^(?:toggle|show|hide)$/,
        pb = new RegExp("^(?:([+-])=|)(" + za + ")([a-z%]*)$", "i"),
        qb = /queueHooks$/,
        rb = [K],
        sb = {
            "*": [
                function (a, b) {
                    var c = this.createTween(a, b),
                        d = c.cur(),
                        e = pb.exec(b),
                        f = e && e[3] || (ea.cssNumber[a] ? "" : "px"),
                        g = (ea.cssNumber[a] || "px" !== f && +d) && pb.exec(ea.css(c.elem, a)),
                        h = 1,
                        i = 20;
                    if (g && g[3] !== f) {
                        f = f || g[3], e = e || [], g = +d || 1;
                        do {
                            h = h || ".5", g /= h, ea.style(c.elem, a, g + f)
                        } while (h !== (h = c.cur() / d) && 1 !== h && --i)
                    }
                    return e && (g = c.start = +g || +d || 0, c.unit = f, c.end = e[1] ? g + (e[1] + 1) * e[2] : +e[2]), c
                }
            ]
        };
    ea.Animation = ea.extend(M, {
        tweener: function (a, b) {
            ea.isFunction(a) ? (b = a, a = ["*"]) : a = a.split(" ");
            for (var c, d = 0, e = a.length; d < e; d++) c = a[d], sb[c] = sb[c] || [], sb[c].unshift(b)
        },
        prefilter: function (a, b) {
            b ? rb.unshift(a) : rb.push(a)
        }
    }), ea.speed = function (a, b, c) {
        var d = a && "object" == typeof a ? ea.extend({}, a) : {
            complete: c || !c && b || ea.isFunction(a) && a,
            duration: a,
            easing: c && b || b && !ea.isFunction(b) && b
        };
        return d.duration = ea.fx.off ? 0 : "number" == typeof d.duration ? d.duration : d.duration in ea.fx.speeds ? ea.fx.speeds[d.duration] : ea.fx.speeds._default, null != d.queue && !0 !== d.queue || (d.queue = "fx"), d.old = d.complete, d.complete = function () {
            ea.isFunction(d.old) && d.old.call(this), d.queue && ea.dequeue(this, d.queue)
        }, d
    }, ea.fn.extend({
        fadeTo: function (a, b, c, d) {
            return this.filter(Ba).css("opacity", 0).show().end().animate({
                opacity: b
            }, a, c, d)
        },
        animate: function (a, b, c, d) {
            var e = ea.isEmptyObject(a),
                f = ea.speed(b, c, d),
                g = function () {
                    var b = M(this, ea.extend({}, a), f);
                    (e || ea._data(this, "finish")) && b.stop(!0)
                };
            return g.finish = g, e || !1 === f.queue ? this.each(g) : this.queue(f.queue, g)
        },
        stop: function (a, b, c) {
            var d = function (a) {
                var b = a.stop;
                delete a.stop, b(c)
            };
            return "string" != typeof a && (c = b, b = a, a = void 0), b && !1 !== a && this.queue(a || "fx", []), this.each(function () {
                var b = !0,
                    e = null != a && a + "queueHooks",
                    f = ea.timers,
                    g = ea._data(this);
                if (e) g[e] && g[e].stop && d(g[e]);
                else
                    for (e in g) g[e] && g[e].stop && qb.test(e) && d(g[e]);
                for (e = f.length; e--;) f[e].elem !== this || null != a && f[e].queue !== a || (f[e].anim.stop(c), b = !1, f.splice(e, 1));
                !b && c || ea.dequeue(this, a)
            })
        },
        finish: function (a) {
            return !1 !== a && (a = a || "fx"), this.each(function () {
                var b, c = ea._data(this),
                    d = c[a + "queue"],
                    e = c[a + "queueHooks"],
                    f = ea.timers,
                    g = d ? d.length : 0;
                for (c.finish = !0, ea.queue(this, a, []), e && e.stop && e.stop.call(this, !0), b = f.length; b--;) f[b].elem === this && f[b].queue === a && (f[b].anim.stop(!0), f.splice(b, 1));
                for (b = 0; b < g; b++) d[b] && d[b].finish && d[b].finish.call(this);
                delete c.finish
            })
        }
    }), ea.each(["toggle", "show", "hide"], function (a, b) {
        var c = ea.fn[b];
        ea.fn[b] = function (a, d, e) {
            return null == a || "boolean" == typeof a ? c.apply(this, arguments) : this.animate(I(b, !0), a, d, e)
        }
    }), ea.each({
        slideDown: I("show"),
        slideUp: I("hide"),
        slideToggle: I("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function (a, b) {
        ea.fn[a] = function (a, c, d) {
            return this.animate(b, a, c, d)
        }
    }), ea.timers = [], ea.fx.tick = function () {
        var a, b = ea.timers,
            c = 0;
        for (mb = ea.now(); c < b.length; c++)(a = b[c])() || b[c] !== a || b.splice(c--, 1);
        b.length || ea.fx.stop(), mb = void 0
    }, ea.fx.timer = function (a) {
        ea.timers.push(a), a() ? ea.fx.start() : ea.timers.pop()
    }, ea.fx.interval = 13, ea.fx.start = function () {
        nb || (nb = setInterval(ea.fx.tick, ea.fx.interval))
    }, ea.fx.stop = function () {
        clearInterval(nb), nb = null
    }, ea.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    }, ea.fn.delay = function (a, b) {
        return a = ea.fx ? ea.fx.speeds[a] || a : a, b = b || "fx", this.queue(b, function (b, c) {
            var d = setTimeout(b, a);
            c.stop = function () {
                clearTimeout(d)
            }
        })
    },
        function () {
            var a, b, c, d, e;
            b = oa.createElement("div"), b.setAttribute("className", "t"), b.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", d = b.getElementsByTagName("a")[0], c = oa.createElement("select"), e = c.appendChild(oa.createElement("option")), a = b.getElementsByTagName("input")[0], d.style.cssText = "top:1px", ca.getSetAttribute = "t" !== b.className, ca.style = /top/.test(d.getAttribute("style")), ca.hrefNormalized = "/a" === d.getAttribute("href"), ca.checkOn = !!a.value, ca.optSelected = e.selected, ca.enctype = !!oa.createElement("form").enctype, c.disabled = !0, ca.optDisabled = !e.disabled, a = oa.createElement("input"), a.setAttribute("value", ""), ca.input = "" === a.getAttribute("value"), a.value = "t", a.setAttribute("type", "radio"), ca.radioValue = "t" === a.value
        }();
    var tb = /\r/g;
    ea.fn.extend({
        val: function (a) {
            var b, c, d, e = this[0]; {
                if (arguments.length) return d = ea.isFunction(a), this.each(function (c) {
                    var e;
                    1 === this.nodeType && (e = d ? a.call(this, c, ea(this).val()) : a, null == e ? e = "" : "number" == typeof e ? e += "" : ea.isArray(e) && (e = ea.map(e, function (a) {
                        return null == a ? "" : a + ""
                    })), (b = ea.valHooks[this.type] || ea.valHooks[this.nodeName.toLowerCase()]) && "set" in b && void 0 !== b.set(this, e, "value") || (this.value = e))
                });
                if (e) return (b = ea.valHooks[e.type] || ea.valHooks[e.nodeName.toLowerCase()]) && "get" in b && void 0 !== (c = b.get(e, "value")) ? c : (c = e.value, "string" == typeof c ? c.replace(tb, "") : null == c ? "" : c)
            }
        }
    }), ea.extend({
        valHooks: {
            option: {
                get: function (a) {
                    var b = ea.find.attr(a, "value");
                    return null != b ? b : ea.trim(ea.text(a))
                }
            },
            select: {
                get: function (a) {
                    for (var b, c, d = a.options, e = a.selectedIndex, f = "select-one" === a.type || e < 0, g = f ? null : [], h = f ? e + 1 : d.length, i = e < 0 ? h : f ? e : 0; i < h; i++)
                        if (c = d[i], (c.selected || i === e) && (ca.optDisabled ? !c.disabled : null === c.getAttribute("disabled")) && (!c.parentNode.disabled || !ea.nodeName(c.parentNode, "optgroup"))) {
                            if (b = ea(c).val(), f) return b;
                            g.push(b)
                        }
                    return g
                },
                set: function (a, b) {
                    for (var c, d, e = a.options, f = ea.makeArray(b), g = e.length; g--;)
                        if (d = e[g], ea.inArray(ea.valHooks.option.get(d), f) >= 0) try {
                            d.selected = c = !0
                        } catch (h) {
                            d.scrollHeight
                        } else d.selected = !1;
                    return c || (a.selectedIndex = -1), e
                }
            }
        }
    }), ea.each(["radio", "checkbox"], function () {
        ea.valHooks[this] = {
            set: function (a, b) {
                if (ea.isArray(b)) return a.checked = ea.inArray(ea(a).val(), b) >= 0
            }
        }, ca.checkOn || (ea.valHooks[this].get = function (a) {
            return null === a.getAttribute("value") ? "on" : a.value
        })
    });
    var ub, vb, wb = ea.expr.attrHandle,
        xb = /^(?:checked|selected)$/i,
        yb = ca.getSetAttribute,
        zb = ca.input;
    ea.fn.extend({
        attr: function (a, b) {
            return Ca(this, ea.attr, a, b, arguments.length > 1)
        },
        removeAttr: function (a) {
            return this.each(function () {
                ea.removeAttr(this, a)
            })
        }
    }), ea.extend({
        attr: function (a, b, c) {
            var d, e, f = a.nodeType;
            if (a && 3 !== f && 8 !== f && 2 !== f) return typeof a.getAttribute === wa ? ea.prop(a, b, c) : (1 === f && ea.isXMLDoc(a) || (b = b.toLowerCase(), d = ea.attrHooks[b] || (ea.expr.match.bool.test(b) ? vb : ub)), void 0 === c ? d && "get" in d && null !== (e = d.get(a, b)) ? e : (e = ea.find.attr(a, b), null == e ? void 0 : e) : null !== c ? d && "set" in d && void 0 !== (e = d.set(a, c, b)) ? e : (a.setAttribute(b, c + ""), c) : void ea.removeAttr(a, b))
        },
        removeAttr: function (a, b) {
            var c, d, e = 0,
                f = b && b.match(sa);
            if (f && 1 === a.nodeType)
                for (; c = f[e++];) d = ea.propFix[c] || c, ea.expr.match.bool.test(c) ? zb && yb || !xb.test(c) ? a[d] = !1 : a[ea.camelCase("default-" + c)] = a[d] = !1 : ea.attr(a, c, ""), a.removeAttribute(yb ? c : d)
        },
        attrHooks: {
            type: {
                set: function (a, b) {
                    if (!ca.radioValue && "radio" === b && ea.nodeName(a, "input")) {
                        var c = a.value;
                        return a.setAttribute("type", b), c && (a.value = c), b
                    }
                }
            }
        }
    }), vb = {
        set: function (a, b, c) {
            return !1 === b ? ea.removeAttr(a, c) : zb && yb || !xb.test(c) ? a.setAttribute(!yb && ea.propFix[c] || c, c) : a[ea.camelCase("default-" + c)] = a[c] = !0, c
        }
    }, ea.each(ea.expr.match.bool.source.match(/\w+/g), function (a, b) {
        var c = wb[b] || ea.find.attr;
        wb[b] = zb && yb || !xb.test(b) ? function (a, b, d) {
            var e, f;
            return d || (f = wb[b], wb[b] = e, e = null != c(a, b, d) ? b.toLowerCase() : null, wb[b] = f), e
        } : function (a, b, c) {
            if (!c) return a[ea.camelCase("default-" + b)] ? b.toLowerCase() : null
        }
    }), zb && yb || (ea.attrHooks.value = {
        set: function (a, b, c) {
            if (!ea.nodeName(a, "input")) return ub && ub.set(a, b, c);
            a.defaultValue = b
        }
    }), yb || (ub = {
        set: function (a, b, c) {
            var d = a.getAttributeNode(c);
            if (d || a.setAttributeNode(d = a.ownerDocument.createAttribute(c)), d.value = b += "", "value" === c || b === a.getAttribute(c)) return b
        }
    }, wb.id = wb.name = wb.coords = function (a, b, c) {
        var d;
        if (!c) return (d = a.getAttributeNode(b)) && "" !== d.value ? d.value : null
    }, ea.valHooks.button = {
        get: function (a, b) {
            var c = a.getAttributeNode(b);
            if (c && c.specified) return c.value
        },
        set: ub.set
    }, ea.attrHooks.contenteditable = {
        set: function (a, b, c) {
            ub.set(a, "" !== b && b, c)
        }
    }, ea.each(["width", "height"], function (a, b) {
        ea.attrHooks[b] = {
            set: function (a, c) {
                if ("" === c) return a.setAttribute(b, "auto"), c
            }
        }
    })), ca.style || (ea.attrHooks.style = {
        get: function (a) {
            return a.style.cssText || void 0
        },
        set: function (a, b) {
            return a.style.cssText = b + ""
        }
    });
    var Ab = /^(?:input|select|textarea|button|object)$/i,
        Bb = /^(?:a|area)$/i;
    ea.fn.extend({
        prop: function (a, b) {
            return Ca(this, ea.prop, a, b, arguments.length > 1)
        },
        removeProp: function (a) {
            return a = ea.propFix[a] || a, this.each(function () {
                try {
                    this[a] = void 0, delete this[a]
                } catch (b) {}
            })
        }
    }), ea.extend({
        propFix: {
            for: "htmlFor",
            class: "className"
        },
        prop: function (a, b, c) {
            var d, e, f, g = a.nodeType;
            if (a && 3 !== g && 8 !== g && 2 !== g) return f = 1 !== g || !ea.isXMLDoc(a), f && (b = ea.propFix[b] || b, e = ea.propHooks[b]), void 0 !== c ? e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : a[b] = c : e && "get" in e && null !== (d = e.get(a, b)) ? d : a[b]
        },
        propHooks: {
            tabIndex: {
                get: function (a) {
                    var b = ea.find.attr(a, "tabindex");
                    return b ? parseInt(b, 10) : Ab.test(a.nodeName) || Bb.test(a.nodeName) && a.href ? 0 : -1
                }
            }
        }
    }), ca.hrefNormalized || ea.each(["href", "src"], function (a, b) {
        ea.propHooks[b] = {
            get: function (a) {
                return a.getAttribute(b, 4)
            }
        }
    }), ca.optSelected || (ea.propHooks.selected = {
        get: function (a) {
            var b = a.parentNode;
            return b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex), null
        }
    }), ea.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
        ea.propFix[this.toLowerCase()] = this
    }), ca.enctype || (ea.propFix.enctype = "encoding");
    var Cb = /[\t\r\n\f]/g;
    ea.fn.extend({
        addClass: function (a) {
            var b, c, d, e, f, g, h = 0,
                i = this.length,
                j = "string" == typeof a && a;
            if (ea.isFunction(a)) return this.each(function (b) {
                ea(this).addClass(a.call(this, b, this.className))
            });
            if (j)
                for (b = (a || "").match(sa) || []; h < i; h++)
                    if (c = this[h], d = 1 === c.nodeType && (c.className ? (" " + c.className + " ").replace(Cb, " ") : " ")) {
                        for (f = 0; e = b[f++];) d.indexOf(" " + e + " ") < 0 && (d += e + " ");
                        g = ea.trim(d), c.className !== g && (c.className = g)
                    }
            return this
        },
        removeClass: function (a) {
            var b, c, d, e, f, g, h = 0,
                i = this.length,
                j = 0 === arguments.length || "string" == typeof a && a;
            if (ea.isFunction(a)) return this.each(function (b) {
                ea(this).removeClass(a.call(this, b, this.className))
            });
            if (j)
                for (b = (a || "").match(sa) || []; h < i; h++)
                    if (c = this[h], d = 1 === c.nodeType && (c.className ? (" " + c.className + " ").replace(Cb, " ") : "")) {
                        for (f = 0; e = b[f++];)
                            for (; d.indexOf(" " + e + " ") >= 0;) d = d.replace(" " + e + " ", " ");
                        g = a ? ea.trim(d) : "", c.className !== g && (c.className = g)
                    }
            return this
        },
        toggleClass: function (a, b) {
            var c = typeof a;
            return "boolean" == typeof b && "string" === c ? b ? this.addClass(a) : this.removeClass(a) : ea.isFunction(a) ? this.each(function (c) {
                ea(this).toggleClass(a.call(this, c, this.className, b), b)
            }) : this.each(function () {
                if ("string" === c)
                    for (var b, d = 0, e = ea(this), f = a.match(sa) || []; b = f[d++];) e.hasClass(b) ? e.removeClass(b) : e.addClass(b);
                else c !== wa && "boolean" !== c || (this.className && ea._data(this, "__className__", this.className), this.className = this.className || !1 === a ? "" : ea._data(this, "__className__") || "")
            })
        },
        hasClass: function (a) {
            for (var b = " " + a + " ", c = 0, d = this.length; c < d; c++)
                if (1 === this[c].nodeType && (" " + this[c].className + " ").replace(Cb, " ").indexOf(b) >= 0) return !0;
            return !1
        }
    }), ea.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (a, b) {
        ea.fn[b] = function (a, c) {
            return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b)
        }
    }), ea.fn.extend({
        hover: function (a, b) {
            return this.mouseenter(a).mouseleave(b || a)
        },
        bind: function (a, b, c) {
            return this.on(a, null, b, c)
        },
        unbind: function (a, b) {
            return this.off(a, null, b)
        },
        delegate: function (a, b, c, d) {
            return this.on(b, a, c, d)
        },
        undelegate: function (a, b, c) {
            return 1 === arguments.length ? this.off(a, "**") : this.off(b, a || "**", c)
        }
    });
    var Db = ea.now(),
        Eb = /\?/,
        Fb = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    ea.parseJSON = function (b) {
        if (a.JSON && a.JSON.parse) return a.JSON.parse(b + "");
        var c, d = null,
            e = ea.trim(b + "");
        return e && !ea.trim(e.replace(Fb, function (a, b, e, f) {
            return c && b && (d = 0), 0 === d ? a : (c = e || b, d += !f - !e, "")
        })) ? Function("return " + e)() : ea.error("Invalid JSON: " + b)
    }, ea.parseXML = function (b) {
        var c, d;
        if (!b || "string" != typeof b) return null;
        try {
            a.DOMParser ? (d = new DOMParser, c = d.parseFromString(b, "text/xml")) : (c = new ActiveXObject("Microsoft.XMLDOM"), c.async = "false", c.loadXML(b))
        } catch (e) {
            c = void 0
        }
        return c && c.documentElement && !c.getElementsByTagName("parsererror").length || ea.error("Invalid XML: " + b), c
    };
    var Gb, Hb, Ib = /#.*$/,
        Jb = /([?&])_=[^&]*/,
        Kb = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
        Lb = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
        Mb = /^(?:GET|HEAD)$/,
        Nb = /^\/\//,
        Ob = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
        Pb = {},
        Qb = {},
        Rb = "*/".concat("*");
    try {
        Hb = location.href
    } catch (ec) {
        Hb = oa.createElement("a"), Hb.href = "", Hb = Hb.href
    }
    Gb = Ob.exec(Hb.toLowerCase()) || [], ea.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: Hb,
            type: "GET",
            isLocal: Lb.test(Gb[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Rb,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": ea.parseJSON,
                "text xml": ea.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function (a, b) {
            return b ? P(P(a, ea.ajaxSettings), b) : P(ea.ajaxSettings, a)
        },
        ajaxPrefilter: N(Pb),
        ajaxTransport: N(Qb),
        ajax: function (a, b) {
            function c(a, b, c, d) {
                var e, k, r, s, u, w = b;
                2 !== t && (t = 2, h && clearTimeout(h), j = void 0, g = d || "", v.readyState = a > 0 ? 4 : 0, e = a >= 200 && a < 300 || 304 === a, c && (s = Q(l, v, c)), s = R(l, s, v, e), e ? (l.ifModified && (u = v.getResponseHeader("Last-Modified"), u && (ea.lastModified[f] = u), (u = v.getResponseHeader("etag")) && (ea.etag[f] = u)), 204 === a || "HEAD" === l.type ? w = "nocontent" : 304 === a ? w = "notmodified" : (w = s.state, k = s.data, r = s.error, e = !r)) : (r = w, !a && w || (w = "error", a < 0 && (a = 0))), v.status = a, v.statusText = (b || w) + "", e ? o.resolveWith(m, [k, w, v]) : o.rejectWith(m, [v, w, r]), v.statusCode(q), q = void 0, i && n.trigger(e ? "ajaxSuccess" : "ajaxError", [v, l, e ? k : r]), p.fireWith(m, [v, w]), i && (n.trigger("ajaxComplete", [v, l]), --ea.active || ea.event.trigger("ajaxStop")))
            }
            "object" == typeof a && (b = a, a = void 0), b = b || {};
            var d, e, f, g, h, i, j, k, l = ea.ajaxSetup({}, b),
                m = l.context || l,
                n = l.context && (m.nodeType || m.jquery) ? ea(m) : ea.event,
                o = ea.Deferred(),
                p = ea.Callbacks("once memory"),
                q = l.statusCode || {},
                r = {},
                s = {},
                t = 0,
                u = "canceled",
                v = {
                    readyState: 0,
                    getResponseHeader: function (a) {
                        var b;
                        if (2 === t) {
                            if (!k)
                                for (k = {}; b = Kb.exec(g);) k[b[1].toLowerCase()] = b[2];
                            b = k[a.toLowerCase()]
                        }
                        return null == b ? null : b
                    },
                    getAllResponseHeaders: function () {
                        return 2 === t ? g : null
                    },
                    setRequestHeader: function (a, b) {
                        var c = a.toLowerCase();
                        return t || (a = s[c] = s[c] || a, r[a] = b), this
                    },
                    overrideMimeType: function (a) {
                        return t || (l.mimeType = a), this
                    },
                    statusCode: function (a) {
                        var b;
                        if (a)
                            if (t < 2)
                                for (b in a) q[b] = [q[b], a[b]];
                            else v.always(a[v.status]);
                        return this
                    },
                    abort: function (a) {
                        var b = a || u;
                        return j && j.abort(b), c(0, b), this
                    }
                };
            if (o.promise(v).complete = p.add, v.success = v.done, v.error = v.fail, l.url = ((a || l.url || Hb) + "").replace(Ib, "").replace(Nb, Gb[1] + "//"), l.type = b.method || b.type || l.method || l.type, l.dataTypes = ea.trim(l.dataType || "*").toLowerCase().match(sa) || [""], null == l.crossDomain && (d = Ob.exec(l.url.toLowerCase()), l.crossDomain = !(!d || d[1] === Gb[1] && d[2] === Gb[2] && (d[3] || ("http:" === d[1] ? "80" : "443")) === (Gb[3] || ("http:" === Gb[1] ? "80" : "443")))), l.data && l.processData && "string" != typeof l.data && (l.data = ea.param(l.data, l.traditional)), O(Pb, l, b, v), 2 === t) return v;
            i = ea.event && l.global, i && 0 == ea.active++ && ea.event.trigger("ajaxStart"), l.type = l.type.toUpperCase(), l.hasContent = !Mb.test(l.type), f = l.url, l.hasContent || (l.data && (f = l.url += (Eb.test(f) ? "&" : "?") + l.data, delete l.data), !1 === l.cache && (l.url = Jb.test(f) ? f.replace(Jb, "$1_=" + Db++) : f + (Eb.test(f) ? "&" : "?") + "_=" + Db++)), l.ifModified && (ea.lastModified[f] && v.setRequestHeader("If-Modified-Since", ea.lastModified[f]), ea.etag[f] && v.setRequestHeader("If-None-Match", ea.etag[f])), (l.data && l.hasContent && !1 !== l.contentType || b.contentType) && v.setRequestHeader("Content-Type", l.contentType), v.setRequestHeader("Accept", l.dataTypes[0] && l.accepts[l.dataTypes[0]] ? l.accepts[l.dataTypes[0]] + ("*" !== l.dataTypes[0] ? ", " + Rb + "; q=0.01" : "") : l.accepts["*"]);
            for (e in l.headers) v.setRequestHeader(e, l.headers[e]);
            if (l.beforeSend && (!1 === l.beforeSend.call(m, v, l) || 2 === t)) return v.abort();
            u = "abort";
            for (e in {
                success: 1,
                error: 1,
                complete: 1
            }) v[e](l[e]);
            if (j = O(Qb, l, b, v)) {
                v.readyState = 1, i && n.trigger("ajaxSend", [v, l]), l.async && l.timeout > 0 && (h = setTimeout(function () {
                    v.abort("timeout")
                }, l.timeout));
                try {
                    t = 1, j.send(r, c)
                } catch (ec) {
                    if (!(t < 2)) throw ec;
                    c(-1, ec)
                }
            } else c(-1, "No Transport");
            return v
        },
        getJSON: function (a, b, c) {
            return ea.get(a, b, c, "json")
        },
        getScript: function (a, b) {
            return ea.get(a, void 0, b, "script")
        }
    }), ea.each(["get", "post"], function (a, b) {
        ea[b] = function (a, c, d, e) {
            return ea.isFunction(c) && (e = e || d, d = c, c = void 0), ea.ajax({
                url: a,
                type: b,
                dataType: e,
                data: c,
                success: d
            })
        }
    }), ea._evalUrl = function (a) {
        return ea.ajax({
            url: a,
            type: "GET",
            dataType: "script",
            async: !1,
            global: !1,
            throws: !0
        })
    }, ea.fn.extend({
        wrapAll: function (a) {
            if (ea.isFunction(a)) return this.each(function (b) {
                ea(this).wrapAll(a.call(this, b))
            });
            if (this[0]) {
                var b = ea(a, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && b.insertBefore(this[0]), b.map(function () {
                    for (var a = this; a.firstChild && 1 === a.firstChild.nodeType;) a = a.firstChild;
                    return a
                }).append(this)
            }
            return this
        },
        wrapInner: function (a) {
            return ea.isFunction(a) ? this.each(function (b) {
                ea(this).wrapInner(a.call(this, b))
            }) : this.each(function () {
                var b = ea(this),
                    c = b.contents();
                c.length ? c.wrapAll(a) : b.append(a)
            })
        },
        wrap: function (a) {
            var b = ea.isFunction(a);
            return this.each(function (c) {
                ea(this).wrapAll(b ? a.call(this, c) : a)
            })
        },
        unwrap: function () {
            return this.parent().each(function () {
                ea.nodeName(this, "body") || ea(this).replaceWith(this.childNodes)
            }).end()
        }
    }), ea.expr.filters.hidden = function (a) {
        return a.offsetWidth <= 0 && a.offsetHeight <= 0 || !ca.reliableHiddenOffsets() && "none" === (a.style && a.style.display || ea.css(a, "display"))
    }, ea.expr.filters.visible = function (a) {
        return !ea.expr.filters.hidden(a)
    };
    var Sb = /%20/g,
        Tb = /\[\]$/,
        Ub = /\r?\n/g,
        Vb = /^(?:submit|button|image|reset|file)$/i,
        Wb = /^(?:input|select|textarea|keygen)/i;
    ea.param = function (a, b) {
        var c, d = [],
            e = function (a, b) {
                b = ea.isFunction(b) ? b() : null == b ? "" : b, d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(b)
            };
        if (void 0 === b && (b = ea.ajaxSettings && ea.ajaxSettings.traditional), ea.isArray(a) || a.jquery && !ea.isPlainObject(a)) ea.each(a, function () {
            e(this.name, this.value)
        });
        else
            for (c in a) S(c, a[c], b, e);
        return d.join("&").replace(Sb, "+")
    }, ea.fn.extend({
        serialize: function () {
            return ea.param(this.serializeArray())
        },
        serializeArray: function () {
            return this.map(function () {
                var a = ea.prop(this, "elements");
                return a ? ea.makeArray(a) : this
            }).filter(function () {
                var a = this.type;
                return this.name && !ea(this).is(":disabled") && Wb.test(this.nodeName) && !Vb.test(a) && (this.checked || !Da.test(a))
            }).map(function (a, b) {
                var c = ea(this).val();
                return null == c ? null : ea.isArray(c) ? ea.map(c, function (a) {
                    return {
                        name: b.name,
                        value: a.replace(Ub, "\r\n")
                    }
                }) : {
                    name: b.name,
                    value: c.replace(Ub, "\r\n")
                }
            }).get()
        }
    }), ea.ajaxSettings.xhr = void 0 !== a.ActiveXObject ? function () {
        return !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && T() || U()
    } : T;
    var Xb = 0,
        Yb = {},
        Zb = ea.ajaxSettings.xhr();
    a.attachEvent && a.attachEvent("onunload", function () {
        for (var a in Yb) Yb[a](void 0, !0)
    }), ca.cors = !!Zb && "withCredentials" in Zb, Zb = ca.ajax = !!Zb, Zb && ea.ajaxTransport(function (a) {
        if (!a.crossDomain || ca.cors) {
            var b;
            return {
                send: function (c, d) {
                    var e, f = a.xhr(),
                        g = ++Xb;
                    if (f.open(a.type, a.url, a.async, a.username, a.password), a.xhrFields)
                        for (e in a.xhrFields) f[e] = a.xhrFields[e];
                    a.mimeType && f.overrideMimeType && f.overrideMimeType(a.mimeType), a.crossDomain || c["X-Requested-With"] || (c["X-Requested-With"] = "XMLHttpRequest");
                    for (e in c) void 0 !== c[e] && f.setRequestHeader(e, c[e] + "");
                    f.send(a.hasContent && a.data || null), b = function (c, e) {
                        var h, i, j;
                        if (b && (e || 4 === f.readyState))
                            if (delete Yb[g], b = void 0, f.onreadystatechange = ea.noop, e) 4 !== f.readyState && f.abort();
                            else {
                                j = {}, h = f.status, "string" == typeof f.responseText && (j.text = f.responseText);
                                try {
                                    i = f.statusText
                                } catch (ec) {
                                    i = ""
                                }
                                h || !a.isLocal || a.crossDomain ? 1223 === h && (h = 204) : h = j.text ? 200 : 404
                            }
                        j && d(h, i, j, f.getAllResponseHeaders())
                    }, a.async ? 4 === f.readyState ? setTimeout(b) : f.onreadystatechange = Yb[g] = b : b()
                },
                abort: function () {
                    b && b(void 0, !0)
                }
            }
        }
    }), ea.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function (a) {
                return ea.globalEval(a), a
            }
        }
    }), ea.ajaxPrefilter("script", function (a) {
        void 0 === a.cache && (a.cache = !1), a.crossDomain && (a.type = "GET", a.global = !1)
    }), ea.ajaxTransport("script", function (a) {
        if (a.crossDomain) {
            var b, c = oa.head || ea("head")[0] || oa.documentElement;
            return {
                send: function (d, e) {
                    b = oa.createElement("script"), b.async = !0, a.scriptCharset && (b.charset = a.scriptCharset), b.src = a.url, b.onload = b.onreadystatechange = function (a, c) {
                        (c || !b.readyState || /loaded|complete/.test(b.readyState)) && (b.onload = b.onreadystatechange = null, b.parentNode && b.parentNode.removeChild(b), b = null, c || e(200, "success"))
                    }, c.insertBefore(b, c.firstChild)
                },
                abort: function () {
                    b && b.onload(void 0, !0)
                }
            }
        }
    });
    var $b = [],
        _b = /(=)\?(?=&|$)|\?\?/;
    ea.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function () {
            var a = $b.pop() || ea.expando + "_" + Db++;
            return this[a] = !0, a
        }
    }), ea.ajaxPrefilter("json jsonp", function (b, c, d) {
        var e, f, g, h = !1 !== b.jsonp && (_b.test(b.url) ? "url" : "string" == typeof b.data && !(b.contentType || "").indexOf("application/x-www-form-urlencoded") && _b.test(b.data) && "data");
        if (h || "jsonp" === b.dataTypes[0]) return e = b.jsonpCallback = ea.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback, h ? b[h] = b[h].replace(_b, "$1" + e) : !1 !== b.jsonp && (b.url += (Eb.test(b.url) ? "&" : "?") + b.jsonp + "=" + e), b.converters["script json"] = function () {
            return g || ea.error(e + " was not called"), g[0]
        }, b.dataTypes[0] = "json", f = a[e], a[e] = function () {
            g = arguments
        }, d.always(function () {
            a[e] = f, b[e] && (b.jsonpCallback = c.jsonpCallback, $b.push(e)), g && ea.isFunction(f) && f(g[0]), g = f = void 0
        }), "script"
    }), ea.parseHTML = function (a, b, c) {
        if (!a || "string" != typeof a) return null;
        "boolean" == typeof b && (c = b, b = !1), b = b || oa;
        var d = la.exec(a),
            e = !c && [];
        return d ? [b.createElement(d[1])] : (d = ea.buildFragment([a], b, e), e && e.length && ea(e).remove(), ea.merge([], d.childNodes))
    };
    var ac = ea.fn.load;
    ea.fn.load = function (a, b, c) {
        if ("string" != typeof a && ac) return ac.apply(this, arguments);
        var d, e, f, g = this,
            h = a.indexOf(" ");
        return h >= 0 && (d = ea.trim(a.slice(h, a.length)), a = a.slice(0, h)), ea.isFunction(b) ? (c = b, b = void 0) : b && "object" == typeof b && (f = "POST"), g.length > 0 && ea.ajax({
            url: a,
            type: f,
            dataType: "html",
            data: b
        }).done(function (a) {
            e = arguments, g.html(d ? ea("<div>").append(ea.parseHTML(a)).find(d) : a)
        }).complete(c && function (a, b) {
            g.each(c, e || [a.responseText, b, a])
        }), this
    }, ea.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (a, b) {
        ea.fn[b] = function (a) {
            return this.on(b, a)
        }
    }), ea.expr.filters.animated = function (a) {
        return ea.grep(ea.timers, function (b) {
            return a === b.elem
        }).length
    };
    var bc = a.document.documentElement;
    ea.offset = {
        setOffset: function (a, b, c) {
            var d, e, f, g, h, i, j, k = ea.css(a, "position"),
                l = ea(a),
                m = {};
            "static" === k && (a.style.position = "relative"), h = l.offset(), f = ea.css(a, "top"), i = ea.css(a, "left"), j = ("absolute" === k || "fixed" === k) && ea.inArray("auto", [f, i]) > -1, j ? (d = l.position(), g = d.top, e = d.left) : (g = parseFloat(f) || 0, e = parseFloat(i) || 0), ea.isFunction(b) && (b = b.call(a, c, h)), null != b.top && (m.top = b.top - h.top + g), null != b.left && (m.left = b.left - h.left + e), "using" in b ? b.using.call(a, m) : l.css(m)
        }
    }, ea.fn.extend({
        offset: function (a) {
            if (arguments.length) return void 0 === a ? this : this.each(function (b) {
                ea.offset.setOffset(this, a, b)
            });
            var b, c, d = {
                    top: 0,
                    left: 0
                },
                e = this[0],
                f = e && e.ownerDocument;
            if (f) return b = f.documentElement, ea.contains(b, e) ? (typeof e.getBoundingClientRect !== wa && (d = e.getBoundingClientRect()), c = V(f), {
                top: d.top + (c.pageYOffset || b.scrollTop) - (b.clientTop || 0),
                left: d.left + (c.pageXOffset || b.scrollLeft) - (b.clientLeft || 0)
            }) : d
        },
        position: function () {
            if (this[0]) {
                var a, b, c = {
                        top: 0,
                        left: 0
                    },
                    d = this[0];
                return "fixed" === ea.css(d, "position") ? b = d.getBoundingClientRect() : (a = this.offsetParent(), b = this.offset(), ea.nodeName(a[0], "html") || (c = a.offset()), c.top += ea.css(a[0], "borderTopWidth", !0), c.left += ea.css(a[0], "borderLeftWidth", !0)), {
                    top: b.top - c.top - ea.css(d, "marginTop", !0),
                    left: b.left - c.left - ea.css(d, "marginLeft", !0)
                }
            }
        },
        offsetParent: function () {
            return this.map(function () {
                for (var a = this.offsetParent || bc; a && !ea.nodeName(a, "html") && "static" === ea.css(a, "position");) a = a.offsetParent;
                return a || bc
            })
        }
    }), ea.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function (a, b) {
        var c = /Y/.test(b);
        ea.fn[a] = function (d) {
            return Ca(this, function (a, d, e) {
                var f = V(a);
                if (void 0 === e) return f ? b in f ? f[b] : f.document.documentElement[d] : a[d];
                f ? f.scrollTo(c ? ea(f).scrollLeft() : e, c ? e : ea(f).scrollTop()) : a[d] = e
            }, a, d, arguments.length, null)
        }
    }), ea.each(["top", "left"], function (a, b) {
        ea.cssHooks[b] = A(ca.pixelPosition, function (a, c) {
            if (c) return c = ab(a, b), cb.test(c) ? ea(a).position()[b] + "px" : c
        })
    }), ea.each({
        Height: "height",
        Width: "width"
    }, function (a, b) {
        ea.each({
            padding: "inner" + a,
            content: b,
            "": "outer" + a
        }, function (c, d) {
            ea.fn[d] = function (d, e) {
                var f = arguments.length && (c || "boolean" != typeof d),
                    g = c || (!0 === d || !0 === e ? "margin" : "border");
                return Ca(this, function (b, c, d) {
                    var e;
                    return ea.isWindow(b) ? b.document.documentElement["client" + a] : 9 === b.nodeType ? (e = b.documentElement, Math.max(b.body["scroll" + a], e["scroll" + a], b.body["offset" + a], e["offset" + a], e["client" + a])) : void 0 === d ? ea.css(b, c, g) : ea.style(b, c, d, g)
                }, b, f ? d : void 0, f, null)
            }
        })
    }), ea.fn.size = function () {
        return this.length
    }, ea.fn.andSelf = ea.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function () {
        return ea
    });
    var cc = a.jQuery,
        dc = a.$;
    return ea.noConflict = function (b) {
        return a.$ === ea && (a.$ = dc), b && a.jQuery === ea && (a.jQuery = cc), ea
    }, typeof b === wa && (a.jQuery = a.$ = ea), ea
}),
    function (a, b, c) {
        function d(a, b) {
            return typeof a === b
        }

        function e() {
            var a, b, c, e, f, g, h;
            for (var i in u) {
                if (a = [], b = u[i], b.name && (a.push(b.name.toLowerCase()), b.options && b.options.aliases && b.options.aliases.length))
                    for (c = 0; c < b.options.aliases.length; c++) a.push(b.options.aliases[c].toLowerCase());
                for (e = d(b.fn, "function") ? b.fn() : b.fn, f = 0; f < a.length; f++) g = a[f], h = g.split("."), 1 === h.length ? w[h[0]] = e : (!w[h[0]] || w[h[0]] instanceof Boolean || (w[h[0]] = new Boolean(w[h[0]])), w[h[0]][h[1]] = e), t.push((e ? "" : "no-") + h.join("-"))
            }
        }

        function f(a) {
            var b = y.className,
                c = w._config.classPrefix || "";
            if (z && (b = b.baseVal), w._config.enableJSClass) {
                var d = new RegExp("(^|\\s)" + c + "no-js(\\s|$)");
                b = b.replace(d, "$1" + c + "js$2")
            }
            w._config.enableClasses && (b += " " + c + a.join(" " + c), z ? y.className.baseVal = b : y.className = b)
        }

        function g(a, b) {
            if ("object" == typeof a)
                for (var c in a) C(a, c) && g(c, a[c]);
            else {
                a = a.toLowerCase();
                var d = a.split("."),
                    e = w[d[0]];
                if (2 == d.length && (e = e[d[1]]), void 0 !== e) return w;
                b = "function" == typeof b ? b() : b, 1 == d.length ? w[d[0]] = b : (!w[d[0]] || w[d[0]] instanceof Boolean || (w[d[0]] = new Boolean(w[d[0]])), w[d[0]][d[1]] = b), f([(b && 0 != b ? "" : "no-") + d.join("-")]), w._trigger(a, b)
            }
            return w
        }

        function h(a) {
            return a.replace(/([a-z])-([a-z])/g, function (a, b, c) {
                return b + c.toUpperCase()
            }).replace(/^-/, "")
        }

        function i(a) {
            return a.replace(/([A-Z])/g, function (a, b) {
                return "-" + b.toLowerCase()
            }).replace(/^ms-/, "-ms-")
        }

        function j() {
            return "function" != typeof b.createElement ? b.createElement(arguments[0]) : z ? b.createElementNS.call(b, "http://www.w3.org/2000/svg", arguments[0]) : b.createElement.apply(b, arguments)
        }

        function k() {
            var a = b.body;
            return a || (a = j(z ? "svg" : "body"), a.fake = !0), a
        }

        function l(a, c, d, e) {
            var f, g, h, i, l = "modernizr",
                m = j("div"),
                n = k();
            if (parseInt(d, 10))
                for (; d--;) h = j("div"), h.id = e ? e[d] : l + (d + 1), m.appendChild(h);
            return f = j("style"), f.type = "text/css", f.id = "s" + l, (n.fake ? n : m).appendChild(f), n.appendChild(m), f.styleSheet ? f.styleSheet.cssText = a : f.appendChild(b.createTextNode(a)), m.id = l, n.fake && (n.style.background = "", n.style.overflow = "hidden", i = y.style.overflow, y.style.overflow = "hidden", y.appendChild(n)), g = c(m, a), n.fake ? (n.parentNode.removeChild(n), y.style.overflow = i, y.offsetHeight) : m.parentNode.removeChild(m), !!g
        }

        function m(a, b) {
            return !!~("" + a).indexOf(b)
        }

        function n(b, d) {
            var e = b.length;
            if ("CSS" in a && "supports" in a.CSS) {
                for (; e--;)
                    if (a.CSS.supports(i(b[e]), d)) return !0;
                return !1
            }
            if ("CSSSupportsRule" in a) {
                for (var f = []; e--;) f.push("(" + i(b[e]) + ":" + d + ")");
                return f = f.join(" or "), l("@supports (" + f + ") { #modernizr { position: absolute; } }", function (a) {
                    return "absolute" == getComputedStyle(a, null).position
                })
            }
            return c
        }

        function o(a, b) {
            return function () {
                return a.apply(b, arguments)
            }
        }

        function p(a, b, c) {
            var e;
            for (var f in a)
                if (a[f] in b) return !1 === c ? a[f] : (e = b[a[f]], d(e, "function") ? o(e, c || b) : e);
            return !1
        }

        function q(a, b, e, f) {
            function g() {
                k && (delete K.style, delete K.modElem)
            }
            if (f = !d(f, "undefined") && f, !d(e, "undefined")) {
                var i = n(a, e);
                if (!d(i, "undefined")) return i
            }
            for (var k, l, o, p, q, r = ["modernizr", "tspan"]; !K.style;) k = !0, K.modElem = j(r.shift()), K.style = K.modElem.style;
            for (o = a.length, l = 0; l < o; l++)
                if (p = a[l], q = K.style[p], m(p, "-") && (p = h(p)), K.style[p] !== c) {
                    if (f || d(e, "undefined")) return g(), "pfx" != b || p;
                    try {
                        K.style[p] = e
                    } catch (s) {}
                    if (K.style[p] != q) return g(), "pfx" != b || p
                }
            return g(), !1
        }

        function r(a, b, c, e, f) {
            var g = a.charAt(0).toUpperCase() + a.slice(1),
                h = (a + " " + G.join(g + " ") + g).split(" ");
            return d(b, "string") || d(b, "undefined") ? q(h, b, e, f) : (h = (a + " " + B.join(g + " ") + g).split(" "), p(h, b, c))
        }

        function s(a, b, d) {
            return r(a, c, c, b, d)
        }
        var t = [],
            u = [],
            v = {
                _version: "3.1.0",
                _config: {
                    classPrefix: "",
                    enableClasses: !0,
                    enableJSClass: !0,
                    usePrefixes: !0
                },
                _q: [],
                on: function (a, b) {
                    var c = this;
                    setTimeout(function () {
                        b(c[a])
                    }, 0)
                },
                addTest: function (a, b, c) {
                    u.push({
                        name: a,
                        fn: b,
                        options: c
                    })
                },
                addAsyncTest: function (a) {
                    u.push({
                        name: null,
                        fn: a
                    })
                }
            },
            w = function () {};
        w.prototype = v, w = new w;
        var x = v._config.usePrefixes ? " -webkit- -moz- -o- -ms- ".split(" ") : [];
        v._prefixes = x;
        var y = b.documentElement,
            z = "svg" === y.nodeName.toLowerCase();
        z || function (a, b) {
            function c(a, b) {
                var c = a.createElement("p"),
                    d = a.getElementsByTagName("head")[0] || a.documentElement;
                return c.innerHTML = "x<style>" + b + "</style>", d.insertBefore(c.lastChild, d.firstChild)
            }

            function d() {
                var a = t.elements;
                return "string" == typeof a ? a.split(" ") : a
            }

            function e(a, b) {
                var c = t.elements;
                "string" != typeof c && (c = c.join(" ")), "string" != typeof a && (a = a.join(" ")), t.elements = c + " " + a, j(b)
            }

            function f(a) {
                var b = s[a[q]];
                return b || (b = {}, r++, a[q] = r, s[r] = b), b
            }

            function g(a, c, d) {
                if (c || (c = b), l) return c.createElement(a);
                d || (d = f(c));
                var e;
                return e = d.cache[a] ? d.cache[a].cloneNode() : p.test(a) ? (d.cache[a] = d.createElem(a)).cloneNode() : d.createElem(a), !e.canHaveChildren || o.test(a) || e.tagUrn ? e : d.frag.appendChild(e)
            }

            function h(a, c) {
                if (a || (a = b), l) return a.createDocumentFragment();
                c = c || f(a);
                for (var e = c.frag.cloneNode(), g = 0, h = d(), i = h.length; g < i; g++) e.createElement(h[g]);
                return e
            }

            function i(a, b) {
                b.cache || (b.cache = {}, b.createElem = a.createElement, b.createFrag = a.createDocumentFragment, b.frag = b.createFrag()), a.createElement = function (c) {
                    return t.shivMethods ? g(c, a, b) : b.createElem(c)
                }, a.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + d().join().replace(/[\w\-:]+/g, function (a) {
                    return b.createElem(a), b.frag.createElement(a), 'c("' + a + '")'
                }) + ");return n}")(t, b.frag)
            }

            function j(a) {
                a || (a = b);
                var d = f(a);
                return !t.shivCSS || k || d.hasCSS || (d.hasCSS = !!c(a, "article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")), l || i(a, d), a
            }
            var k, l, m = "3.7.3",
                n = a.html5 || {},
                o = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,
                p = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,
                q = "_html5shiv",
                r = 0,
                s = {};
            ! function () {
                try {
                    var a = b.createElement("a");
                    a.innerHTML = "<xyz></xyz>", k = "hidden" in a, l = 1 == a.childNodes.length || function () {
                        b.createElement("a");
                        var a = b.createDocumentFragment();
                        return void 0 === a.cloneNode || void 0 === a.createDocumentFragment || void 0 === a.createElement
                    }()
                } catch (c) {
                    k = !0, l = !0
                }
            }();
            var t = {
                elements: n.elements || "abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output picture progress section summary template time video",
                version: m,
                shivCSS: !1 !== n.shivCSS,
                supportsUnknownElements: l,
                shivMethods: !1 !== n.shivMethods,
                type: "default",
                shivDocument: j,
                createElement: g,
                createDocumentFragment: h,
                addElements: e
            };
            a.html5 = t, j(b), "object" == typeof module && module.exports && (module.exports = t)
        }(void 0 !== a ? a : this, b);
        var A = "Moz O ms Webkit",
            B = v._config.usePrefixes ? A.toLowerCase().split(" ") : [];
        v._domPrefixes = B;
        var C;
        ! function () {
            var a = {}.hasOwnProperty;
            C = d(a, "undefined") || d(a.call, "undefined") ? function (a, b) {
                return b in a && d(a.constructor.prototype[b], "undefined")
            } : function (b, c) {
                return a.call(b, c)
            }
        }(), v._l = {}, v.on = function (a, b) {
            this._l[a] || (this._l[a] = []), this._l[a].push(b), w.hasOwnProperty(a) && setTimeout(function () {
                w._trigger(a, w[a])
            }, 0)
        }, v._trigger = function (a, b) {
            if (this._l[a]) {
                var c = this._l[a];
                setTimeout(function () {
                    var a;
                    for (a = 0; a < c.length; a++)(0, c[a])(b)
                }, 0), delete this._l[a]
            }
        }, w._q.push(function () {
            v.addTest = g
        });
        var D = function (a, b) {
            var c = !1,
                d = j("div"),
                e = d.style;
            if (a in e) {
                var f = B.length;
                for (e[a] = b, c = e[a]; f-- && !c;) e[a] = "-" + B[f] + "-" + b, c = e[a]
            }
            return "" === c && (c = !1), c
        };
        v.prefixedCSSValue = D, w.addTest("cssgradients", function () {
            var a = "background-image:",
                b = "linear-gradient(left top,#9f9, white);",
                c = a + x.join(b + a).slice(0, -a.length);
            w._config.usePrefixes && (c += a + "-webkit-gradient(linear,left top,right bottom,from(#9f9),to(white));");
            var d = j("a"),
                e = d.style;
            return e.cssText = c, ("" + e.backgroundImage).indexOf("gradient") > -1
        }), w.addTest("rgba", function () {
            var a = j("a").style;
            return a.cssText = "background-color:rgba(150,255,150,.5)", ("" + a.backgroundColor).indexOf("rgba") > -1
        }), w.addTest("opacity", function () {
            var a = j("a").style;
            return a.cssText = x.join("opacity:.55;"), /^0.55$/.test(a.opacity)
        });
        var E = "CSS" in a && "supports" in a.CSS,
            F = "supportsCSS" in a;
        w.addTest("supports", E || F);
        var G = v._config.usePrefixes ? A.split(" ") : [];
        v._cssomPrefixes = G;
        var H = function (b) {
            var d, e = x.length,
                f = a.CSSRule;
            if (void 0 === f) return c;
            if (!b) return !1;
            if (b = b.replace(/^@/, ""), (d = b.replace(/-/g, "_").toUpperCase() + "_RULE") in f) return "@" + b;
            for (var g = 0; g < e; g++) {
                var h = x[g];
                if (h.toUpperCase() + "_" + d in f) return "@-" + h.toLowerCase() + "-" + b
            }
            return !1
        };
        v.atRule = H;
        var I = v.testStyles = l;
        ! function () {
            var a = navigator.userAgent,
                b = a.match(/applewebkit\/([0-9]+)/gi) && parseFloat(RegExp.$1),
                c = a.match(/w(eb)?osbrowser/gi),
                d = a.match(/windows phone/gi) && a.match(/iemobile\/([0-9])+/gi) && parseFloat(RegExp.$1) >= 9,
                e = b < 533 && a.match(/android/gi);
            return c || e || d
        }() ? I('@font-face {font-family:"font";src:url("https://")}', function (a, c) {
            var d = b.getElementById("smodernizr"),
                e = d.sheet || d.styleSheet,
                f = e ? e.cssRules && e.cssRules[0] ? e.cssRules[0].cssText : e.cssText || "" : "",
                g = /src/i.test(f) && 0 === f.indexOf(c.split(" ")[0]);
            w.addTest("fontface", g)
        }) : w.addTest("fontface", !1);
        var J = {
            elem: j("modernizr")
        };
        w._q.push(function () {
            delete J.elem
        });
        var K = {
            style: J.elem.style
        };
        w._q.unshift(function () {
            delete K.style
        });
        v.testProp = function (a, b, d) {
            return q([a], c, b, d)
        };
        v.testAllProps = r;
        var L = v.prefixed = function (a, b, c) {
            return 0 === a.indexOf("@") ? H(a) : (-1 != a.indexOf("-") && (a = h(a)), b ? r(a, b, c) : r(a, "pfx"))
        };
        v.prefixedCSS = function (a) {
            var b = L(a);
            return b && i(b)
        };
        v.testAllProps = s, w.addTest("backgroundsize", s("backgroundSize", "100%", !0)), w.addTest("flexbox", s("flexBasis", "1px", !0)), w.addTest("flexboxlegacy", s("boxDirection", "reverse", !0)), w.addTest("flexboxtweener", s("flexAlign", "end", !0)), w.addTest("flexwrap", s("flexWrap", "wrap", !0)), w.addTest("csstransforms", function () {
            return -1 === navigator.userAgent.indexOf("Android 2.") && s("transform", "scale(1)", !0)
        }), w.addTest("csstransforms3d", function () {
            var a = !!s("perspective", "1px", !0),
                b = w._config.usePrefixes;
            if (a && (!b || "webkitPerspective" in y.style)) {
                var c;
                w.supports ? c = "@supports (perspective: 1px)" : (c = "@media (transform-3d)", b && (c += ",(-webkit-transform-3d)")), c += "{#modernizr{left:9px;position:absolute;height:5px;margin:0;padding:0;border:0}}", I(c, function (b) {
                    a = 9 === b.offsetLeft && 5 === b.offsetHeight
                })
            }
            return a
        }), w.addTest("preserve3d", s("transformStyle", "preserve-3d")), w.addTest("csstransitions", s("transition", "all", !0)), e(), f(t), delete v.addTest, delete v.addAsyncTest;
        for (var M = 0; M < w._q.length; M++) w._q[M]();
        a.Modernizr = w
    }(window, document), window.matchMedia || (window.matchMedia = function (a) {
    "use strict";
    var b = a.document,
        c = b.documentElement,
        d = [],
        e = 0,
        f = "",
        g = {},
        h = /\s*(only|not)?\s*(screen|print|[a-z\-]+)\s*(and)?\s*/i,
        i = /^\s*\(\s*(-[a-z]+-)?(min-|max-)?([a-z\-]+)\s*(:?\s*([0-9]+(\.[0-9]+)?|portrait|landscape)(px|em|dppx|dpcm|rem|%|in|cm|mm|ex|pt|pc|\/([0-9]+(\.[0-9]+)?))?)?\s*\)\s*$/,
        j = 0,
        k = function (a) {
            var b = -1 !== a.indexOf(",") && a.split(",") || [a],
                c = b.length - 1,
                d = c,
                e = null,
                j = null,
                k = "",
                l = 0,
                m = !1,
                n = "",
                o = "",
                p = null,
                q = 0,
                r = null,
                s = "",
                t = "",
                u = "",
                v = "",
                w = "",
                x = !1;
            if ("" === a) return !0;
            do {
                if (e = b[d - c], m = !1, j = e.match(h), j && (k = j[0], l = j.index), !j || -1 === e.substring(0, l).indexOf("(") && (l || !j[3] && k !== j.input)) x = !1;
                else {
                    if (o = e, m = "not" === j[1], l || (n = j[2], o = e.substring(k.length)), x = n === f || "all" === n || "" === n, p = -1 !== o.indexOf(" and ") && o.split(" and ") || [o], q = p.length - 1, q, x && q >= 0 && "" !== o)
                        do {
                            if (!(r = p[q].match(i)) || !g[r[3]]) {
                                x = !1;
                                break
                            }
                            if (s = r[2], t = r[5], v = t, u = r[7], w = g[r[3]], u && (v = "px" === u ? Number(t) : "em" === u || "rem" === u ? 16 * t : r[8] ? (t / r[8]).toFixed(2) : "dppx" === u ? 96 * t : "dpcm" === u ? .3937 * t : Number(t)), !(x = "min-" === s && v ? w >= v : "max-" === s && v ? w <= v : v ? w === v : !!w)) break
                        } while (q--);
                    if (x) break
                }
            } while (c--);
            return m ? !x : x
        },
        l = function () {
            var b = a.innerWidth || c.clientWidth,
                d = a.innerHeight || c.clientHeight,
                e = a.screen.width,
                f = a.screen.height,
                h = a.screen.colorDepth,
                i = a.devicePixelRatio;
            g.width = b, g.height = d, g["aspect-ratio"] = (b / d).toFixed(2), g["device-width"] = e, g["device-height"] = f, g["device-aspect-ratio"] = (e / f).toFixed(2), g.color = h, g["color-index"] = Math.pow(2, h), g.orientation = d >= b ? "portrait" : "landscape", g.resolution = i && 96 * i || a.screen.deviceXDPI || 96, g["device-pixel-ratio"] = i || 1
        },
        m = function () {
            clearTimeout(j), j = setTimeout(function () {
                var b = null,
                    c = e - 1,
                    f = c,
                    g = !1;
                if (c >= 0) {
                    l();
                    do {
                        if ((b = d[f - c]) && ((g = k(b.mql.media)) && !b.mql.matches || !g && b.mql.matches) && (b.mql.matches = g, b.listeners))
                            for (var h = 0, i = b.listeners.length; h < i; h++) b.listeners[h] && b.listeners[h].call(a, b.mql)
                    } while (c--)
                }
            }, 10)
        };
    return function () {
        var c = b.getElementsByTagName("head")[0],
            d = b.createElement("style"),
            e = null,
            g = ["screen", "print", "speech", "projection", "handheld", "tv", "braille", "embossed", "tty"],
            h = 0,
            i = g.length,
            j = "#mediamatchjs { position: relative; z-index: 0; }",
            k = "",
            n = a.addEventListener || (k = "on") && a.attachEvent;
        for (d.type = "text/css", d.id = "mediamatchjs", c.appendChild(d), e = a.getComputedStyle && a.getComputedStyle(d) || d.currentStyle; h < i; h++) j += "@media " + g[h] + " { #mediamatchjs { position: relative; z-index: " + h + " } }";
        d.styleSheet ? d.styleSheet.cssText = j : d.textContent = j, f = g[1 * e.zIndex || 0], c.removeChild(d), l(), n(k + "resize", m), n(k + "orientationchange", m)
    }(),
        function (a) {
            var b = e,
                c = {
                    matches: !1,
                    media: a,
                    addListener: function (a) {
                        d[b].listeners || (d[b].listeners = []), a && d[b].listeners.push(a)
                    },
                    removeListener: function (a) {
                        var c = d[b],
                            e = 0,
                            f = 0;
                        if (c)
                            for (f = c.listeners.length; e < f; e++) c.listeners[e] === a && c.listeners.splice(e, 1)
                    }
                };
            return "" === a ? (c.matches = !0, c) : (c.matches = k(a), e = d.push({
                mql: c,
                listeners: null
            }), c)
        }
}(window)),
    function (a, b) {
        "object" == typeof exports && "undefined" != typeof module ? b(exports, require("jquery"), require("popper.js")) : "function" == typeof define && define.amd ? define(["exports", "jquery", "popper.js"], b) : b(a.bootstrap = {}, a.jQuery, a.Popper)
    }(this, function (a, b, c) {
        "use strict";

        function d(a, b) {
            for (var c = 0; c < b.length; c++) {
                var d = b[c];
                d.enumerable = d.enumerable || !1, d.configurable = !0, "value" in d && (d.writable = !0), Object.defineProperty(a, d.key, d)
            }
        }

        function e(a, b, c) {
            return b && d(a.prototype, b), c && d(a, c), a
        }

        function f() {
            return (f = Object.assign || function (a) {
                for (var b = 1; b < arguments.length; b++) {
                    var c = arguments[b];
                    for (var d in c) Object.prototype.hasOwnProperty.call(c, d) && (a[d] = c[d])
                }
                return a
            }).apply(this, arguments)
        }
        b = b && b.hasOwnProperty("default") ? b.default : b, c = c && c.hasOwnProperty("default") ? c.default : c;
        var g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, A, B, C, D, E, F, G = function (a) {
                function b(b) {
                    var c = this,
                        e = !1;
                    return a(this).one(d.TRANSITION_END, function () {
                        e = !0
                    }), setTimeout(function () {
                        e || d.triggerTransitionEnd(c)
                    }, b), this
                }
                var c = !1,
                    d = {
                        TRANSITION_END: "bsTransitionEnd",
                        getUID: function (a) {
                            do {
                                a += ~~(1e6 * Math.random())
                            } while (document.getElementById(a));
                            return a
                        },
                        getSelectorFromElement: function (b) {
                            var c, d = b.getAttribute("data-target");
                            d && "#" !== d || (d = b.getAttribute("href") || ""), "#" === d.charAt(0) && (c = d, d = c = "function" == typeof a.escapeSelector ? a.escapeSelector(c).substr(1) : c.replace(/(:|\.|\[|\]|,|=|@)/g, "\\$1"));
                            try {
                                return a(document).find(d).length > 0 ? d : null
                            } catch (a) {
                                return null
                            }
                        },
                        reflow: function (a) {
                            return a.offsetHeight
                        },
                        triggerTransitionEnd: function (b) {
                            a(b).trigger(c.end)
                        },
                        supportsTransitionEnd: function () {
                            return Boolean(c)
                        },
                        isElement: function (a) {
                            return (a[0] || a).nodeType
                        },
                        typeCheckConfig: function (a, b, c) {
                            for (var e in c)
                                if (Object.prototype.hasOwnProperty.call(c, e)) {
                                    var f = c[e],
                                        g = b[e],
                                        h = g && d.isElement(g) ? "element" : (i = g, {}.toString.call(i).match(/\s([a-zA-Z]+)/)[1].toLowerCase());
                                    if (!new RegExp(f).test(h)) throw new Error(a.toUpperCase() + ': Option "' + e + '" provided type "' + h + '" but expected type "' + f + '".')
                                }
                            var i
                        }
                    };
                return c = ("undefined" == typeof window || !window.QUnit) && {
                    end: "transitionend"
                }, a.fn.emulateTransitionEnd = b, d.supportsTransitionEnd() && (a.event.special[d.TRANSITION_END] = {
                    bindType: c.end,
                    delegateType: c.end,
                    handle: function (b) {
                        if (a(b.target).is(this)) return b.handleObj.handler.apply(this, arguments)
                    }
                }), d
            }(b),
            H = (h = "alert", j = "." + (i = "bs.alert"), k = (g = b).fn[h], l = {
                CLOSE: "close" + j,
                CLOSED: "closed" + j,
                CLICK_DATA_API: "click" + j + ".data-api"
            }, m = "alert", n = "fade", o = "show", p = function () {
                function a(a) {
                    this._element = a
                }
                var b = a.prototype;
                return b.close = function (a) {
                    a = a || this._element;
                    var b = this._getRootElement(a);
                    this._triggerCloseEvent(b).isDefaultPrevented() || this._removeElement(b)
                }, b.dispose = function () {
                    g.removeData(this._element, i), this._element = null
                }, b._getRootElement = function (a) {
                    var b = G.getSelectorFromElement(a),
                        c = !1;
                    return b && (c = g(b)[0]), c || (c = g(a).closest("." + m)[0]), c
                }, b._triggerCloseEvent = function (a) {
                    var b = g.Event(l.CLOSE);
                    return g(a).trigger(b), b
                }, b._removeElement = function (a) {
                    var b = this;
                    g(a).removeClass(o), G.supportsTransitionEnd() && g(a).hasClass(n) ? g(a).one(G.TRANSITION_END, function (c) {
                        return b._destroyElement(a, c)
                    }).emulateTransitionEnd(150) : this._destroyElement(a)
                }, b._destroyElement = function (a) {
                    g(a).detach().trigger(l.CLOSED).remove()
                }, a._jQueryInterface = function (b) {
                    return this.each(function () {
                        var c = g(this),
                            d = c.data(i);
                        d || (d = new a(this), c.data(i, d)), "close" === b && d[b](this)
                    })
                }, a._handleDismiss = function (a) {
                    return function (b) {
                        b && b.preventDefault(), a.close(this)
                    }
                }, e(a, null, [{
                    key: "VERSION",
                    get: function () {
                        return "4.0.0"
                    }
                }]), a
            }(), g(document).on(l.CLICK_DATA_API, '[data-dismiss="alert"]', p._handleDismiss(new p)), g.fn[h] = p._jQueryInterface, g.fn[h].Constructor = p, g.fn[h].noConflict = function () {
                return g.fn[h] = k, p._jQueryInterface
            }, p),
            I = (r = "button", t = "." + (s = "bs.button"), u = ".data-api", v = (q = b).fn[r], w = "active", x = "btn", y = "focus", z = '[data-toggle^="button"]', A = '[data-toggle="buttons"]', B = "input", C = ".active", D = ".btn", E = {
                CLICK_DATA_API: "click" + t + u,
                FOCUS_BLUR_DATA_API: "focus" + t + u + " blur" + t + u
            }, F = function () {
                function a(a) {
                    this._element = a
                }
                var b = a.prototype;
                return b.toggle = function () {
                    var a = !0,
                        b = !0,
                        c = q(this._element).closest(A)[0];
                    if (c) {
                        var d = q(this._element).find(B)[0];
                        if (d) {
                            if ("radio" === d.type)
                                if (d.checked && q(this._element).hasClass(w)) a = !1;
                                else {
                                    var e = q(c).find(C)[0];
                                    e && q(e).removeClass(w)
                                }
                            if (a) {
                                if (d.hasAttribute("disabled") || c.hasAttribute("disabled") || d.classList.contains("disabled") || c.classList.contains("disabled")) return;
                                d.checked = !q(this._element).hasClass(w), q(d).trigger("change")
                            }
                            d.focus(), b = !1
                        }
                    }
                    b && this._element.setAttribute("aria-pressed", !q(this._element).hasClass(w)), a && q(this._element).toggleClass(w)
                }, b.dispose = function () {
                    q.removeData(this._element, s), this._element = null
                }, a._jQueryInterface = function (b) {
                    return this.each(function () {
                        var c = q(this).data(s);
                        c || (c = new a(this), q(this).data(s, c)), "toggle" === b && c[b]()
                    })
                }, e(a, null, [{
                    key: "VERSION",
                    get: function () {
                        return "4.0.0"
                    }
                }]), a
            }(), q(document).on(E.CLICK_DATA_API, z, function (a) {
                a.preventDefault();
                var b = a.target;
                q(b).hasClass(x) || (b = q(b).closest(D)), F._jQueryInterface.call(q(b), "toggle")
            }).on(E.FOCUS_BLUR_DATA_API, z, function (a) {
                var b = q(a.target).closest(D)[0];
                q(b).toggleClass(y, /^focus(in)?$/.test(a.type))
            }), q.fn[r] = F._jQueryInterface, q.fn[r].Constructor = F, q.fn[r].noConflict = function () {
                return q.fn[r] = v, F._jQueryInterface
            }, F),
            J = function (a) {
                var b = "carousel",
                    c = "bs.carousel",
                    d = "." + c,
                    g = a.fn[b],
                    h = {
                        interval: 5e3,
                        keyboard: !0,
                        slide: !1,
                        pause: "hover",
                        wrap: !0
                    },
                    i = {
                        interval: "(number|boolean)",
                        keyboard: "boolean",
                        slide: "(boolean|string)",
                        pause: "(string|boolean)",
                        wrap: "boolean"
                    },
                    j = "next",
                    k = "prev",
                    l = "left",
                    m = "right",
                    n = {
                        SLIDE: "slide" + d,
                        SLID: "slid" + d,
                        KEYDOWN: "keydown" + d,
                        MOUSEENTER: "mouseenter" + d,
                        MOUSELEAVE: "mouseleave" + d,
                        TOUCHEND: "touchend" + d,
                        LOAD_DATA_API: "load" + d + ".data-api",
                        CLICK_DATA_API: "click" + d + ".data-api"
                    },
                    o = "carousel",
                    p = "active",
                    q = "slide",
                    r = "carousel-item-right",
                    s = "carousel-item-left",
                    t = "carousel-item-next",
                    u = "carousel-item-prev",
                    v = {
                        ACTIVE: ".active",
                        ACTIVE_ITEM: ".active.carousel-item",
                        ITEM: ".carousel-item",
                        NEXT_PREV: ".carousel-item-next, .carousel-item-prev",
                        INDICATORS: ".carousel-indicators",
                        DATA_SLIDE: "[data-slide], [data-slide-to]",
                        DATA_RIDE: '[data-ride="carousel"]'
                    },
                    w = function () {
                        function g(b, c) {
                            this._items = null, this._interval = null, this._activeElement = null, this._isPaused = !1, this._isSliding = !1, this.touchTimeout = null, this._config = this._getConfig(c), this._element = a(b)[0], this._indicatorsElement = a(this._element).find(v.INDICATORS)[0], this._addEventListeners()
                        }
                        var w = g.prototype;
                        return w.next = function () {
                            this._isSliding || this._slide(j)
                        }, w.nextWhenVisible = function () {
                            !document.hidden && a(this._element).is(":visible") && "hidden" !== a(this._element).css("visibility") && this.next()
                        }, w.prev = function () {
                            this._isSliding || this._slide(k)
                        }, w.pause = function (b) {
                            b || (this._isPaused = !0), a(this._element).find(v.NEXT_PREV)[0] && G.supportsTransitionEnd() && (G.triggerTransitionEnd(this._element), this.cycle(!0)), clearInterval(this._interval), this._interval = null
                        }, w.cycle = function (a) {
                            a || (this._isPaused = !1), this._interval && (clearInterval(this._interval), this._interval = null), this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval))
                        }, w.to = function (b) {
                            var c = this;
                            this._activeElement = a(this._element).find(v.ACTIVE_ITEM)[0];
                            var d = this._getItemIndex(this._activeElement);
                            if (!(b > this._items.length - 1 || b < 0))
                                if (this._isSliding) a(this._element).one(n.SLID, function () {
                                    return c.to(b)
                                });
                                else {
                                    if (d === b) return this.pause(), void this.cycle();
                                    var e = b > d ? j : k;
                                    this._slide(e, this._items[b])
                                }
                        }, w.dispose = function () {
                            a(this._element).off(d), a.removeData(this._element, c), this._items = null, this._config = null, this._element = null, this._interval = null, this._isPaused = null, this._isSliding = null, this._activeElement = null, this._indicatorsElement = null
                        }, w._getConfig = function (a) {
                            return a = f({}, h, a), G.typeCheckConfig(b, a, i), a
                        }, w._addEventListeners = function () {
                            var b = this;
                            this._config.keyboard && a(this._element).on(n.KEYDOWN, function (a) {
                                return b._keydown(a)
                            }), "hover" === this._config.pause && (a(this._element).on(n.MOUSEENTER, function (a) {
                                return b.pause(a)
                            }).on(n.MOUSELEAVE, function (a) {
                                return b.cycle(a)
                            }), "ontouchstart" in document.documentElement && a(this._element).on(n.TOUCHEND, function () {
                                b.pause(), b.touchTimeout && clearTimeout(b.touchTimeout), b.touchTimeout = setTimeout(function (a) {
                                    return b.cycle(a)
                                }, 500 + b._config.interval)
                            }))
                        }, w._keydown = function (a) {
                            if (!/input|textarea/i.test(a.target.tagName)) switch (a.which) {
                                case 37:
                                    a.preventDefault(), this.prev();
                                    break;
                                case 39:
                                    a.preventDefault(), this.next()
                            }
                        }, w._getItemIndex = function (b) {
                            return this._items = a.makeArray(a(b).parent().find(v.ITEM)), this._items.indexOf(b)
                        }, w._getItemByDirection = function (a, b) {
                            var c = a === j,
                                d = a === k,
                                e = this._getItemIndex(b),
                                f = this._items.length - 1;
                            if ((d && 0 === e || c && e === f) && !this._config.wrap) return b;
                            var g = (e + (a === k ? -1 : 1)) % this._items.length;
                            return -1 === g ? this._items[this._items.length - 1] : this._items[g]
                        }, w._triggerSlideEvent = function (b, c) {
                            var d = this._getItemIndex(b),
                                e = this._getItemIndex(a(this._element).find(v.ACTIVE_ITEM)[0]),
                                f = a.Event(n.SLIDE, {
                                    relatedTarget: b,
                                    direction: c,
                                    from: e,
                                    to: d
                                });
                            return a(this._element).trigger(f), f
                        }, w._setActiveIndicatorElement = function (b) {
                            if (this._indicatorsElement) {
                                a(this._indicatorsElement).find(v.ACTIVE).removeClass(p);
                                var c = this._indicatorsElement.children[this._getItemIndex(b)];
                                c && a(c).addClass(p)
                            }
                        }, w._slide = function (b, c) {
                            var d, e, f, g = this,
                                h = a(this._element).find(v.ACTIVE_ITEM)[0],
                                i = this._getItemIndex(h),
                                k = c || h && this._getItemByDirection(b, h),
                                o = this._getItemIndex(k),
                                w = Boolean(this._interval);
                            if (b === j ? (d = s, e = t, f = l) : (d = r, e = u, f = m), k && a(k).hasClass(p)) this._isSliding = !1;
                            else if (!this._triggerSlideEvent(k, f).isDefaultPrevented() && h && k) {
                                this._isSliding = !0, w && this.pause(), this._setActiveIndicatorElement(k);
                                var x = a.Event(n.SLID, {
                                    relatedTarget: k,
                                    direction: f,
                                    from: i,
                                    to: o
                                });
                                G.supportsTransitionEnd() && a(this._element).hasClass(q) ? (a(k).addClass(e), G.reflow(k), a(h).addClass(d), a(k).addClass(d), a(h).one(G.TRANSITION_END, function () {
                                    a(k).removeClass(d + " " + e).addClass(p), a(h).removeClass(p + " " + e + " " + d), g._isSliding = !1, setTimeout(function () {
                                        return a(g._element).trigger(x)
                                    }, 0)
                                }).emulateTransitionEnd(600)) : (a(h).removeClass(p), a(k).addClass(p), this._isSliding = !1, a(this._element).trigger(x)), w && this.cycle()
                            }
                        }, g._jQueryInterface = function (b) {
                            return this.each(function () {
                                var d = a(this).data(c),
                                    e = f({}, h, a(this).data());
                                "object" == typeof b && (e = f({}, e, b));
                                var i = "string" == typeof b ? b : e.slide;
                                if (d || (d = new g(this, e), a(this).data(c, d)), "number" == typeof b) d.to(b);
                                else if ("string" == typeof i) {
                                    if (void 0 === d[i]) throw new TypeError('No method named "' + i + '"');
                                    d[i]()
                                } else e.interval && (d.pause(), d.cycle())
                            })
                        }, g._dataApiClickHandler = function (b) {
                            var d = G.getSelectorFromElement(this);
                            if (d) {
                                var e = a(d)[0];
                                if (e && a(e).hasClass(o)) {
                                    var h = f({}, a(e).data(), a(this).data()),
                                        i = this.getAttribute("data-slide-to");
                                    i && (h.interval = !1), g._jQueryInterface.call(a(e), h), i && a(e).data(c).to(i), b.preventDefault()
                                }
                            }
                        }, e(g, null, [{
                            key: "VERSION",
                            get: function () {
                                return "4.0.0"
                            }
                        }, {
                            key: "Default",
                            get: function () {
                                return h
                            }
                        }]), g
                    }();
                return a(document).on(n.CLICK_DATA_API, v.DATA_SLIDE, w._dataApiClickHandler), a(window).on(n.LOAD_DATA_API, function () {
                    a(v.DATA_RIDE).each(function () {
                        var b = a(this);
                        w._jQueryInterface.call(b, b.data())
                    })
                }), a.fn[b] = w._jQueryInterface, a.fn[b].Constructor = w, a.fn[b].noConflict = function () {
                    return a.fn[b] = g, w._jQueryInterface
                }, w
            }(b),
            K = function (a) {
                var b = "collapse",
                    c = "bs.collapse",
                    d = "." + c,
                    g = a.fn[b],
                    h = {
                        toggle: !0,
                        parent: ""
                    },
                    i = {
                        toggle: "boolean",
                        parent: "(string|element)"
                    },
                    j = {
                        SHOW: "show" + d,
                        SHOWN: "shown" + d,
                        HIDE: "hide" + d,
                        HIDDEN: "hidden" + d,
                        CLICK_DATA_API: "click" + d + ".data-api"
                    },
                    k = "show",
                    l = "collapse",
                    m = "collapsing",
                    n = "collapsed",
                    o = "width",
                    p = "height",
                    q = {
                        ACTIVES: ".show, .collapsing",
                        DATA_TOGGLE: '[data-toggle="collapse"]'
                    },
                    r = function () {
                        function d(b, c) {
                            this._isTransitioning = !1, this._element = b, this._config = this._getConfig(c), this._triggerArray = a.makeArray(a('[data-toggle="collapse"][href="#' + b.id + '"],[data-toggle="collapse"][data-target="#' + b.id + '"]'));
                            for (var d = a(q.DATA_TOGGLE), e = 0; e < d.length; e++) {
                                var f = d[e],
                                    g = G.getSelectorFromElement(f);
                                null !== g && a(g).filter(b).length > 0 && (this._selector = g, this._triggerArray.push(f))
                            }
                            this._parent = this._config.parent ? this._getParent() : null, this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray), this._config.toggle && this.toggle()
                        }
                        var g = d.prototype;
                        return g.toggle = function () {
                            a(this._element).hasClass(k) ? this.hide() : this.show()
                        }, g.show = function () {
                            var b, e, f = this;
                            if (!(this._isTransitioning || a(this._element).hasClass(k) || (this._parent && 0 === (b = a.makeArray(a(this._parent).find(q.ACTIVES).filter('[data-parent="' + this._config.parent + '"]'))).length && (b = null), b && (e = a(b).not(this._selector).data(c)) && e._isTransitioning))) {
                                var g = a.Event(j.SHOW);
                                if (a(this._element).trigger(g), !g.isDefaultPrevented()) {
                                    b && (d._jQueryInterface.call(a(b).not(this._selector), "hide"), e || a(b).data(c, null));
                                    var h = this._getDimension();
                                    a(this._element).removeClass(l).addClass(m), this._element.style[h] = 0, this._triggerArray.length > 0 && a(this._triggerArray).removeClass(n).attr("aria-expanded", !0), this.setTransitioning(!0);
                                    var i = function () {
                                        a(f._element).removeClass(m).addClass(l).addClass(k), f._element.style[h] = "", f.setTransitioning(!1), a(f._element).trigger(j.SHOWN)
                                    };
                                    if (G.supportsTransitionEnd()) {
                                        var o = "scroll" + (h[0].toUpperCase() + h.slice(1));
                                        a(this._element).one(G.TRANSITION_END, i).emulateTransitionEnd(600), this._element.style[h] = this._element[o] + "px"
                                    } else i()
                                }
                            }
                        }, g.hide = function () {
                            var b = this;
                            if (!this._isTransitioning && a(this._element).hasClass(k)) {
                                var c = a.Event(j.HIDE);
                                if (a(this._element).trigger(c), !c.isDefaultPrevented()) {
                                    var d = this._getDimension();
                                    if (this._element.style[d] = this._element.getBoundingClientRect()[d] + "px", G.reflow(this._element), a(this._element).addClass(m).removeClass(l).removeClass(k), this._triggerArray.length > 0)
                                        for (var e = 0; e < this._triggerArray.length; e++) {
                                            var f = this._triggerArray[e],
                                                g = G.getSelectorFromElement(f);
                                            null !== g && (a(g).hasClass(k) || a(f).addClass(n).attr("aria-expanded", !1))
                                        }
                                    this.setTransitioning(!0);
                                    var h = function () {
                                        b.setTransitioning(!1), a(b._element).removeClass(m).addClass(l).trigger(j.HIDDEN)
                                    };
                                    this._element.style[d] = "", G.supportsTransitionEnd() ? a(this._element).one(G.TRANSITION_END, h).emulateTransitionEnd(600) : h()
                                }
                            }
                        }, g.setTransitioning = function (a) {
                            this._isTransitioning = a
                        }, g.dispose = function () {
                            a.removeData(this._element, c), this._config = null, this._parent = null, this._element = null, this._triggerArray = null, this._isTransitioning = null
                        }, g._getConfig = function (a) {
                            return (a = f({}, h, a)).toggle = Boolean(a.toggle), G.typeCheckConfig(b, a, i), a
                        }, g._getDimension = function () {
                            return a(this._element).hasClass(o) ? o : p
                        }, g._getParent = function () {
                            var b = this,
                                c = null;
                            G.isElement(this._config.parent) ? (c = this._config.parent, void 0 !== this._config.parent.jquery && (c = this._config.parent[0])) : c = a(this._config.parent)[0];
                            var e = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]';
                            return a(c).find(e).each(function (a, c) {
                                b._addAriaAndCollapsedClass(d._getTargetFromElement(c), [c])
                            }), c
                        }, g._addAriaAndCollapsedClass = function (b, c) {
                            if (b) {
                                var d = a(b).hasClass(k);
                                c.length > 0 && a(c).toggleClass(n, !d).attr("aria-expanded", d)
                            }
                        }, d._getTargetFromElement = function (b) {
                            var c = G.getSelectorFromElement(b);
                            return c ? a(c)[0] : null
                        }, d._jQueryInterface = function (b) {
                            return this.each(function () {
                                var e = a(this),
                                    g = e.data(c),
                                    i = f({}, h, e.data(), "object" == typeof b && b);
                                if (!g && i.toggle && /show|hide/.test(b) && (i.toggle = !1), g || (g = new d(this, i), e.data(c, g)), "string" == typeof b) {
                                    if (void 0 === g[b]) throw new TypeError('No method named "' + b + '"');
                                    g[b]()
                                }
                            })
                        }, e(d, null, [{
                            key: "VERSION",
                            get: function () {
                                return "4.0.0"
                            }
                        }, {
                            key: "Default",
                            get: function () {
                                return h
                            }
                        }]), d
                    }();
                return a(document).on(j.CLICK_DATA_API, q.DATA_TOGGLE, function (b) {
                    "A" === b.currentTarget.tagName && b.preventDefault();
                    var d = a(this),
                        e = G.getSelectorFromElement(this);
                    a(e).each(function () {
                        var b = a(this),
                            e = b.data(c) ? "toggle" : d.data();
                        r._jQueryInterface.call(b, e)
                    })
                }), a.fn[b] = r._jQueryInterface, a.fn[b].Constructor = r, a.fn[b].noConflict = function () {
                    return a.fn[b] = g, r._jQueryInterface
                }, r
            }(b),
            L = function (a) {
                var b = "dropdown",
                    d = "bs.dropdown",
                    g = "." + d,
                    h = ".data-api",
                    i = a.fn[b],
                    j = new RegExp("38|40|27"),
                    k = {
                        HIDE: "hide" + g,
                        HIDDEN: "hidden" + g,
                        SHOW: "show" + g,
                        SHOWN: "shown" + g,
                        CLICK: "click" + g,
                        CLICK_DATA_API: "click" + g + h,
                        KEYDOWN_DATA_API: "keydown" + g + h,
                        KEYUP_DATA_API: "keyup" + g + h
                    },
                    l = "disabled",
                    m = "show",
                    n = "dropup",
                    o = "dropright",
                    p = "dropleft",
                    q = "dropdown-menu-right",
                    r = "dropdown-menu-left",
                    s = "position-static",
                    t = '[data-toggle="dropdown"]',
                    u = ".dropdown-menu",
                    v = ".navbar-nav",
                    w = ".dropdown-menu .dropdown-item:not(.disabled)",
                    x = "top-start",
                    y = "top-end",
                    z = "bottom-start",
                    A = "bottom-end",
                    B = "right-start",
                    C = "left-start",
                    D = {
                        offset: 0,
                        flip: !0,
                        boundary: "scrollParent"
                    },
                    E = {
                        offset: "(number|string|function)",
                        flip: "boolean",
                        boundary: "(string|element)"
                    },
                    F = function () {
                        function h(a, b) {
                            this._element = a, this._popper = null, this._config = this._getConfig(b), this._menu = this._getMenuElement(), this._inNavbar = this._detectNavbar(), this._addEventListeners()
                        }
                        var i = h.prototype;
                        return i.toggle = function () {
                            if (!this._element.disabled && !a(this._element).hasClass(l)) {
                                var b = h._getParentFromElement(this._element),
                                    d = a(this._menu).hasClass(m);
                                if (h._clearMenus(), !d) {
                                    var e = {
                                            relatedTarget: this._element
                                        },
                                        f = a.Event(k.SHOW, e);
                                    if (a(b).trigger(f), !f.isDefaultPrevented()) {
                                        if (!this._inNavbar) {
                                            if (void 0 === c) throw new TypeError("Bootstrap dropdown require Popper.js (https://popper.js.org)");
                                            var g = this._element;
                                            a(b).hasClass(n) && (a(this._menu).hasClass(r) || a(this._menu).hasClass(q)) && (g = b), "scrollParent" !== this._config.boundary && a(b).addClass(s), this._popper = new c(g, this._menu, this._getPopperConfig())
                                        }
                                        "ontouchstart" in document.documentElement && 0 === a(b).closest(v).length && a("body").children().on("mouseover", null, a.noop), this._element.focus(), this._element.setAttribute("aria-expanded", !0), a(this._menu).toggleClass(m), a(b).toggleClass(m).trigger(a.Event(k.SHOWN, e))
                                    }
                                }
                            }
                        }, i.dispose = function () {
                            a.removeData(this._element, d), a(this._element).off(g), this._element = null, this._menu = null, null !== this._popper && (this._popper.destroy(), this._popper = null)
                        }, i.update = function () {
                            this._inNavbar = this._detectNavbar(), null !== this._popper && this._popper.scheduleUpdate()
                        }, i._addEventListeners = function () {
                            var b = this;
                            a(this._element).on(k.CLICK, function (a) {
                                a.preventDefault(), a.stopPropagation(), b.toggle()
                            })
                        }, i._getConfig = function (c) {
                            return c = f({}, this.constructor.Default, a(this._element).data(), c),
                                G.typeCheckConfig(b, c, this.constructor.DefaultType), c
                        }, i._getMenuElement = function () {
                            if (!this._menu) {
                                var b = h._getParentFromElement(this._element);
                                this._menu = a(b).find(u)[0]
                            }
                            return this._menu
                        }, i._getPlacement = function () {
                            var b = a(this._element).parent(),
                                c = z;
                            return b.hasClass(n) ? (c = x, a(this._menu).hasClass(q) && (c = y)) : b.hasClass(o) ? c = B : b.hasClass(p) ? c = C : a(this._menu).hasClass(q) && (c = A), c
                        }, i._detectNavbar = function () {
                            return a(this._element).closest(".navbar").length > 0
                        }, i._getPopperConfig = function () {
                            var a = this,
                                b = {};
                            return "function" == typeof this._config.offset ? b.fn = function (b) {
                                return b.offsets = f({}, b.offsets, a._config.offset(b.offsets) || {}), b
                            } : b.offset = this._config.offset, {
                                placement: this._getPlacement(),
                                modifiers: {
                                    offset: b,
                                    flip: {
                                        enabled: this._config.flip
                                    },
                                    preventOverflow: {
                                        boundariesElement: this._config.boundary
                                    }
                                }
                            }
                        }, h._jQueryInterface = function (b) {
                            return this.each(function () {
                                var c = a(this).data(d);
                                if (c || (c = new h(this, "object" == typeof b ? b : null), a(this).data(d, c)), "string" == typeof b) {
                                    if (void 0 === c[b]) throw new TypeError('No method named "' + b + '"');
                                    c[b]()
                                }
                            })
                        }, h._clearMenus = function (b) {
                            if (!b || 3 !== b.which && ("keyup" !== b.type || 9 === b.which))
                                for (var c = a.makeArray(a(t)), e = 0; e < c.length; e++) {
                                    var f = h._getParentFromElement(c[e]),
                                        g = a(c[e]).data(d),
                                        i = {
                                            relatedTarget: c[e]
                                        };
                                    if (g) {
                                        var j = g._menu;
                                        if (a(f).hasClass(m) && !(b && ("click" === b.type && /input|textarea/i.test(b.target.tagName) || "keyup" === b.type && 9 === b.which) && a.contains(f, b.target))) {
                                            var l = a.Event(k.HIDE, i);
                                            a(f).trigger(l), l.isDefaultPrevented() || ("ontouchstart" in document.documentElement && a("body").children().off("mouseover", null, a.noop), c[e].setAttribute("aria-expanded", "false"), a(j).removeClass(m), a(f).removeClass(m).trigger(a.Event(k.HIDDEN, i)))
                                        }
                                    }
                                }
                        }, h._getParentFromElement = function (b) {
                            var c, d = G.getSelectorFromElement(b);
                            return d && (c = a(d)[0]), c || b.parentNode
                        }, h._dataApiKeydownHandler = function (b) {
                            if ((/input|textarea/i.test(b.target.tagName) ? !(32 === b.which || 27 !== b.which && (40 !== b.which && 38 !== b.which || a(b.target).closest(u).length)) : j.test(b.which)) && (b.preventDefault(), b.stopPropagation(), !this.disabled && !a(this).hasClass(l))) {
                                var c = h._getParentFromElement(this),
                                    d = a(c).hasClass(m);
                                if ((d || 27 === b.which && 32 === b.which) && (!d || 27 !== b.which && 32 !== b.which)) {
                                    var e = a(c).find(w).get();
                                    if (0 !== e.length) {
                                        var f = e.indexOf(b.target);
                                        38 === b.which && f > 0 && f--, 40 === b.which && f < e.length - 1 && f++, f < 0 && (f = 0), e[f].focus()
                                    }
                                } else {
                                    if (27 === b.which) {
                                        var g = a(c).find(t)[0];
                                        a(g).trigger("focus")
                                    }
                                    a(this).trigger("click")
                                }
                            }
                        }, e(h, null, [{
                            key: "VERSION",
                            get: function () {
                                return "4.0.0"
                            }
                        }, {
                            key: "Default",
                            get: function () {
                                return D
                            }
                        }, {
                            key: "DefaultType",
                            get: function () {
                                return E
                            }
                        }]), h
                    }();
                return a(document).on(k.KEYDOWN_DATA_API, t, F._dataApiKeydownHandler).on(k.KEYDOWN_DATA_API, u, F._dataApiKeydownHandler).on(k.CLICK_DATA_API + " " + k.KEYUP_DATA_API, F._clearMenus).on(k.CLICK_DATA_API, t, function (b) {
                    b.preventDefault(), b.stopPropagation(), F._jQueryInterface.call(a(this), "toggle")
                }).on(k.CLICK_DATA_API, ".dropdown form", function (a) {
                    a.stopPropagation()
                }), a.fn[b] = F._jQueryInterface, a.fn[b].Constructor = F, a.fn[b].noConflict = function () {
                    return a.fn[b] = i, F._jQueryInterface
                }, F
            }(b),
            M = function (a) {
                var b = "modal",
                    c = "bs.modal",
                    d = "." + c,
                    g = a.fn.modal,
                    h = {
                        backdrop: !0,
                        keyboard: !0,
                        focus: !0,
                        show: !0
                    },
                    i = {
                        backdrop: "(boolean|string)",
                        keyboard: "boolean",
                        focus: "boolean",
                        show: "boolean"
                    },
                    j = {
                        HIDE: "hide" + d,
                        HIDDEN: "hidden" + d,
                        SHOW: "show" + d,
                        SHOWN: "shown" + d,
                        FOCUSIN: "focusin" + d,
                        RESIZE: "resize" + d,
                        CLICK_DISMISS: "click.dismiss" + d,
                        KEYDOWN_DISMISS: "keydown.dismiss" + d,
                        MOUSEUP_DISMISS: "mouseup.dismiss" + d,
                        MOUSEDOWN_DISMISS: "mousedown.dismiss" + d,
                        CLICK_DATA_API: "click" + d + ".data-api"
                    },
                    k = "modal-scrollbar-measure",
                    l = "modal-backdrop",
                    m = "modal-open",
                    n = "fade",
                    o = "show",
                    p = {
                        DIALOG: ".modal-dialog",
                        DATA_TOGGLE: '[data-toggle="modal"]',
                        DATA_DISMISS: '[data-dismiss="modal"]',
                        FIXED_CONTENT: ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",
                        STICKY_CONTENT: ".sticky-top",
                        NAVBAR_TOGGLER: ".navbar-toggler"
                    },
                    q = function () {
                        function g(b, c) {
                            this._config = this._getConfig(c), this._element = b, this._dialog = a(b).find(p.DIALOG)[0], this._backdrop = null, this._isShown = !1, this._isBodyOverflowing = !1, this._ignoreBackdropClick = !1, this._originalBodyPadding = 0, this._scrollbarWidth = 0
                        }
                        var q = g.prototype;
                        return q.toggle = function (a) {
                            return this._isShown ? this.hide() : this.show(a)
                        }, q.show = function (b) {
                            var c = this;
                            if (!this._isTransitioning && !this._isShown) {
                                G.supportsTransitionEnd() && a(this._element).hasClass(n) && (this._isTransitioning = !0);
                                var d = a.Event(j.SHOW, {
                                    relatedTarget: b
                                });
                                a(this._element).trigger(d), this._isShown || d.isDefaultPrevented() || (this._isShown = !0, this._checkScrollbar(), this._setScrollbar(), this._adjustDialog(), a(document.body).addClass(m), this._setEscapeEvent(), this._setResizeEvent(), a(this._element).on(j.CLICK_DISMISS, p.DATA_DISMISS, function (a) {
                                    return c.hide(a)
                                }), a(this._dialog).on(j.MOUSEDOWN_DISMISS, function () {
                                    a(c._element).one(j.MOUSEUP_DISMISS, function (b) {
                                        a(b.target).is(c._element) && (c._ignoreBackdropClick = !0)
                                    })
                                }), this._showBackdrop(function () {
                                    return c._showElement(b)
                                }))
                            }
                        }, q.hide = function (b) {
                            var c = this;
                            if (b && b.preventDefault(), !this._isTransitioning && this._isShown) {
                                var d = a.Event(j.HIDE);
                                if (a(this._element).trigger(d), this._isShown && !d.isDefaultPrevented()) {
                                    this._isShown = !1;
                                    var e = G.supportsTransitionEnd() && a(this._element).hasClass(n);
                                    e && (this._isTransitioning = !0), this._setEscapeEvent(), this._setResizeEvent(), a(document).off(j.FOCUSIN), a(this._element).removeClass(o), a(this._element).off(j.CLICK_DISMISS), a(this._dialog).off(j.MOUSEDOWN_DISMISS), e ? a(this._element).one(G.TRANSITION_END, function (a) {
                                        return c._hideModal(a)
                                    }).emulateTransitionEnd(300) : this._hideModal()
                                }
                            }
                        }, q.dispose = function () {
                            a.removeData(this._element, c), a(window, document, this._element, this._backdrop).off(d), this._config = null, this._element = null, this._dialog = null, this._backdrop = null, this._isShown = null, this._isBodyOverflowing = null, this._ignoreBackdropClick = null, this._scrollbarWidth = null
                        }, q.handleUpdate = function () {
                            this._adjustDialog()
                        }, q._getConfig = function (a) {
                            return a = f({}, h, a), G.typeCheckConfig(b, a, i), a
                        }, q._showElement = function (b) {
                            var c = this,
                                d = G.supportsTransitionEnd() && a(this._element).hasClass(n);
                            this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE || document.body.appendChild(this._element), this._element.style.display = "block", this._element.removeAttribute("aria-hidden"), this._element.scrollTop = 0, d && G.reflow(this._element), a(this._element).addClass(o), this._config.focus && this._enforceFocus();
                            var e = a.Event(j.SHOWN, {
                                    relatedTarget: b
                                }),
                                f = function () {
                                    c._config.focus && c._element.focus(), c._isTransitioning = !1, a(c._element).trigger(e)
                                };
                            d ? a(this._dialog).one(G.TRANSITION_END, f).emulateTransitionEnd(300) : f()
                        }, q._enforceFocus = function () {
                            var b = this;
                            a(document).off(j.FOCUSIN).on(j.FOCUSIN, function (c) {
                                document !== c.target && b._element !== c.target && 0 === a(b._element).has(c.target).length && b._element.focus()
                            })
                        }, q._setEscapeEvent = function () {
                            var b = this;
                            this._isShown && this._config.keyboard ? a(this._element).on(j.KEYDOWN_DISMISS, function (a) {
                                27 === a.which && (a.preventDefault(), b.hide())
                            }) : this._isShown || a(this._element).off(j.KEYDOWN_DISMISS)
                        }, q._setResizeEvent = function () {
                            var b = this;
                            this._isShown ? a(window).on(j.RESIZE, function (a) {
                                return b.handleUpdate(a)
                            }) : a(window).off(j.RESIZE)
                        }, q._hideModal = function () {
                            var b = this;
                            this._element.style.display = "none", this._element.setAttribute("aria-hidden", !0), this._isTransitioning = !1, this._showBackdrop(function () {
                                a(document.body).removeClass(m), b._resetAdjustments(), b._resetScrollbar(), a(b._element).trigger(j.HIDDEN)
                            })
                        }, q._removeBackdrop = function () {
                            this._backdrop && (a(this._backdrop).remove(), this._backdrop = null)
                        }, q._showBackdrop = function (b) {
                            var c = this,
                                d = a(this._element).hasClass(n) ? n : "";
                            if (this._isShown && this._config.backdrop) {
                                var e = G.supportsTransitionEnd() && d;
                                if (this._backdrop = document.createElement("div"), this._backdrop.className = l, d && a(this._backdrop).addClass(d), a(this._backdrop).appendTo(document.body), a(this._element).on(j.CLICK_DISMISS, function (a) {
                                    c._ignoreBackdropClick ? c._ignoreBackdropClick = !1 : a.target === a.currentTarget && ("static" === c._config.backdrop ? c._element.focus() : c.hide())
                                }), e && G.reflow(this._backdrop), a(this._backdrop).addClass(o), !b) return;
                                if (!e) return void b();
                                a(this._backdrop).one(G.TRANSITION_END, b).emulateTransitionEnd(150)
                            } else if (!this._isShown && this._backdrop) {
                                a(this._backdrop).removeClass(o);
                                var f = function () {
                                    c._removeBackdrop(), b && b()
                                };
                                G.supportsTransitionEnd() && a(this._element).hasClass(n) ? a(this._backdrop).one(G.TRANSITION_END, f).emulateTransitionEnd(150) : f()
                            } else b && b()
                        }, q._adjustDialog = function () {
                            var a = this._element.scrollHeight > document.documentElement.clientHeight;
                            !this._isBodyOverflowing && a && (this._element.style.paddingLeft = this._scrollbarWidth + "px"), this._isBodyOverflowing && !a && (this._element.style.paddingRight = this._scrollbarWidth + "px")
                        }, q._resetAdjustments = function () {
                            this._element.style.paddingLeft = "", this._element.style.paddingRight = ""
                        }, q._checkScrollbar = function () {
                            var a = document.body.getBoundingClientRect();
                            this._isBodyOverflowing = a.left + a.right < window.innerWidth, this._scrollbarWidth = this._getScrollbarWidth()
                        }, q._setScrollbar = function () {
                            var b = this;
                            if (this._isBodyOverflowing) {
                                a(p.FIXED_CONTENT).each(function (c, d) {
                                    var e = a(d)[0].style.paddingRight,
                                        f = a(d).css("padding-right");
                                    a(d).data("padding-right", e).css("padding-right", parseFloat(f) + b._scrollbarWidth + "px")
                                }), a(p.STICKY_CONTENT).each(function (c, d) {
                                    var e = a(d)[0].style.marginRight,
                                        f = a(d).css("margin-right");
                                    a(d).data("margin-right", e).css("margin-right", parseFloat(f) - b._scrollbarWidth + "px")
                                }), a(p.NAVBAR_TOGGLER).each(function (c, d) {
                                    var e = a(d)[0].style.marginRight,
                                        f = a(d).css("margin-right");
                                    a(d).data("margin-right", e).css("margin-right", parseFloat(f) + b._scrollbarWidth + "px")
                                });
                                var c = document.body.style.paddingRight,
                                    d = a("body").css("padding-right");
                                a("body").data("padding-right", c).css("padding-right", parseFloat(d) + this._scrollbarWidth + "px")
                            }
                        }, q._resetScrollbar = function () {
                            a(p.FIXED_CONTENT).each(function (b, c) {
                                var d = a(c).data("padding-right");
                                void 0 !== d && a(c).css("padding-right", d).removeData("padding-right")
                            }), a(p.STICKY_CONTENT + ", " + p.NAVBAR_TOGGLER).each(function (b, c) {
                                var d = a(c).data("margin-right");
                                void 0 !== d && a(c).css("margin-right", d).removeData("margin-right")
                            });
                            var b = a("body").data("padding-right");
                            void 0 !== b && a("body").css("padding-right", b).removeData("padding-right")
                        }, q._getScrollbarWidth = function () {
                            var a = document.createElement("div");
                            a.className = k, document.body.appendChild(a);
                            var b = a.getBoundingClientRect().width - a.clientWidth;
                            return document.body.removeChild(a), b
                        }, g._jQueryInterface = function (b, d) {
                            return this.each(function () {
                                var e = a(this).data(c),
                                    h = f({}, g.Default, a(this).data(), "object" == typeof b && b);
                                if (e || (e = new g(this, h), a(this).data(c, e)), "string" == typeof b) {
                                    if (void 0 === e[b]) throw new TypeError('No method named "' + b + '"');
                                    e[b](d)
                                } else h.show && e.show(d)
                            })
                        }, e(g, null, [{
                            key: "VERSION",
                            get: function () {
                                return "4.0.0"
                            }
                        }, {
                            key: "Default",
                            get: function () {
                                return h
                            }
                        }]), g
                    }();
                return a(document).on(j.CLICK_DATA_API, p.DATA_TOGGLE, function (b) {
                    var d, e = this,
                        g = G.getSelectorFromElement(this);
                    g && (d = a(g)[0]);
                    var h = a(d).data(c) ? "toggle" : f({}, a(d).data(), a(this).data());
                    "A" !== this.tagName && "AREA" !== this.tagName || b.preventDefault();
                    var i = a(d).one(j.SHOW, function (b) {
                        b.isDefaultPrevented() || i.one(j.HIDDEN, function () {
                            a(e).is(":visible") && e.focus()
                        })
                    });
                    q._jQueryInterface.call(a(d), h, this)
                }), a.fn.modal = q._jQueryInterface, a.fn.modal.Constructor = q, a.fn.modal.noConflict = function () {
                    return a.fn.modal = g, q._jQueryInterface
                }, q
            }(b),
            N = function (a) {
                var b = "tooltip",
                    d = "bs.tooltip",
                    g = "." + d,
                    h = a.fn[b],
                    i = new RegExp("(^|\\s)bs-tooltip\\S+", "g"),
                    j = {
                        animation: "boolean",
                        template: "string",
                        title: "(string|element|function)",
                        trigger: "string",
                        delay: "(number|object)",
                        html: "boolean",
                        selector: "(string|boolean)",
                        placement: "(string|function)",
                        offset: "(number|string)",
                        container: "(string|element|boolean)",
                        fallbackPlacement: "(string|array)",
                        boundary: "(string|element)"
                    },
                    k = {
                        AUTO: "auto",
                        TOP: "top",
                        RIGHT: "right",
                        BOTTOM: "bottom",
                        LEFT: "left"
                    },
                    l = {
                        animation: !0,
                        template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
                        trigger: "hover focus",
                        title: "",
                        delay: 0,
                        html: !1,
                        selector: !1,
                        placement: "top",
                        offset: 0,
                        container: !1,
                        fallbackPlacement: "flip",
                        boundary: "scrollParent"
                    },
                    m = "show",
                    n = "out",
                    o = {
                        HIDE: "hide" + g,
                        HIDDEN: "hidden" + g,
                        SHOW: "show" + g,
                        SHOWN: "shown" + g,
                        INSERTED: "inserted" + g,
                        CLICK: "click" + g,
                        FOCUSIN: "focusin" + g,
                        FOCUSOUT: "focusout" + g,
                        MOUSEENTER: "mouseenter" + g,
                        MOUSELEAVE: "mouseleave" + g
                    },
                    p = "fade",
                    q = "show",
                    r = ".tooltip-inner",
                    s = ".arrow",
                    t = "hover",
                    u = "focus",
                    v = "click",
                    w = "manual",
                    x = function () {
                        function h(a, b) {
                            if (void 0 === c) throw new TypeError("Bootstrap tooltips require Popper.js (https://popper.js.org)");
                            this._isEnabled = !0, this._timeout = 0, this._hoverState = "", this._activeTrigger = {}, this._popper = null, this.element = a, this.config = this._getConfig(b), this.tip = null, this._setListeners()
                        }
                        var x = h.prototype;
                        return x.enable = function () {
                            this._isEnabled = !0
                        }, x.disable = function () {
                            this._isEnabled = !1
                        }, x.toggleEnabled = function () {
                            this._isEnabled = !this._isEnabled
                        }, x.toggle = function (b) {
                            if (this._isEnabled)
                                if (b) {
                                    var c = this.constructor.DATA_KEY,
                                        d = a(b.currentTarget).data(c);
                                    d || (d = new this.constructor(b.currentTarget, this._getDelegateConfig()), a(b.currentTarget).data(c, d)), d._activeTrigger.click = !d._activeTrigger.click, d._isWithActiveTrigger() ? d._enter(null, d) : d._leave(null, d)
                                } else {
                                    if (a(this.getTipElement()).hasClass(q)) return void this._leave(null, this);
                                    this._enter(null, this)
                                }
                        }, x.dispose = function () {
                            clearTimeout(this._timeout), a.removeData(this.element, this.constructor.DATA_KEY), a(this.element).off(this.constructor.EVENT_KEY), a(this.element).closest(".modal").off("hide.bs.modal"), this.tip && a(this.tip).remove(), this._isEnabled = null, this._timeout = null, this._hoverState = null, this._activeTrigger = null, null !== this._popper && this._popper.destroy(), this._popper = null, this.element = null, this.config = null, this.tip = null
                        }, x.show = function () {
                            var b = this;
                            if ("none" === a(this.element).css("display")) throw new Error("Please use show on visible elements");
                            var d = a.Event(this.constructor.Event.SHOW);
                            if (this.isWithContent() && this._isEnabled) {
                                a(this.element).trigger(d);
                                var e = a.contains(this.element.ownerDocument.documentElement, this.element);
                                if (d.isDefaultPrevented() || !e) return;
                                var f = this.getTipElement(),
                                    g = G.getUID(this.constructor.NAME);
                                f.setAttribute("id", g), this.element.setAttribute("aria-describedby", g), this.setContent(), this.config.animation && a(f).addClass(p);
                                var i = "function" == typeof this.config.placement ? this.config.placement.call(this, f, this.element) : this.config.placement,
                                    j = this._getAttachment(i);
                                this.addAttachmentClass(j);
                                var k = !1 === this.config.container ? document.body : a(this.config.container);
                                a(f).data(this.constructor.DATA_KEY, this), a.contains(this.element.ownerDocument.documentElement, this.tip) || a(f).appendTo(k), a(this.element).trigger(this.constructor.Event.INSERTED), this._popper = new c(this.element, f, {
                                    placement: j,
                                    modifiers: {
                                        offset: {
                                            offset: this.config.offset
                                        },
                                        flip: {
                                            behavior: this.config.fallbackPlacement
                                        },
                                        arrow: {
                                            element: s
                                        },
                                        preventOverflow: {
                                            boundariesElement: this.config.boundary
                                        }
                                    },
                                    onCreate: function (a) {
                                        a.originalPlacement !== a.placement && b._handlePopperPlacementChange(a)
                                    },
                                    onUpdate: function (a) {
                                        b._handlePopperPlacementChange(a)
                                    }
                                }), a(f).addClass(q), "ontouchstart" in document.documentElement && a("body").children().on("mouseover", null, a.noop);
                                var l = function () {
                                    b.config.animation && b._fixTransition();
                                    var c = b._hoverState;
                                    b._hoverState = null, a(b.element).trigger(b.constructor.Event.SHOWN), c === n && b._leave(null, b)
                                };
                                G.supportsTransitionEnd() && a(this.tip).hasClass(p) ? a(this.tip).one(G.TRANSITION_END, l).emulateTransitionEnd(h._TRANSITION_DURATION) : l()
                            }
                        }, x.hide = function (b) {
                            var c = this,
                                d = this.getTipElement(),
                                e = a.Event(this.constructor.Event.HIDE),
                                f = function () {
                                    c._hoverState !== m && d.parentNode && d.parentNode.removeChild(d), c._cleanTipClass(), c.element.removeAttribute("aria-describedby"), a(c.element).trigger(c.constructor.Event.HIDDEN), null !== c._popper && c._popper.destroy(), b && b()
                                };
                            a(this.element).trigger(e), e.isDefaultPrevented() || (a(d).removeClass(q), "ontouchstart" in document.documentElement && a("body").children().off("mouseover", null, a.noop), this._activeTrigger[v] = !1, this._activeTrigger[u] = !1, this._activeTrigger[t] = !1, G.supportsTransitionEnd() && a(this.tip).hasClass(p) ? a(d).one(G.TRANSITION_END, f).emulateTransitionEnd(150) : f(), this._hoverState = "")
                        }, x.update = function () {
                            null !== this._popper && this._popper.scheduleUpdate()
                        }, x.isWithContent = function () {
                            return Boolean(this.getTitle())
                        }, x.addAttachmentClass = function (b) {
                            a(this.getTipElement()).addClass("bs-tooltip-" + b)
                        }, x.getTipElement = function () {
                            return this.tip = this.tip || a(this.config.template)[0], this.tip
                        }, x.setContent = function () {
                            var b = a(this.getTipElement());
                            this.setElementContent(b.find(r), this.getTitle()), b.removeClass(p + " " + q)
                        }, x.setElementContent = function (b, c) {
                            var d = this.config.html;
                            "object" == typeof c && (c.nodeType || c.jquery) ? d ? a(c).parent().is(b) || b.empty().append(c) : b.text(a(c).text()) : b[d ? "html" : "text"](c)
                        }, x.getTitle = function () {
                            var a = this.element.getAttribute("data-original-title");
                            return a || (a = "function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title), a
                        }, x._getAttachment = function (a) {
                            return k[a.toUpperCase()]
                        }, x._setListeners = function () {
                            var b = this;
                            this.config.trigger.split(" ").forEach(function (c) {
                                if ("click" === c) a(b.element).on(b.constructor.Event.CLICK, b.config.selector, function (a) {
                                    return b.toggle(a)
                                });
                                else if (c !== w) {
                                    var d = c === t ? b.constructor.Event.MOUSEENTER : b.constructor.Event.FOCUSIN,
                                        e = c === t ? b.constructor.Event.MOUSELEAVE : b.constructor.Event.FOCUSOUT;
                                    a(b.element).on(d, b.config.selector, function (a) {
                                        return b._enter(a)
                                    }).on(e, b.config.selector, function (a) {
                                        return b._leave(a)
                                    })
                                }
                                a(b.element).closest(".modal").on("hide.bs.modal", function () {
                                    return b.hide()
                                })
                            }), this.config.selector ? this.config = f({}, this.config, {
                                trigger: "manual",
                                selector: ""
                            }) : this._fixTitle()
                        }, x._fixTitle = function () {
                            var a = typeof this.element.getAttribute("data-original-title");
                            (this.element.getAttribute("title") || "string" !== a) && (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""), this.element.setAttribute("title", ""))
                        }, x._enter = function (b, c) {
                            var d = this.constructor.DATA_KEY;
                            (c = c || a(b.currentTarget).data(d)) || (c = new this.constructor(b.currentTarget, this._getDelegateConfig()), a(b.currentTarget).data(d, c)), b && (c._activeTrigger["focusin" === b.type ? u : t] = !0), a(c.getTipElement()).hasClass(q) || c._hoverState === m ? c._hoverState = m : (clearTimeout(c._timeout), c._hoverState = m, c.config.delay && c.config.delay.show ? c._timeout = setTimeout(function () {
                                c._hoverState === m && c.show()
                            }, c.config.delay.show) : c.show())
                        }, x._leave = function (b, c) {
                            var d = this.constructor.DATA_KEY;
                            (c = c || a(b.currentTarget).data(d)) || (c = new this.constructor(b.currentTarget, this._getDelegateConfig()), a(b.currentTarget).data(d, c)), b && (c._activeTrigger["focusout" === b.type ? u : t] = !1), c._isWithActiveTrigger() || (clearTimeout(c._timeout), c._hoverState = n, c.config.delay && c.config.delay.hide ? c._timeout = setTimeout(function () {
                                c._hoverState === n && c.hide()
                            }, c.config.delay.hide) : c.hide())
                        }, x._isWithActiveTrigger = function () {
                            for (var a in this._activeTrigger)
                                if (this._activeTrigger[a]) return !0;
                            return !1
                        }, x._getConfig = function (c) {
                            return "number" == typeof (c = f({}, this.constructor.Default, a(this.element).data(), c)).delay && (c.delay = {
                                show: c.delay,
                                hide: c.delay
                            }), "number" == typeof c.title && (c.title = c.title.toString()), "number" == typeof c.content && (c.content = c.content.toString()), G.typeCheckConfig(b, c, this.constructor.DefaultType), c
                        }, x._getDelegateConfig = function () {
                            var a = {};
                            if (this.config)
                                for (var b in this.config) this.constructor.Default[b] !== this.config[b] && (a[b] = this.config[b]);
                            return a
                        }, x._cleanTipClass = function () {
                            var b = a(this.getTipElement()),
                                c = b.attr("class").match(i);
                            null !== c && c.length > 0 && b.removeClass(c.join(""))
                        }, x._handlePopperPlacementChange = function (a) {
                            this._cleanTipClass(), this.addAttachmentClass(this._getAttachment(a.placement))
                        }, x._fixTransition = function () {
                            var b = this.getTipElement(),
                                c = this.config.animation;
                            null === b.getAttribute("x-placement") && (a(b).removeClass(p), this.config.animation = !1, this.hide(), this.show(), this.config.animation = c)
                        }, h._jQueryInterface = function (b) {
                            return this.each(function () {
                                var c = a(this).data(d),
                                    e = "object" == typeof b && b;
                                if ((c || !/dispose|hide/.test(b)) && (c || (c = new h(this, e), a(this).data(d, c)), "string" == typeof b)) {
                                    if (void 0 === c[b]) throw new TypeError('No method named "' + b + '"');
                                    c[b]()
                                }
                            })
                        }, e(h, null, [{
                            key: "VERSION",
                            get: function () {
                                return "4.0.0"
                            }
                        }, {
                            key: "Default",
                            get: function () {
                                return l
                            }
                        }, {
                            key: "NAME",
                            get: function () {
                                return b
                            }
                        }, {
                            key: "DATA_KEY",
                            get: function () {
                                return d
                            }
                        }, {
                            key: "Event",
                            get: function () {
                                return o
                            }
                        }, {
                            key: "EVENT_KEY",
                            get: function () {
                                return g
                            }
                        }, {
                            key: "DefaultType",
                            get: function () {
                                return j
                            }
                        }]), h
                    }();
                return a.fn[b] = x._jQueryInterface, a.fn[b].Constructor = x, a.fn[b].noConflict = function () {
                    return a.fn[b] = h, x._jQueryInterface
                }, x
            }(b),
            O = function (a) {
                var b = "popover",
                    c = "bs.popover",
                    d = "." + c,
                    g = a.fn[b],
                    h = new RegExp("(^|\\s)bs-popover\\S+", "g"),
                    i = f({}, N.Default, {
                        placement: "right",
                        trigger: "click",
                        content: "",
                        template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
                    }),
                    j = f({}, N.DefaultType, {
                        content: "(string|element|function)"
                    }),
                    k = "fade",
                    l = "show",
                    m = ".popover-header",
                    n = ".popover-body",
                    o = {
                        HIDE: "hide" + d,
                        HIDDEN: "hidden" + d,
                        SHOW: "show" + d,
                        SHOWN: "shown" + d,
                        INSERTED: "inserted" + d,
                        CLICK: "click" + d,
                        FOCUSIN: "focusin" + d,
                        FOCUSOUT: "focusout" + d,
                        MOUSEENTER: "mouseenter" + d,
                        MOUSELEAVE: "mouseleave" + d
                    },
                    p = function (f) {
                        function g() {
                            return f.apply(this, arguments) || this
                        }
                        var p, q;
                        q = f, (p = g).prototype = Object.create(q.prototype), p.prototype.constructor = p, p.__proto__ = q;
                        var r = g.prototype;
                        return r.isWithContent = function () {
                            return this.getTitle() || this._getContent()
                        }, r.addAttachmentClass = function (b) {
                            a(this.getTipElement()).addClass("bs-popover-" + b)
                        }, r.getTipElement = function () {
                            return this.tip = this.tip || a(this.config.template)[0], this.tip
                        }, r.setContent = function () {
                            var b = a(this.getTipElement());
                            this.setElementContent(b.find(m), this.getTitle());
                            var c = this._getContent();
                            "function" == typeof c && (c = c.call(this.element)), this.setElementContent(b.find(n), c), b.removeClass(k + " " + l)
                        }, r._getContent = function () {
                            return this.element.getAttribute("data-content") || this.config.content
                        }, r._cleanTipClass = function () {
                            var b = a(this.getTipElement()),
                                c = b.attr("class").match(h);
                            null !== c && c.length > 0 && b.removeClass(c.join(""))
                        }, g._jQueryInterface = function (b) {
                            return this.each(function () {
                                var d = a(this).data(c),
                                    e = "object" == typeof b ? b : null;
                                if ((d || !/destroy|hide/.test(b)) && (d || (d = new g(this, e), a(this).data(c, d)), "string" == typeof b)) {
                                    if (void 0 === d[b]) throw new TypeError('No method named "' + b + '"');
                                    d[b]()
                                }
                            })
                        }, e(g, null, [{
                            key: "VERSION",
                            get: function () {
                                return "4.0.0"
                            }
                        }, {
                            key: "Default",
                            get: function () {
                                return i
                            }
                        }, {
                            key: "NAME",
                            get: function () {
                                return b
                            }
                        }, {
                            key: "DATA_KEY",
                            get: function () {
                                return c
                            }
                        }, {
                            key: "Event",
                            get: function () {
                                return o
                            }
                        }, {
                            key: "EVENT_KEY",
                            get: function () {
                                return d
                            }
                        }, {
                            key: "DefaultType",
                            get: function () {
                                return j
                            }
                        }]), g
                    }(N);
                return a.fn[b] = p._jQueryInterface, a.fn[b].Constructor = p, a.fn[b].noConflict = function () {
                    return a.fn[b] = g, p._jQueryInterface
                }, p
            }(b),
            P = function (a) {
                var b = "scrollspy",
                    c = "bs.scrollspy",
                    d = "." + c,
                    g = a.fn[b],
                    h = {
                        offset: 10,
                        method: "auto",
                        target: ""
                    },
                    i = {
                        offset: "number",
                        method: "string",
                        target: "(string|element)"
                    },
                    j = {
                        ACTIVATE: "activate" + d,
                        SCROLL: "scroll" + d,
                        LOAD_DATA_API: "load" + d + ".data-api"
                    },
                    k = "dropdown-item",
                    l = "active",
                    m = {
                        DATA_SPY: '[data-spy="scroll"]',
                        ACTIVE: ".active",
                        NAV_LIST_GROUP: ".nav, .list-group",
                        NAV_LINKS: ".nav-link",
                        NAV_ITEMS: ".nav-item",
                        LIST_ITEMS: ".list-group-item",
                        DROPDOWN: ".dropdown",
                        DROPDOWN_ITEMS: ".dropdown-item",
                        DROPDOWN_TOGGLE: ".dropdown-toggle"
                    },
                    n = "offset",
                    o = "position",
                    p = function () {
                        function g(b, c) {
                            var d = this;
                            this._element = b, this._scrollElement = "BODY" === b.tagName ? window : b, this._config = this._getConfig(c), this._selector = this._config.target + " " + m.NAV_LINKS + "," + this._config.target + " " + m.LIST_ITEMS + "," + this._config.target + " " + m.DROPDOWN_ITEMS, this._offsets = [], this._targets = [], this._activeTarget = null, this._scrollHeight = 0, a(this._scrollElement).on(j.SCROLL, function (a) {
                                return d._process(a)
                            }), this.refresh(), this._process()
                        }
                        var p = g.prototype;
                        return p.refresh = function () {
                            var b = this,
                                c = this._scrollElement === this._scrollElement.window ? n : o,
                                d = "auto" === this._config.method ? c : this._config.method,
                                e = d === o ? this._getScrollTop() : 0;
                            this._offsets = [], this._targets = [], this._scrollHeight = this._getScrollHeight(), a.makeArray(a(this._selector)).map(function (b) {
                                var c, f = G.getSelectorFromElement(b);
                                if (f && (c = a(f)[0]), c) {
                                    var g = c.getBoundingClientRect();
                                    if (g.width || g.height) return [a(c)[d]().top + e, f]
                                }
                                return null
                            }).filter(function (a) {
                                return a
                            }).sort(function (a, b) {
                                return a[0] - b[0]
                            }).forEach(function (a) {
                                b._offsets.push(a[0]), b._targets.push(a[1])
                            })
                        }, p.dispose = function () {
                            a.removeData(this._element, c), a(this._scrollElement).off(d), this._element = null, this._scrollElement = null, this._config = null, this._selector = null, this._offsets = null, this._targets = null, this._activeTarget = null, this._scrollHeight = null
                        }, p._getConfig = function (c) {
                            if ("string" != typeof (c = f({}, h, c)).target) {
                                var d = a(c.target).attr("id");
                                d || (d = G.getUID(b), a(c.target).attr("id", d)), c.target = "#" + d
                            }
                            return G.typeCheckConfig(b, c, i), c
                        }, p._getScrollTop = function () {
                            return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop
                        }, p._getScrollHeight = function () {
                            return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
                        }, p._getOffsetHeight = function () {
                            return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height
                        }, p._process = function () {
                            var a = this._getScrollTop() + this._config.offset,
                                b = this._getScrollHeight(),
                                c = this._config.offset + b - this._getOffsetHeight();
                            if (this._scrollHeight !== b && this.refresh(), a >= c) {
                                var d = this._targets[this._targets.length - 1];
                                this._activeTarget !== d && this._activate(d)
                            } else {
                                if (this._activeTarget && a < this._offsets[0] && this._offsets[0] > 0) return this._activeTarget = null, void this._clear();
                                for (var e = this._offsets.length; e--;) this._activeTarget !== this._targets[e] && a >= this._offsets[e] && (void 0 === this._offsets[e + 1] || a < this._offsets[e + 1]) && this._activate(this._targets[e])
                            }
                        }, p._activate = function (b) {
                            this._activeTarget = b, this._clear();
                            var c = this._selector.split(",");
                            c = c.map(function (a) {
                                return a + '[data-target="' + b + '"],' + a + '[href="' + b + '"]'
                            });
                            var d = a(c.join(","));
                            d.hasClass(k) ? (d.closest(m.DROPDOWN).find(m.DROPDOWN_TOGGLE).addClass(l), d.addClass(l)) : (d.addClass(l), d.parents(m.NAV_LIST_GROUP).prev(m.NAV_LINKS + ", " + m.LIST_ITEMS).addClass(l), d.parents(m.NAV_LIST_GROUP).prev(m.NAV_ITEMS).children(m.NAV_LINKS).addClass(l)), a(this._scrollElement).trigger(j.ACTIVATE, {
                                relatedTarget: b
                            })
                        }, p._clear = function () {
                            a(this._selector).filter(m.ACTIVE).removeClass(l)
                        }, g._jQueryInterface = function (b) {
                            return this.each(function () {
                                var d = a(this).data(c);
                                if (d || (d = new g(this, "object" == typeof b && b), a(this).data(c, d)), "string" == typeof b) {
                                    if (void 0 === d[b]) throw new TypeError('No method named "' + b + '"');
                                    d[b]()
                                }
                            })
                        }, e(g, null, [{
                            key: "VERSION",
                            get: function () {
                                return "4.0.0"
                            }
                        }, {
                            key: "Default",
                            get: function () {
                                return h
                            }
                        }]), g
                    }();
                return a(window).on(j.LOAD_DATA_API, function () {
                    for (var b = a.makeArray(a(m.DATA_SPY)), c = b.length; c--;) {
                        var d = a(b[c]);
                        p._jQueryInterface.call(d, d.data())
                    }
                }), a.fn[b] = p._jQueryInterface, a.fn[b].Constructor = p, a.fn[b].noConflict = function () {
                    return a.fn[b] = g, p._jQueryInterface
                }, p
            }(b),
            Q = function (a) {
                var b = "bs.tab",
                    c = "." + b,
                    d = a.fn.tab,
                    f = {
                        HIDE: "hide" + c,
                        HIDDEN: "hidden" + c,
                        SHOW: "show" + c,
                        SHOWN: "shown" + c,
                        CLICK_DATA_API: "click.bs.tab.data-api"
                    },
                    g = "dropdown-menu",
                    h = "active",
                    i = "disabled",
                    j = "fade",
                    k = "show",
                    l = ".dropdown",
                    m = ".nav, .list-group",
                    n = ".active",
                    o = "> li > .active",
                    p = ".dropdown-toggle",
                    q = "> .dropdown-menu .active",
                    r = function () {
                        function c(a) {
                            this._element = a
                        }
                        var d = c.prototype;
                        return d.show = function () {
                            var b = this;
                            if (!(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && a(this._element).hasClass(h) || a(this._element).hasClass(i))) {
                                var c, d, e = a(this._element).closest(m)[0],
                                    g = G.getSelectorFromElement(this._element);
                                if (e) {
                                    var j = "UL" === e.nodeName ? o : n;
                                    d = (d = a.makeArray(a(e).find(j)))[d.length - 1]
                                }
                                var k = a.Event(f.HIDE, {
                                        relatedTarget: this._element
                                    }),
                                    l = a.Event(f.SHOW, {
                                        relatedTarget: d
                                    });
                                if (d && a(d).trigger(k), a(this._element).trigger(l), !l.isDefaultPrevented() && !k.isDefaultPrevented()) {
                                    g && (c = a(g)[0]), this._activate(this._element, e);
                                    var p = function () {
                                        var c = a.Event(f.HIDDEN, {
                                                relatedTarget: b._element
                                            }),
                                            e = a.Event(f.SHOWN, {
                                                relatedTarget: d
                                            });
                                        a(d).trigger(c), a(b._element).trigger(e)
                                    };
                                    c ? this._activate(c, c.parentNode, p) : p()
                                }
                            }
                        }, d.dispose = function () {
                            a.removeData(this._element, b), this._element = null
                        }, d._activate = function (b, c, d) {
                            var e = this,
                                f = ("UL" === c.nodeName ? a(c).find(o) : a(c).children(n))[0],
                                g = d && G.supportsTransitionEnd() && f && a(f).hasClass(j),
                                h = function () {
                                    return e._transitionComplete(b, f, d)
                                };
                            f && g ? a(f).one(G.TRANSITION_END, h).emulateTransitionEnd(150) : h()
                        }, d._transitionComplete = function (b, c, d) {
                            if (c) {
                                a(c).removeClass(k + " " + h);
                                var e = a(c.parentNode).find(q)[0];
                                e && a(e).removeClass(h), "tab" === c.getAttribute("role") && c.setAttribute("aria-selected", !1)
                            }
                            if (a(b).addClass(h), "tab" === b.getAttribute("role") && b.setAttribute("aria-selected", !0), G.reflow(b), a(b).addClass(k), b.parentNode && a(b.parentNode).hasClass(g)) {
                                var f = a(b).closest(l)[0];
                                f && a(f).find(p).addClass(h), b.setAttribute("aria-expanded", !0)
                            }
                            d && d()
                        }, c._jQueryInterface = function (d) {
                            return this.each(function () {
                                var e = a(this),
                                    f = e.data(b);
                                if (f || (f = new c(this), e.data(b, f)), "string" == typeof d) {
                                    if (void 0 === f[d]) throw new TypeError('No method named "' + d + '"');
                                    f[d]()
                                }
                            })
                        }, e(c, null, [{
                            key: "VERSION",
                            get: function () {
                                return "4.0.0"
                            }
                        }]), c
                    }();
                return a(document).on(f.CLICK_DATA_API, '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]', function (b) {
                    b.preventDefault(), r._jQueryInterface.call(a(this), "show")
                }), a.fn.tab = r._jQueryInterface, a.fn.tab.Constructor = r, a.fn.tab.noConflict = function () {
                    return a.fn.tab = d, r._jQueryInterface
                }, r
            }(b);
        ! function (a) {
            if (void 0 === a) throw new TypeError("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");
            var b = a.fn.jquery.split(" ")[0].split(".");
            if (b[0] < 2 && b[1] < 9 || 1 === b[0] && 9 === b[1] && b[2] < 1 || b[0] >= 4) throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0")
        }(b), a.Util = G, a.Alert = H, a.Button = I, a.Carousel = J, a.Collapse = K, a.Dropdown = L, a.Modal = M, a.Popover = O, a.Scrollspy = P, a.Tab = Q, a.Tooltip = N, Object.defineProperty(a, "__esModule", {
            value: !0
        })
    });
var objectFitImages = function () {
    "use strict";

    function a(a, b) {
        return "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='" + a + "' height='" + b + "'%3E%3C/svg%3E"
    }

    function b(a) {
        if (a.srcset && !p && window.picturefill) {
            var b = window.picturefill._;
            a[b.ns] && a[b.ns].evaled || b.fillImg(a, {
                reselect: !0
            }), a[b.ns].curSrc || (a[b.ns].supported = !1, b.fillImg(a, {
                reselect: !0
            })), a.currentSrc = a[b.ns].curSrc || a.src
        }
    }

    function c(a) {
        for (var b, c = getComputedStyle(a).fontFamily, d = {}; null !== (b = k.exec(c));) d[b[1]] = b[2];
        return d
    }

    function d(b, c, d) {
        var e = a(c || 1, d || 0);
        q.call(b, "src") !== e && r.call(b, "src", e)
    }

    function e(a, b) {
        a.naturalWidth ? b(a) : setTimeout(e, 100, a, b)
    }

    function f(a) {
        var f = c(a),
            h = a[j];
        if (f["object-fit"] = f["object-fit"] || "fill", !h.img) {
            if ("fill" === f["object-fit"]) return;
            if (!h.skipTest && m && !f["object-position"]) return
        }
        if (!h.img) {
            h.img = new Image(a.width, a.height), h.img.srcset = q.call(a, "data-ofi-srcset") || a.srcset, h.img.src = q.call(a, "data-ofi-src") || a.src, r.call(a, "data-ofi-src", a.src), a.srcset && r.call(a, "data-ofi-srcset", a.srcset), d(a, a.naturalWidth || a.width, a.naturalHeight || a.height), a.srcset && (a.srcset = "");
            try {
                g(a)
            } catch (i) {
                window.console && console.warn("https://bit.ly/ofi-old-browser")
            }
        }
        b(h.img), a.style.backgroundImage = 'url("' + (h.img.currentSrc || h.img.src).replace(/"/g, '\\"') + '")', a.style.backgroundPosition = f["object-position"] || "center", a.style.backgroundRepeat = "no-repeat", a.style.backgroundOrigin = "content-box", /scale-down/.test(f["object-fit"]) ? e(h.img, function () {
            h.img.naturalWidth > a.width || h.img.naturalHeight > a.height ? a.style.backgroundSize = "contain" : a.style.backgroundSize = "auto"
        }) : a.style.backgroundSize = f["object-fit"].replace("none", "auto").replace("fill", "100% 100%"), e(h.img, function (b) {
            d(a, b.naturalWidth, b.naturalHeight)
        })
    }

    function g(a) {
        var b = {
            get: function (b) {
                return a[j].img[b || "src"]
            },
            set: function (b, c) {
                return a[j].img[c || "src"] = b, r.call(a, "data-ofi-" + c, b), f(a), b
            }
        };
        Object.defineProperty(a, "src", b), Object.defineProperty(a, "currentSrc", {
            get: function () {
                return b.get("currentSrc")
            }
        }), Object.defineProperty(a, "srcset", {
            get: function () {
                return b.get("srcset")
            },
            set: function (a) {
                return b.set(a, "srcset")
            }
        })
    }

    function h() {
        function a(a, b) {
            return a[j] && a[j].img && ("src" === b || "srcset" === b) ? a[j].img : a
        }
        n || (HTMLImageElement.prototype.getAttribute = function (b) {
            return q.call(a(this, b), b)
        }, HTMLImageElement.prototype.setAttribute = function (b, c) {
            return r.call(a(this, b), b, String(c))
        })
    }

    function i(a, b) {
        var c = !s && !a;
        if (b = b || {}, a = a || "img", n && !b.skipTest || !o) return !1;
        "img" === a ? a = document.getElementsByTagName("img") : "string" == typeof a ? a = document.querySelectorAll(a) : "length" in a || (a = [a]);
        for (var d = 0; d < a.length; d++) a[d][j] = a[d][j] || {
            skipTest: b.skipTest
        }, f(a[d]);
        c && (document.body.addEventListener("load", function (a) {
            "IMG" === a.target.tagName && i(a.target, {
                skipTest: b.skipTest
            })
        }, !0), s = !0, a = "img"), b.watchMQ && window.addEventListener("resize", i.bind(null, a, {
            skipTest: b.skipTest
        }))
    }
    var j = "bfred-it:object-fit-images",
        k = /(object-fit|object-position)\s*:\s*([-\w\s%]+)/g,
        l = "undefined" == typeof Image ? {
            style: {
                "object-position": 1
            }
        } : new Image,
        m = "object-fit" in l.style,
        n = "object-position" in l.style,
        o = "background-size" in l.style,
        p = "string" == typeof l.currentSrc,
        q = l.getAttribute,
        r = l.setAttribute,
        s = !1;
    return i.supportsObjectFit = m, i.supportsObjectPosition = n, h(), i
}();
! function (a) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], a) : "undefined" != typeof exports ? module.exports = a(require("jquery")) : a(jQuery)
}(function (a) {
    "use strict";
    var b = window.Slick || {};
    b = function () {
        function b(b, d) {
            var e, f = this;
            f.defaults = {
                accessibility: !0,
                adaptiveHeight: !1,
                appendArrows: a(b),
                appendDots: a(b),
                arrows: !0,
                asNavFor: null,
                prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
                nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
                autoplay: !1,
                autoplaySpeed: 3e3,
                centerMode: !1,
                centerPadding: "50px",
                cssEase: "ease",
                customPaging: function (b, c) {
                    return a('<button type="button" />').text(c + 1)
                },
                dots: !1,
                dotsClass: "slick-dots",
                draggable: !0,
                easing: "linear",
                edgeFriction: .35,
                fade: !1,
                focusOnSelect: !1,
                focusOnChange: !1,
                infinite: !0,
                initialSlide: 0,
                lazyLoad: "ondemand",
                mobileFirst: !1,
                pauseOnHover: !0,
                pauseOnFocus: !0,
                pauseOnDotsHover: !1,
                respondTo: "window",
                responsive: null,
                rows: 1,
                rtl: !1,
                slide: "",
                slidesPerRow: 1,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 500,
                swipe: !0,
                swipeToSlide: !1,
                touchMove: !0,
                touchThreshold: 5,
                useCSS: !0,
                useTransform: !0,
                variableWidth: !1,
                vertical: !1,
                verticalSwiping: !1,
                waitForAnimate: !0,
                zIndex: 1e3
            }, f.initials = {
                animating: !1,
                dragging: !1,
                autoPlayTimer: null,
                currentDirection: 0,
                currentLeft: null,
                currentSlide: 0,
                direction: 1,
                $dots: null,
                listWidth: null,
                listHeight: null,
                loadIndex: 0,
                $nextArrow: null,
                $prevArrow: null,
                scrolling: !1,
                slideCount: null,
                slideWidth: null,
                $slideTrack: null,
                $slides: null,
                sliding: !1,
                slideOffset: 0,
                swipeLeft: null,
                swiping: !1,
                $list: null,
                touchObject: {},
                transformsEnabled: !1,
                unslicked: !1
            }, a.extend(f, f.initials), f.activeBreakpoint = null, f.animType = null, f.animProp = null, f.breakpoints = [], f.breakpointSettings = [], f.cssTransitions = !1, f.focussed = !1, f.interrupted = !1, f.hidden = "hidden", f.paused = !0, f.positionProp = null, f.respondTo = null, f.rowCount = 1, f.shouldClick = !0, f.$slider = a(b), f.$slidesCache = null, f.transformType = null, f.transitionType = null, f.visibilityChange = "visibilitychange", f.windowWidth = 0, f.windowTimer = null, e = a(b).data("slick") || {}, f.options = a.extend({}, f.defaults, d, e), f.currentSlide = f.options.initialSlide, f.originalSettings = f.options, void 0 !== document.mozHidden ? (f.hidden = "mozHidden", f.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (f.hidden = "webkitHidden", f.visibilityChange = "webkitvisibilitychange"), f.autoPlay = a.proxy(f.autoPlay, f), f.autoPlayClear = a.proxy(f.autoPlayClear, f), f.autoPlayIterator = a.proxy(f.autoPlayIterator, f), f.changeSlide = a.proxy(f.changeSlide, f), f.clickHandler = a.proxy(f.clickHandler, f), f.selectHandler = a.proxy(f.selectHandler, f), f.setPosition = a.proxy(f.setPosition, f), f.swipeHandler = a.proxy(f.swipeHandler, f), f.dragHandler = a.proxy(f.dragHandler, f), f.keyHandler = a.proxy(f.keyHandler, f), f.instanceUid = c++, f.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, f.registerBreakpoints(), f.init(!0)
        }
        var c = 0;
        return b
    }(), b.prototype.activateADA = function () {
        this.$slideTrack.find(".slick-active").attr({
            "aria-hidden": "false"
        }).find("a, input, button, select").attr({
            tabindex: "0"
        })
    }, b.prototype.addSlide = b.prototype.slickAdd = function (b, c, d) {
        var e = this;
        if ("boolean" == typeof c) d = c, c = null;
        else if (c < 0 || c >= e.slideCount) return !1;
        e.unload(), "number" == typeof c ? 0 === c && 0 === e.$slides.length ? a(b).appendTo(e.$slideTrack) : d ? a(b).insertBefore(e.$slides.eq(c)) : a(b).insertAfter(e.$slides.eq(c)) : !0 === d ? a(b).prependTo(e.$slideTrack) : a(b).appendTo(e.$slideTrack), e.$slides = e.$slideTrack.children(this.options.slide), e.$slideTrack.children(this.options.slide).detach(), e.$slideTrack.append(e.$slides), e.$slides.each(function (b, c) {
            a(c).attr("data-slick-index", b)
        }), e.$slidesCache = e.$slides, e.reinit()
    }, b.prototype.animateHeight = function () {
        var a = this;
        if (1 === a.options.slidesToShow && !0 === a.options.adaptiveHeight && !1 === a.options.vertical) {
            var b = a.$slides.eq(a.currentSlide).outerHeight(!0);
            a.$list.animate({
                height: b
            }, a.options.speed)
        }
    }, b.prototype.animateSlide = function (b, c) {
        var d = {},
            e = this;
        e.animateHeight(), !0 === e.options.rtl && !1 === e.options.vertical && (b = -b), !1 === e.transformsEnabled ? !1 === e.options.vertical ? e.$slideTrack.animate({
            left: b
        }, e.options.speed, e.options.easing, c) : e.$slideTrack.animate({
            top: b
        }, e.options.speed, e.options.easing, c) : !1 === e.cssTransitions ? (!0 === e.options.rtl && (e.currentLeft = -e.currentLeft), a({
            animStart: e.currentLeft
        }).animate({
            animStart: b
        }, {
            duration: e.options.speed,
            easing: e.options.easing,
            step: function (a) {
                a = Math.ceil(a), !1 === e.options.vertical ? (d[e.animType] = "translate(" + a + "px, 0px)", e.$slideTrack.css(d)) : (d[e.animType] = "translate(0px," + a + "px)", e.$slideTrack.css(d))
            },
            complete: function () {
                c && c.call()
            }
        })) : (e.applyTransition(), b = Math.ceil(b), !1 === e.options.vertical ? d[e.animType] = "translate3d(" + b + "px, 0px, 0px)" : d[e.animType] = "translate3d(0px," + b + "px, 0px)", e.$slideTrack.css(d), c && setTimeout(function () {
            e.disableTransition(), c.call()
        }, e.options.speed))
    }, b.prototype.getNavTarget = function () {
        var b = this,
            c = b.options.asNavFor;
        return c && null !== c && (c = a(c).not(b.$slider)), c
    }, b.prototype.asNavFor = function (b) {
        var c = this,
            d = c.getNavTarget();
        null !== d && "object" == typeof d && d.each(function () {
            var c = a(this).slick("getSlick");
            c.unslicked || c.slideHandler(b, !0)
        })
    }, b.prototype.applyTransition = function (a) {
        var b = this,
            c = {};
        !1 === b.options.fade ? c[b.transitionType] = b.transformType + " " + b.options.speed + "ms " + b.options.cssEase : c[b.transitionType] = "opacity " + b.options.speed + "ms " + b.options.cssEase, !1 === b.options.fade ? b.$slideTrack.css(c) : b.$slides.eq(a).css(c)
    }, b.prototype.autoPlay = function () {
        var a = this;
        a.autoPlayClear(), a.slideCount > a.options.slidesToShow && (a.autoPlayTimer = setInterval(a.autoPlayIterator, a.options.autoplaySpeed))
    }, b.prototype.autoPlayClear = function () {
        var a = this;
        a.autoPlayTimer && clearInterval(a.autoPlayTimer)
    }, b.prototype.autoPlayIterator = function () {
        var a = this,
            b = a.currentSlide + a.options.slidesToScroll;
        a.paused || a.interrupted || a.focussed || (!1 === a.options.infinite && (1 === a.direction && a.currentSlide + 1 === a.slideCount - 1 ? a.direction = 0 : 0 === a.direction && (b = a.currentSlide - a.options.slidesToScroll, a.currentSlide - 1 == 0 && (a.direction = 1))), a.slideHandler(b))
    }, b.prototype.buildArrows = function () {
        var b = this;
        !0 === b.options.arrows && (b.$prevArrow = a(b.options.prevArrow).addClass("slick-arrow"), b.$nextArrow = a(b.options.nextArrow).addClass("slick-arrow"), b.slideCount > b.options.slidesToShow ? (b.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), b.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), b.htmlExpr.test(b.options.prevArrow) && b.$prevArrow.prependTo(b.options.appendArrows), b.htmlExpr.test(b.options.nextArrow) && b.$nextArrow.appendTo(b.options.appendArrows), !0 !== b.options.infinite && b.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : b.$prevArrow.add(b.$nextArrow).addClass("slick-hidden").attr({
            "aria-disabled": "true",
            tabindex: "-1"
        }))
    }, b.prototype.buildDots = function () {
        var b, c, d = this;
        if (!0 === d.options.dots && d.slideCount > d.options.slidesToShow) {
            for (d.$slider.addClass("slick-dotted"), c = a("<ul />").addClass(d.options.dotsClass), b = 0; b <= d.getDotCount(); b += 1) c.append(a("<li />").append(d.options.customPaging.call(this, d, b)));
            d.$dots = c.appendTo(d.options.appendDots), d.$dots.find("li").first().addClass("slick-active")
        }
    }, b.prototype.buildOut = function () {
        var b = this;
        b.$slides = b.$slider.children(b.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), b.slideCount = b.$slides.length, b.$slides.each(function (b, c) {
            a(c).attr("data-slick-index", b).data("originalStyling", a(c).attr("style") || "")
        }), b.$slider.addClass("slick-slider"), b.$slideTrack = 0 === b.slideCount ? a('<div class="slick-track"/>').appendTo(b.$slider) : b.$slides.wrapAll('<div class="slick-track"/>').parent(), b.$list = b.$slideTrack.wrap('<div class="slick-list"/>').parent(), b.$slideTrack.css("opacity", 0), !0 !== b.options.centerMode && !0 !== b.options.swipeToSlide || (b.options.slidesToScroll = 1), a("img[data-lazy]", b.$slider).not("[src]").addClass("slick-loading"), b.setupInfinite(), b.buildArrows(), b.buildDots(), b.updateDots(), b.setSlideClasses("number" == typeof b.currentSlide ? b.currentSlide : 0), !0 === b.options.draggable && b.$list.addClass("draggable")
    }, b.prototype.buildRows = function () {
        var a, b, c, d, e, f, g, h = this;
        if (d = document.createDocumentFragment(), f = h.$slider.children(), h.options.rows > 0) {
            for (g = h.options.slidesPerRow * h.options.rows, e = Math.ceil(f.length / g), a = 0; a < e; a++) {
                var i = document.createElement("div");
                for (b = 0; b < h.options.rows; b++) {
                    var j = document.createElement("div");
                    for (c = 0; c < h.options.slidesPerRow; c++) {
                        var k = a * g + (b * h.options.slidesPerRow + c);
                        f.get(k) && j.appendChild(f.get(k))
                    }
                    i.appendChild(j)
                }
                d.appendChild(i)
            }
            h.$slider.empty().append(d), h.$slider.children().children().children().css({
                width: 100 / h.options.slidesPerRow + "%",
                display: "inline-block"
            })
        }
    }, b.prototype.checkResponsive = function (b, c) {
        var d, e, f, g = this,
            h = !1,
            i = g.$slider.width(),
            j = window.innerWidth || a(window).width();
        if ("window" === g.respondTo ? f = j : "slider" === g.respondTo ? f = i : "min" === g.respondTo && (f = Math.min(j, i)), g.options.responsive && g.options.responsive.length && null !== g.options.responsive) {
            e = null;
            for (d in g.breakpoints) g.breakpoints.hasOwnProperty(d) && (!1 === g.originalSettings.mobileFirst ? f < g.breakpoints[d] && (e = g.breakpoints[d]) : f > g.breakpoints[d] && (e = g.breakpoints[d]));
            null !== e ? null !== g.activeBreakpoint ? (e !== g.activeBreakpoint || c) && (g.activeBreakpoint = e, "unslick" === g.breakpointSettings[e] ? g.unslick(e) : (g.options = a.extend({}, g.originalSettings, g.breakpointSettings[e]), !0 === b && (g.currentSlide = g.options.initialSlide), g.refresh(b)), h = e) : (g.activeBreakpoint = e, "unslick" === g.breakpointSettings[e] ? g.unslick(e) : (g.options = a.extend({}, g.originalSettings, g.breakpointSettings[e]), !0 === b && (g.currentSlide = g.options.initialSlide), g.refresh(b)), h = e) : null !== g.activeBreakpoint && (g.activeBreakpoint = null, g.options = g.originalSettings, !0 === b && (g.currentSlide = g.options.initialSlide), g.refresh(b), h = e), b || !1 === h || g.$slider.trigger("breakpoint", [g, h])
        }
    }, b.prototype.changeSlide = function (b, c) {
        var d, e, f, g = this,
            h = a(b.currentTarget);
        switch (h.is("a") && b.preventDefault(), h.is("li") || (h = h.closest("li")), f = g.slideCount % g.options.slidesToScroll != 0, d = f ? 0 : (g.slideCount - g.currentSlide) % g.options.slidesToScroll, b.data.message) {
            case "previous":
                e = 0 === d ? g.options.slidesToScroll : g.options.slidesToShow - d, g.slideCount > g.options.slidesToShow && g.slideHandler(g.currentSlide - e, !1, c);
                break;
            case "next":
                e = 0 === d ? g.options.slidesToScroll : d, g.slideCount > g.options.slidesToShow && g.slideHandler(g.currentSlide + e, !1, c);
                break;
            case "index":
                var i = 0 === b.data.index ? 0 : b.data.index || h.index() * g.options.slidesToScroll;
                g.slideHandler(g.checkNavigable(i), !1, c), h.children().trigger("focus");
                break;
            default:
                return
        }
    }, b.prototype.checkNavigable = function (a) {
        var b, c, d = this;
        if (b = d.getNavigableIndexes(), c = 0, a > b[b.length - 1]) a = b[b.length - 1];
        else
            for (var e in b) {
                if (a < b[e]) {
                    a = c;
                    break
                }
                c = b[e]
            }
        return a
    }, b.prototype.cleanUpEvents = function () {
        var b = this;
        b.options.dots && null !== b.$dots && (a("li", b.$dots).off("click.slick", b.changeSlide).off("mouseenter.slick", a.proxy(b.interrupt, b, !0)).off("mouseleave.slick", a.proxy(b.interrupt, b, !1)), !0 === b.options.accessibility && b.$dots.off("keydown.slick", b.keyHandler)), b.$slider.off("focus.slick blur.slick"), !0 === b.options.arrows && b.slideCount > b.options.slidesToShow && (b.$prevArrow && b.$prevArrow.off("click.slick", b.changeSlide), b.$nextArrow && b.$nextArrow.off("click.slick", b.changeSlide), !0 === b.options.accessibility && (b.$prevArrow && b.$prevArrow.off("keydown.slick", b.keyHandler), b.$nextArrow && b.$nextArrow.off("keydown.slick", b.keyHandler))), b.$list.off("touchstart.slick mousedown.slick", b.swipeHandler), b.$list.off("touchmove.slick mousemove.slick", b.swipeHandler), b.$list.off("touchend.slick mouseup.slick", b.swipeHandler), b.$list.off("touchcancel.slick mouseleave.slick", b.swipeHandler), b.$list.off("click.slick", b.clickHandler), a(document).off(b.visibilityChange, b.visibility), b.cleanUpSlideEvents(), !0 === b.options.accessibility && b.$list.off("keydown.slick", b.keyHandler), !0 === b.options.focusOnSelect && a(b.$slideTrack).children().off("click.slick", b.selectHandler), a(window).off("orientationchange.slick.slick-" + b.instanceUid, b.orientationChange), a(window).off("resize.slick.slick-" + b.instanceUid, b.resize), a("[draggable!=true]", b.$slideTrack).off("dragstart", b.preventDefault), a(window).off("load.slick.slick-" + b.instanceUid, b.setPosition)
    }, b.prototype.cleanUpSlideEvents = function () {
        var b = this;
        b.$list.off("mouseenter.slick", a.proxy(b.interrupt, b, !0)), b.$list.off("mouseleave.slick", a.proxy(b.interrupt, b, !1))
    }, b.prototype.cleanUpRows = function () {
        var a, b = this;
        b.options.rows > 0 && (a = b.$slides.children().children(), a.removeAttr("style"), b.$slider.empty().append(a))
    }, b.prototype.clickHandler = function (a) {
        !1 === this.shouldClick && (a.stopImmediatePropagation(), a.stopPropagation(), a.preventDefault())
    }, b.prototype.destroy = function (b) {
        var c = this;
        c.autoPlayClear(), c.touchObject = {}, c.cleanUpEvents(), a(".slick-cloned", c.$slider).detach(), c.$dots && c.$dots.remove(), c.$prevArrow && c.$prevArrow.length && (c.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), c.htmlExpr.test(c.options.prevArrow) && c.$prevArrow.remove()), c.$nextArrow && c.$nextArrow.length && (c.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), c.htmlExpr.test(c.options.nextArrow) && c.$nextArrow.remove()), c.$slides && (c.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function () {
            a(this).attr("style", a(this).data("originalStyling"))
        }), c.$slideTrack.children(this.options.slide).detach(), c.$slideTrack.detach(), c.$list.detach(), c.$slider.append(c.$slides)), c.cleanUpRows(), c.$slider.removeClass("slick-slider"), c.$slider.removeClass("slick-initialized"), c.$slider.removeClass("slick-dotted"), c.unslicked = !0, b || c.$slider.trigger("destroy", [c])
    }, b.prototype.disableTransition = function (a) {
        var b = this,
            c = {};
        c[b.transitionType] = "", !1 === b.options.fade ? b.$slideTrack.css(c) : b.$slides.eq(a).css(c)
    }, b.prototype.fadeSlide = function (a, b) {
        var c = this;
        !1 === c.cssTransitions ? (c.$slides.eq(a).css({
            zIndex: c.options.zIndex
        }), c.$slides.eq(a).animate({
            opacity: 1
        }, c.options.speed, c.options.easing, b)) : (c.applyTransition(a), c.$slides.eq(a).css({
            opacity: 1,
            zIndex: c.options.zIndex
        }), b && setTimeout(function () {
            c.disableTransition(a), b.call()
        }, c.options.speed))
    }, b.prototype.fadeSlideOut = function (a) {
        var b = this;
        !1 === b.cssTransitions ? b.$slides.eq(a).animate({
            opacity: 0,
            zIndex: b.options.zIndex - 2
        }, b.options.speed, b.options.easing) : (b.applyTransition(a), b.$slides.eq(a).css({
            opacity: 0,
            zIndex: b.options.zIndex - 2
        }))
    }, b.prototype.filterSlides = b.prototype.slickFilter = function (a) {
        var b = this;
        null !== a && (b.$slidesCache = b.$slides, b.unload(), b.$slideTrack.children(this.options.slide).detach(), b.$slidesCache.filter(a).appendTo(b.$slideTrack), b.reinit())
    }, b.prototype.focusHandler = function () {
        var b = this;
        b.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*", function (c) {
            c.stopImmediatePropagation();
            var d = a(this);
            setTimeout(function () {
                b.options.pauseOnFocus && (b.focussed = d.is(":focus"), b.autoPlay())
            }, 0)
        })
    }, b.prototype.getCurrent = b.prototype.slickCurrentSlide = function () {
        return this.currentSlide
    }, b.prototype.getDotCount = function () {
        var a = this,
            b = 0,
            c = 0,
            d = 0;
        if (!0 === a.options.infinite)
            if (a.slideCount <= a.options.slidesToShow)++d;
            else
                for (; b < a.slideCount;)++d, b = c + a.options.slidesToScroll, c += a.options.slidesToScroll <= a.options.slidesToShow ? a.options.slidesToScroll : a.options.slidesToShow;
        else if (!0 === a.options.centerMode) d = a.slideCount;
        else if (a.options.asNavFor)
            for (; b < a.slideCount;)++d, b = c + a.options.slidesToScroll, c += a.options.slidesToScroll <= a.options.slidesToShow ? a.options.slidesToScroll : a.options.slidesToShow;
        else d = 1 + Math.ceil((a.slideCount - a.options.slidesToShow) / a.options.slidesToScroll);
        return d - 1
    }, b.prototype.getLeft = function (a) {
        var b, c, d, e, f = this,
            g = 0;
        return f.slideOffset = 0, c = f.$slides.first().outerHeight(!0), !0 === f.options.infinite ? (f.slideCount > f.options.slidesToShow && (f.slideOffset = f.slideWidth * f.options.slidesToShow * -1, e = -1, !0 === f.options.vertical && !0 === f.options.centerMode && (2 === f.options.slidesToShow ? e = -1.5 : 1 === f.options.slidesToShow && (e = -2)), g = c * f.options.slidesToShow * e), f.slideCount % f.options.slidesToScroll != 0 && a + f.options.slidesToScroll > f.slideCount && f.slideCount > f.options.slidesToShow && (a > f.slideCount ? (f.slideOffset = (f.options.slidesToShow - (a - f.slideCount)) * f.slideWidth * -1, g = (f.options.slidesToShow - (a - f.slideCount)) * c * -1) : (f.slideOffset = f.slideCount % f.options.slidesToScroll * f.slideWidth * -1, g = f.slideCount % f.options.slidesToScroll * c * -1))) : a + f.options.slidesToShow > f.slideCount && (f.slideOffset = (a + f.options.slidesToShow - f.slideCount) * f.slideWidth, g = (a + f.options.slidesToShow - f.slideCount) * c), f.slideCount <= f.options.slidesToShow && (f.slideOffset = 0, g = 0), !0 === f.options.centerMode && f.slideCount <= f.options.slidesToShow ? f.slideOffset = f.slideWidth * Math.floor(f.options.slidesToShow) / 2 - f.slideWidth * f.slideCount / 2 : !0 === f.options.centerMode && !0 === f.options.infinite ? f.slideOffset += f.slideWidth * Math.floor(f.options.slidesToShow / 2) - f.slideWidth : !0 === f.options.centerMode && (f.slideOffset = 0, f.slideOffset += f.slideWidth * Math.floor(f.options.slidesToShow / 2)), b = !1 === f.options.vertical ? a * f.slideWidth * -1 + f.slideOffset : a * c * -1 + g, !0 === f.options.variableWidth && (d = f.slideCount <= f.options.slidesToShow || !1 === f.options.infinite ? f.$slideTrack.children(".slick-slide").eq(a) : f.$slideTrack.children(".slick-slide").eq(a + f.options.slidesToShow), b = !0 === f.options.rtl ? d[0] ? -1 * (f.$slideTrack.width() - d[0].offsetLeft - d.width()) : 0 : d[0] ? -1 * d[0].offsetLeft : 0, !0 === f.options.centerMode && (d = f.slideCount <= f.options.slidesToShow || !1 === f.options.infinite ? f.$slideTrack.children(".slick-slide").eq(a) : f.$slideTrack.children(".slick-slide").eq(a + f.options.slidesToShow + 1), b = !0 === f.options.rtl ? d[0] ? -1 * (f.$slideTrack.width() - d[0].offsetLeft - d.width()) : 0 : d[0] ? -1 * d[0].offsetLeft : 0, b += (f.$list.width() - d.outerWidth()) / 2)), b
    }, b.prototype.getOption = b.prototype.slickGetOption = function (a) {
        return this.options[a]
    }, b.prototype.getNavigableIndexes = function () {
        var a, b = this,
            c = 0,
            d = 0,
            e = [];
        for (!1 === b.options.infinite ? a = b.slideCount : (c = -1 * b.options.slidesToScroll, d = -1 * b.options.slidesToScroll, a = 2 * b.slideCount); c < a;) e.push(c), c = d + b.options.slidesToScroll, d += b.options.slidesToScroll <= b.options.slidesToShow ? b.options.slidesToScroll : b.options.slidesToShow;
        return e
    }, b.prototype.getSlick = function () {
        return this
    }, b.prototype.getSlideCount = function () {
        var b, c, d = this;
        return c = !0 === d.options.centerMode ? d.slideWidth * Math.floor(d.options.slidesToShow / 2) : 0, !0 === d.options.swipeToSlide ? (d.$slideTrack.find(".slick-slide").each(function (e, f) {
            if (f.offsetLeft - c + a(f).outerWidth() / 2 > -1 * d.swipeLeft) return b = f, !1
        }), Math.abs(a(b).attr("data-slick-index") - d.currentSlide) || 1) : d.options.slidesToScroll
    }, b.prototype.goTo = b.prototype.slickGoTo = function (a, b) {
        this.changeSlide({
            data: {
                message: "index",
                index: parseInt(a)
            }
        }, b)
    }, b.prototype.init = function (b) {
        var c = this;
        a(c.$slider).hasClass("slick-initialized") || (a(c.$slider).addClass("slick-initialized"), c.buildRows(), c.buildOut(), c.setProps(), c.startLoad(), c.loadSlider(), c.initializeEvents(), c.updateArrows(), c.updateDots(), c.checkResponsive(!0), c.focusHandler()), b && c.$slider.trigger("init", [c]), !0 === c.options.accessibility && c.initADA(), c.options.autoplay && (c.paused = !1, c.autoPlay())
    }, b.prototype.initADA = function () {
        var b = this,
            c = Math.ceil(b.slideCount / b.options.slidesToShow),
            d = b.getNavigableIndexes().filter(function (a) {
                return a >= 0 && a < b.slideCount
            });
        b.$slides.add(b.$slideTrack.find(".slick-cloned")).attr({
            "aria-hidden": "true",
            tabindex: "-1"
        }).find("a, input, button, select").attr({
            tabindex: "-1"
        }), null !== b.$dots && (b.$slides.not(b.$slideTrack.find(".slick-cloned")).each(function (c) {
            var e = d.indexOf(c);
            if (a(this).attr({
                role: "tabpanel",
                id: "slick-slide" + b.instanceUid + c,
                tabindex: -1
            }), -1 !== e) {
                var f = "slick-slide-control" + b.instanceUid + e;
                a("#" + f).length && a(this).attr({
                    "aria-describedby": f
                })
            }
        }), b.$dots.attr("role", "tablist").find("li").each(function (e) {
            var f = d[e];
            a(this).attr({
                role: "presentation"
            }), a(this).find("button").first().attr({
                role: "tab",
                id: "slick-slide-control" + b.instanceUid + e,
                "aria-controls": "slick-slide" + b.instanceUid + f,
                "aria-label": e + 1 + " of " + c,
                "aria-selected": null,
                tabindex: "-1"
            })
        }).eq(b.currentSlide).find("button").attr({
            "aria-selected": "true",
            tabindex: "0"
        }).end());
        for (var e = b.currentSlide, f = e + b.options.slidesToShow; e < f; e++) b.options.focusOnChange ? b.$slides.eq(e).attr({
            tabindex: "0"
        }) : b.$slides.eq(e).removeAttr("tabindex");
        b.activateADA()
    }, b.prototype.initArrowEvents = function () {
        var a = this;
        !0 === a.options.arrows && a.slideCount > a.options.slidesToShow && (a.$prevArrow.off("click.slick").on("click.slick", {
            message: "previous"
        }, a.changeSlide), a.$nextArrow.off("click.slick").on("click.slick", {
            message: "next"
        }, a.changeSlide), !0 === a.options.accessibility && (a.$prevArrow.on("keydown.slick", a.keyHandler), a.$nextArrow.on("keydown.slick", a.keyHandler)))
    }, b.prototype.initDotEvents = function () {
        var b = this;
        !0 === b.options.dots && b.slideCount > b.options.slidesToShow && (a("li", b.$dots).on("click.slick", {
            message: "index"
        }, b.changeSlide), !0 === b.options.accessibility && b.$dots.on("keydown.slick", b.keyHandler)), !0 === b.options.dots && !0 === b.options.pauseOnDotsHover && b.slideCount > b.options.slidesToShow && a("li", b.$dots).on("mouseenter.slick", a.proxy(b.interrupt, b, !0)).on("mouseleave.slick", a.proxy(b.interrupt, b, !1))
    }, b.prototype.initSlideEvents = function () {
        var b = this;
        b.options.pauseOnHover && (b.$list.on("mouseenter.slick", a.proxy(b.interrupt, b, !0)), b.$list.on("mouseleave.slick", a.proxy(b.interrupt, b, !1)))
    }, b.prototype.initializeEvents = function () {
        var b = this;
        b.initArrowEvents(), b.initDotEvents(), b.initSlideEvents(), b.$list.on("touchstart.slick mousedown.slick", {
            action: "start"
        }, b.swipeHandler), b.$list.on("touchmove.slick mousemove.slick", {
            action: "move"
        }, b.swipeHandler), b.$list.on("touchend.slick mouseup.slick", {
            action: "end"
        }, b.swipeHandler), b.$list.on("touchcancel.slick mouseleave.slick", {
            action: "end"
        }, b.swipeHandler), b.$list.on("click.slick", b.clickHandler), a(document).on(b.visibilityChange, a.proxy(b.visibility, b)), !0 === b.options.accessibility && b.$list.on("keydown.slick", b.keyHandler), !0 === b.options.focusOnSelect && a(b.$slideTrack).children().on("click.slick", b.selectHandler), a(window).on("orientationchange.slick.slick-" + b.instanceUid, a.proxy(b.orientationChange, b)), a(window).on("resize.slick.slick-" + b.instanceUid, a.proxy(b.resize, b)), a("[draggable!=true]", b.$slideTrack).on("dragstart", b.preventDefault), a(window).on("load.slick.slick-" + b.instanceUid, b.setPosition), a(b.setPosition)
    }, b.prototype.initUI = function () {
        var a = this;
        !0 === a.options.arrows && a.slideCount > a.options.slidesToShow && (a.$prevArrow.show(), a.$nextArrow.show()), !0 === a.options.dots && a.slideCount > a.options.slidesToShow && a.$dots.show()
    }, b.prototype.keyHandler = function (a) {
        var b = this;
        a.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === a.keyCode && !0 === b.options.accessibility ? b.changeSlide({
            data: {
                message: !0 === b.options.rtl ? "next" : "previous"
            }
        }) : 39 === a.keyCode && !0 === b.options.accessibility && b.changeSlide({
            data: {
                message: !0 === b.options.rtl ? "previous" : "next"
            }
        }))
    }, b.prototype.lazyLoad = function () {
        function b(b) {
            a("img[data-lazy]", b).each(function () {
                var b = a(this),
                    c = a(this).attr("data-lazy"),
                    d = a(this).attr("data-srcset"),
                    e = a(this).attr("data-sizes") || g.$slider.attr("data-sizes"),
                    f = document.createElement("img");
                f.onload = function () {
                    b.animate({
                        opacity: 0
                    }, 100, function () {
                        d && (b.attr("srcset", d), e && b.attr("sizes", e)), b.attr("src", c).animate({
                            opacity: 1
                        }, 200, function () {
                            b.removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading")
                        }), g.$slider.trigger("lazyLoaded", [g, b, c])
                    })
                }, f.onerror = function () {
                    b.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), g.$slider.trigger("lazyLoadError", [g, b, c])
                }, f.src = c
            })
        }
        var c, d, e, f, g = this;
        if (!0 === g.options.centerMode ? !0 === g.options.infinite ? (e = g.currentSlide + (g.options.slidesToShow / 2 + 1), f = e + g.options.slidesToShow + 2) : (e = Math.max(0, g.currentSlide - (g.options.slidesToShow / 2 + 1)), f = g.options.slidesToShow / 2 + 1 + 2 + g.currentSlide) : (e = g.options.infinite ? g.options.slidesToShow + g.currentSlide : g.currentSlide, f = Math.ceil(e + g.options.slidesToShow), !0 === g.options.fade && (e > 0 && e--, f <= g.slideCount && f++)), c = g.$slider.find(".slick-slide").slice(e, f), "anticipated" === g.options.lazyLoad)
            for (var h = e - 1, i = f, j = g.$slider.find(".slick-slide"), k = 0; k < g.options.slidesToScroll; k++) h < 0 && (h = g.slideCount - 1), c = c.add(j.eq(h)), c = c.add(j.eq(i)), h--, i++;
        b(c), g.slideCount <= g.options.slidesToShow ? (d = g.$slider.find(".slick-slide"), b(d)) : g.currentSlide >= g.slideCount - g.options.slidesToShow ? (d = g.$slider.find(".slick-cloned").slice(0, g.options.slidesToShow), b(d)) : 0 === g.currentSlide && (d = g.$slider.find(".slick-cloned").slice(-1 * g.options.slidesToShow), b(d))
    }, b.prototype.loadSlider = function () {
        var a = this;
        a.setPosition(), a.$slideTrack.css({
            opacity: 1
        }), a.$slider.removeClass("slick-loading"), a.initUI(), "progressive" === a.options.lazyLoad && a.progressiveLazyLoad()
    }, b.prototype.next = b.prototype.slickNext = function () {
        this.changeSlide({
            data: {
                message: "next"
            }
        })
    }, b.prototype.orientationChange = function () {
        var a = this;
        a.checkResponsive(), a.setPosition()
    }, b.prototype.pause = b.prototype.slickPause = function () {
        var a = this;
        a.autoPlayClear(), a.paused = !0
    }, b.prototype.play = b.prototype.slickPlay = function () {
        var a = this;
        a.autoPlay(), a.options.autoplay = !0, a.paused = !1, a.focussed = !1, a.interrupted = !1
    }, b.prototype.postSlide = function (b) {
        var c = this;
        if (!c.unslicked && (c.$slider.trigger("afterChange", [c, b]), c.animating = !1, c.slideCount > c.options.slidesToShow && c.setPosition(), c.swipeLeft = null, c.options.autoplay && c.autoPlay(), !0 === c.options.accessibility && (c.initADA(), c.options.focusOnChange))) {
            a(c.$slides.get(c.currentSlide)).attr("tabindex", 0).focus()
        }
    }, b.prototype.prev = b.prototype.slickPrev = function () {
        this.changeSlide({
            data: {
                message: "previous"
            }
        })
    }, b.prototype.preventDefault = function (a) {
        a.preventDefault()
    }, b.prototype.progressiveLazyLoad = function (b) {
        b = b || 1;
        var c, d, e, f, g, h = this,
            i = a("img[data-lazy]", h.$slider);
        i.length ? (c = i.first(), d = c.attr("data-lazy"), e = c.attr("data-srcset"), f = c.attr("data-sizes") || h.$slider.attr("data-sizes"), g = document.createElement("img"), g.onload = function () {
            e && (c.attr("srcset", e), f && c.attr("sizes", f)), c.attr("src", d).removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading"), !0 === h.options.adaptiveHeight && h.setPosition(), h.$slider.trigger("lazyLoaded", [h, c, d]), h.progressiveLazyLoad()
        }, g.onerror = function () {
            b < 3 ? setTimeout(function () {
                h.progressiveLazyLoad(b + 1)
            }, 500) : (c.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), h.$slider.trigger("lazyLoadError", [h, c, d]), h.progressiveLazyLoad())
        }, g.src = d) : h.$slider.trigger("allImagesLoaded", [h])
    }, b.prototype.refresh = function (b) {
        var c, d, e = this;
        d = e.slideCount - e.options.slidesToShow, !e.options.infinite && e.currentSlide > d && (e.currentSlide = d), e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0), c = e.currentSlide, e.destroy(!0), a.extend(e, e.initials, {
            currentSlide: c
        }), e.init(), b || e.changeSlide({
            data: {
                message: "index",
                index: c
            }
        }, !1)
    }, b.prototype.registerBreakpoints = function () {
        var b, c, d, e = this,
            f = e.options.responsive || null;
        if ("array" === a.type(f) && f.length) {
            e.respondTo = e.options.respondTo || "window";
            for (b in f)
                if (d = e.breakpoints.length - 1, f.hasOwnProperty(b)) {
                    for (c = f[b].breakpoint; d >= 0;) e.breakpoints[d] && e.breakpoints[d] === c && e.breakpoints.splice(d, 1), d--;
                    e.breakpoints.push(c), e.breakpointSettings[c] = f[b].settings
                }
            e.breakpoints.sort(function (a, b) {
                return e.options.mobileFirst ? a - b : b - a
            })
        }
    }, b.prototype.reinit = function () {
        var b = this;
        b.$slides = b.$slideTrack.children(b.options.slide).addClass("slick-slide"), b.slideCount = b.$slides.length, b.currentSlide >= b.slideCount && 0 !== b.currentSlide && (b.currentSlide = b.currentSlide - b.options.slidesToScroll), b.slideCount <= b.options.slidesToShow && (b.currentSlide = 0), b.registerBreakpoints(), b.setProps(), b.setupInfinite(), b.buildArrows(), b.updateArrows(), b.initArrowEvents(), b.buildDots(), b.updateDots(), b.initDotEvents(), b.cleanUpSlideEvents(), b.initSlideEvents(), b.checkResponsive(!1, !0), !0 === b.options.focusOnSelect && a(b.$slideTrack).children().on("click.slick", b.selectHandler), b.setSlideClasses("number" == typeof b.currentSlide ? b.currentSlide : 0), b.setPosition(), b.focusHandler(), b.paused = !b.options.autoplay, b.autoPlay(), b.$slider.trigger("reInit", [b])
    }, b.prototype.resize = function () {
        var b = this;
        a(window).width() !== b.windowWidth && (clearTimeout(b.windowDelay), b.windowDelay = window.setTimeout(function () {
            b.windowWidth = a(window).width(), b.checkResponsive(), b.unslicked || b.setPosition()
        }, 50))
    }, b.prototype.removeSlide = b.prototype.slickRemove = function (a, b, c) {
        var d = this;
        if ("boolean" == typeof a ? (b = a, a = !0 === b ? 0 : d.slideCount - 1) : a = !0 === b ? --a : a, d.slideCount < 1 || a < 0 || a > d.slideCount - 1) return !1;
        d.unload(), !0 === c ? d.$slideTrack.children().remove() : d.$slideTrack.children(this.options.slide).eq(a).remove(), d.$slides = d.$slideTrack.children(this.options.slide), d.$slideTrack.children(this.options.slide).detach(), d.$slideTrack.append(d.$slides), d.$slidesCache = d.$slides, d.reinit()
    }, b.prototype.setCSS = function (a) {
        var b, c, d = this,
            e = {};
        !0 === d.options.rtl && (a = -a), b = "left" == d.positionProp ? Math.ceil(a) + "px" : "0px", c = "top" == d.positionProp ? Math.ceil(a) + "px" : "0px", e[d.positionProp] = a, !1 === d.transformsEnabled ? d.$slideTrack.css(e) : (e = {}, !1 === d.cssTransitions ? (e[d.animType] = "translate(" + b + ", " + c + ")", d.$slideTrack.css(e)) : (e[d.animType] = "translate3d(" + b + ", " + c + ", 0px)", d.$slideTrack.css(e)))
    }, b.prototype.setDimensions = function () {
        var a = this;
        !1 === a.options.vertical ? !0 === a.options.centerMode && a.$list.css({
            padding: "0px " + a.options.centerPadding
        }) : (a.$list.height(a.$slides.first().outerHeight(!0) * a.options.slidesToShow), !0 === a.options.centerMode && a.$list.css({
            padding: a.options.centerPadding + " 0px"
        })), a.listWidth = a.$list.width(), a.listHeight = a.$list.height(), !1 === a.options.vertical && !1 === a.options.variableWidth ? (a.slideWidth = Math.ceil(a.listWidth / a.options.slidesToShow), a.$slideTrack.width(Math.ceil(a.slideWidth * a.$slideTrack.children(".slick-slide").length))) : !0 === a.options.variableWidth ? a.$slideTrack.width(5e3 * a.slideCount) : (a.slideWidth = Math.ceil(a.listWidth), a.$slideTrack.height(Math.ceil(a.$slides.first().outerHeight(!0) * a.$slideTrack.children(".slick-slide").length)));
        var b = a.$slides.first().outerWidth(!0) - a.$slides.first().width();
        !1 === a.options.variableWidth && a.$slideTrack.children(".slick-slide").width(a.slideWidth - b)
    }, b.prototype.setFade = function () {
        var b, c = this;
        c.$slides.each(function (d, e) {
            b = c.slideWidth * d * -1, !0 === c.options.rtl ? a(e).css({
                position: "relative",
                right: b,
                top: 0,
                zIndex: c.options.zIndex - 2,
                opacity: 0
            }) : a(e).css({
                position: "relative",
                left: b,
                top: 0,
                zIndex: c.options.zIndex - 2,
                opacity: 0
            })
        }), c.$slides.eq(c.currentSlide).css({
            zIndex: c.options.zIndex - 1,
            opacity: 1
        })
    },
        b.prototype.setHeight = function () {
            var a = this;
            if (1 === a.options.slidesToShow && !0 === a.options.adaptiveHeight && !1 === a.options.vertical) {
                var b = a.$slides.eq(a.currentSlide).outerHeight(!0);
                a.$list.css("height", b)
            }
        }, b.prototype.setOption = b.prototype.slickSetOption = function () {
        var b, c, d, e, f, g = this,
            h = !1;
        if ("object" === a.type(arguments[0]) ? (d = arguments[0], h = arguments[1], f = "multiple") : "string" === a.type(arguments[0]) && (d = arguments[0], e = arguments[1], h = arguments[2], "responsive" === arguments[0] && "array" === a.type(arguments[1]) ? f = "responsive" : void 0 !== arguments[1] && (f = "single")), "single" === f) g.options[d] = e;
        else if ("multiple" === f) a.each(d, function (a, b) {
            g.options[a] = b
        });
        else if ("responsive" === f)
            for (c in e)
                if ("array" !== a.type(g.options.responsive)) g.options.responsive = [e[c]];
                else {
                    for (b = g.options.responsive.length - 1; b >= 0;) g.options.responsive[b].breakpoint === e[c].breakpoint && g.options.responsive.splice(b, 1), b--;
                    g.options.responsive.push(e[c])
                }
        h && (g.unload(), g.reinit())
    }, b.prototype.setPosition = function () {
        var a = this;
        a.setDimensions(), a.setHeight(), !1 === a.options.fade ? a.setCSS(a.getLeft(a.currentSlide)) : a.setFade(), a.$slider.trigger("setPosition", [a])
    }, b.prototype.setProps = function () {
        var a = this,
            b = document.body.style;
        a.positionProp = !0 === a.options.vertical ? "top" : "left", "top" === a.positionProp ? a.$slider.addClass("slick-vertical") : a.$slider.removeClass("slick-vertical"), void 0 === b.WebkitTransition && void 0 === b.MozTransition && void 0 === b.msTransition || !0 === a.options.useCSS && (a.cssTransitions = !0), a.options.fade && ("number" == typeof a.options.zIndex ? a.options.zIndex < 3 && (a.options.zIndex = 3) : a.options.zIndex = a.defaults.zIndex), void 0 !== b.OTransform && (a.animType = "OTransform", a.transformType = "-o-transform", a.transitionType = "OTransition", void 0 === b.perspectiveProperty && void 0 === b.webkitPerspective && (a.animType = !1)), void 0 !== b.MozTransform && (a.animType = "MozTransform", a.transformType = "-moz-transform", a.transitionType = "MozTransition", void 0 === b.perspectiveProperty && void 0 === b.MozPerspective && (a.animType = !1)), void 0 !== b.webkitTransform && (a.animType = "webkitTransform", a.transformType = "-webkit-transform", a.transitionType = "webkitTransition", void 0 === b.perspectiveProperty && void 0 === b.webkitPerspective && (a.animType = !1)), void 0 !== b.msTransform && (a.animType = "msTransform", a.transformType = "-ms-transform", a.transitionType = "msTransition", void 0 === b.msTransform && (a.animType = !1)), void 0 !== b.transform && !1 !== a.animType && (a.animType = "transform", a.transformType = "transform", a.transitionType = "transition"), a.transformsEnabled = a.options.useTransform && null !== a.animType && !1 !== a.animType
    }, b.prototype.setSlideClasses = function (a) {
        var b, c, d, e, f = this;
        if (c = f.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), f.$slides.eq(a).addClass("slick-current"), !0 === f.options.centerMode) {
            var g = f.options.slidesToShow % 2 == 0 ? 1 : 0;
            b = Math.floor(f.options.slidesToShow / 2), !0 === f.options.infinite && (a >= b && a <= f.slideCount - 1 - b ? f.$slides.slice(a - b + g, a + b + 1).addClass("slick-active").attr("aria-hidden", "false") : (d = f.options.slidesToShow + a, c.slice(d - b + 1 + g, d + b + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === a ? c.eq(c.length - 1 - f.options.slidesToShow).addClass("slick-center") : a === f.slideCount - 1 && c.eq(f.options.slidesToShow).addClass("slick-center")), f.$slides.eq(a).addClass("slick-center")
        } else a >= 0 && a <= f.slideCount - f.options.slidesToShow ? f.$slides.slice(a, a + f.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : c.length <= f.options.slidesToShow ? c.addClass("slick-active").attr("aria-hidden", "false") : (e = f.slideCount % f.options.slidesToShow, d = !0 === f.options.infinite ? f.options.slidesToShow + a : a, f.options.slidesToShow == f.options.slidesToScroll && f.slideCount - a < f.options.slidesToShow ? c.slice(d - (f.options.slidesToShow - e), d + e).addClass("slick-active").attr("aria-hidden", "false") : c.slice(d, d + f.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false"));
        "ondemand" !== f.options.lazyLoad && "anticipated" !== f.options.lazyLoad || f.lazyLoad()
    }, b.prototype.setupInfinite = function () {
        var b, c, d, e = this;
        if (!0 === e.options.fade && (e.options.centerMode = !1), !0 === e.options.infinite && !1 === e.options.fade && (c = null, e.slideCount > e.options.slidesToShow)) {
            for (d = !0 === e.options.centerMode ? e.options.slidesToShow + 1 : e.options.slidesToShow, b = e.slideCount; b > e.slideCount - d; b -= 1) c = b - 1, a(e.$slides[c]).clone(!0).attr("id", "").attr("data-slick-index", c - e.slideCount).prependTo(e.$slideTrack).addClass("slick-cloned");
            for (b = 0; b < d + e.slideCount; b += 1) c = b, a(e.$slides[c]).clone(!0).attr("id", "").attr("data-slick-index", c + e.slideCount).appendTo(e.$slideTrack).addClass("slick-cloned");
            e.$slideTrack.find(".slick-cloned").find("[id]").each(function () {
                a(this).attr("id", "")
            })
        }
    }, b.prototype.interrupt = function (a) {
        var b = this;
        a || b.autoPlay(), b.interrupted = a
    }, b.prototype.selectHandler = function (b) {
        var c = this,
            d = a(b.target).is(".slick-slide") ? a(b.target) : a(b.target).parents(".slick-slide"),
            e = parseInt(d.attr("data-slick-index"));
        if (e || (e = 0), c.slideCount <= c.options.slidesToShow) return void c.slideHandler(e, !1, !0);
        c.slideHandler(e)
    }, b.prototype.slideHandler = function (a, b, c) {
        var d, e, f, g, h, i = null,
            j = this;
        if (b = b || !1, !(!0 === j.animating && !0 === j.options.waitForAnimate || !0 === j.options.fade && j.currentSlide === a)) {
            if (!1 === b && j.asNavFor(a), d = a, i = j.getLeft(d), g = j.getLeft(j.currentSlide), j.currentLeft = null === j.swipeLeft ? g : j.swipeLeft, !1 === j.options.infinite && !1 === j.options.centerMode && (a < 0 || a > j.getDotCount() * j.options.slidesToScroll)) return void(!1 === j.options.fade && (d = j.currentSlide, !0 !== c && j.slideCount > j.options.slidesToShow ? j.animateSlide(g, function () {
                j.postSlide(d)
            }) : j.postSlide(d)));
            if (!1 === j.options.infinite && !0 === j.options.centerMode && (a < 0 || a > j.slideCount - j.options.slidesToScroll)) return void(!1 === j.options.fade && (d = j.currentSlide, !0 !== c && j.slideCount > j.options.slidesToShow ? j.animateSlide(g, function () {
                j.postSlide(d)
            }) : j.postSlide(d)));
            if (j.options.autoplay && clearInterval(j.autoPlayTimer), e = d < 0 ? j.slideCount % j.options.slidesToScroll != 0 ? j.slideCount - j.slideCount % j.options.slidesToScroll : j.slideCount + d : d >= j.slideCount ? j.slideCount % j.options.slidesToScroll != 0 ? 0 : d - j.slideCount : d, j.animating = !0, j.$slider.trigger("beforeChange", [j, j.currentSlide, e]), f = j.currentSlide, j.currentSlide = e, j.setSlideClasses(j.currentSlide), j.options.asNavFor && (h = j.getNavTarget(), h = h.slick("getSlick"), h.slideCount <= h.options.slidesToShow && h.setSlideClasses(j.currentSlide)), j.updateDots(), j.updateArrows(), !0 === j.options.fade) return !0 !== c ? (j.fadeSlideOut(f), j.fadeSlide(e, function () {
                j.postSlide(e)
            })) : j.postSlide(e), void j.animateHeight();
            !0 !== c && j.slideCount > j.options.slidesToShow ? j.animateSlide(i, function () {
                j.postSlide(e)
            }) : j.postSlide(e)
        }
    }, b.prototype.startLoad = function () {
        var a = this;
        !0 === a.options.arrows && a.slideCount > a.options.slidesToShow && (a.$prevArrow.hide(), a.$nextArrow.hide()), !0 === a.options.dots && a.slideCount > a.options.slidesToShow && a.$dots.hide(), a.$slider.addClass("slick-loading")
    }, b.prototype.swipeDirection = function () {
        var a, b, c, d, e = this;
        return a = e.touchObject.startX - e.touchObject.curX, b = e.touchObject.startY - e.touchObject.curY, c = Math.atan2(b, a), d = Math.round(180 * c / Math.PI), d < 0 && (d = 360 - Math.abs(d)), d <= 45 && d >= 0 ? !1 === e.options.rtl ? "left" : "right" : d <= 360 && d >= 315 ? !1 === e.options.rtl ? "left" : "right" : d >= 135 && d <= 225 ? !1 === e.options.rtl ? "right" : "left" : !0 === e.options.verticalSwiping ? d >= 35 && d <= 135 ? "down" : "up" : "vertical"
    }, b.prototype.swipeEnd = function (a) {
        var b, c, d = this;
        if (d.dragging = !1, d.swiping = !1, d.scrolling) return d.scrolling = !1, !1;
        if (d.interrupted = !1, d.shouldClick = !(d.touchObject.swipeLength > 10), void 0 === d.touchObject.curX) return !1;
        if (!0 === d.touchObject.edgeHit && d.$slider.trigger("edge", [d, d.swipeDirection()]), d.touchObject.swipeLength >= d.touchObject.minSwipe) {
            switch (c = d.swipeDirection()) {
                case "left":
                case "down":
                    b = d.options.swipeToSlide ? d.checkNavigable(d.currentSlide + d.getSlideCount()) : d.currentSlide + d.getSlideCount(), d.currentDirection = 0;
                    break;
                case "right":
                case "up":
                    b = d.options.swipeToSlide ? d.checkNavigable(d.currentSlide - d.getSlideCount()) : d.currentSlide - d.getSlideCount(), d.currentDirection = 1
            }
            "vertical" != c && (d.slideHandler(b), d.touchObject = {}, d.$slider.trigger("swipe", [d, c]))
        } else d.touchObject.startX !== d.touchObject.curX && (d.slideHandler(d.currentSlide), d.touchObject = {})
    }, b.prototype.swipeHandler = function (a) {
        var b = this;
        if (!(!1 === b.options.swipe || "ontouchend" in document && !1 === b.options.swipe || !1 === b.options.draggable && -1 !== a.type.indexOf("mouse"))) switch (b.touchObject.fingerCount = a.originalEvent && void 0 !== a.originalEvent.touches ? a.originalEvent.touches.length : 1, b.touchObject.minSwipe = b.listWidth / b.options.touchThreshold, !0 === b.options.verticalSwiping && (b.touchObject.minSwipe = b.listHeight / b.options.touchThreshold), a.data.action) {
            case "start":
                b.swipeStart(a);
                break;
            case "move":
                b.swipeMove(a);
                break;
            case "end":
                b.swipeEnd(a)
        }
    }, b.prototype.swipeMove = function (a) {
        var b, c, d, e, f, g, h = this;
        return f = void 0 !== a.originalEvent ? a.originalEvent.touches : null, !(!h.dragging || h.scrolling || f && 1 !== f.length) && (b = h.getLeft(h.currentSlide), h.touchObject.curX = void 0 !== f ? f[0].pageX : a.clientX, h.touchObject.curY = void 0 !== f ? f[0].pageY : a.clientY, h.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(h.touchObject.curX - h.touchObject.startX, 2))), g = Math.round(Math.sqrt(Math.pow(h.touchObject.curY - h.touchObject.startY, 2))), !h.options.verticalSwiping && !h.swiping && g > 4 ? (h.scrolling = !0, !1) : (!0 === h.options.verticalSwiping && (h.touchObject.swipeLength = g), c = h.swipeDirection(), void 0 !== a.originalEvent && h.touchObject.swipeLength > 4 && (h.swiping = !0, a.preventDefault()), e = (!1 === h.options.rtl ? 1 : -1) * (h.touchObject.curX > h.touchObject.startX ? 1 : -1), !0 === h.options.verticalSwiping && (e = h.touchObject.curY > h.touchObject.startY ? 1 : -1), d = h.touchObject.swipeLength, h.touchObject.edgeHit = !1, !1 === h.options.infinite && (0 === h.currentSlide && "right" === c || h.currentSlide >= h.getDotCount() && "left" === c) && (d = h.touchObject.swipeLength * h.options.edgeFriction, h.touchObject.edgeHit = !0), !1 === h.options.vertical ? h.swipeLeft = b + d * e : h.swipeLeft = b + d * (h.$list.height() / h.listWidth) * e, !0 === h.options.verticalSwiping && (h.swipeLeft = b + d * e), !0 !== h.options.fade && !1 !== h.options.touchMove && (!0 === h.animating ? (h.swipeLeft = null, !1) : void h.setCSS(h.swipeLeft))))
    }, b.prototype.swipeStart = function (a) {
        var b, c = this;
        if (c.interrupted = !0, 1 !== c.touchObject.fingerCount || c.slideCount <= c.options.slidesToShow) return c.touchObject = {}, !1;
        void 0 !== a.originalEvent && void 0 !== a.originalEvent.touches && (b = a.originalEvent.touches[0]), c.touchObject.startX = c.touchObject.curX = void 0 !== b ? b.pageX : a.clientX, c.touchObject.startY = c.touchObject.curY = void 0 !== b ? b.pageY : a.clientY, c.dragging = !0
    }, b.prototype.unfilterSlides = b.prototype.slickUnfilter = function () {
        var a = this;
        null !== a.$slidesCache && (a.unload(), a.$slideTrack.children(this.options.slide).detach(), a.$slidesCache.appendTo(a.$slideTrack), a.reinit())
    }, b.prototype.unload = function () {
        var b = this;
        a(".slick-cloned", b.$slider).remove(), b.$dots && b.$dots.remove(), b.$prevArrow && b.htmlExpr.test(b.options.prevArrow) && b.$prevArrow.remove(), b.$nextArrow && b.htmlExpr.test(b.options.nextArrow) && b.$nextArrow.remove(), b.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
    }, b.prototype.unslick = function (a) {
        var b = this;
        b.$slider.trigger("unslick", [b, a]), b.destroy()
    }, b.prototype.updateArrows = function () {
        var a = this;
        Math.floor(a.options.slidesToShow / 2), !0 === a.options.arrows && a.slideCount > a.options.slidesToShow && !a.options.infinite && (a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), a.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === a.currentSlide ? (a.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), a.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : a.currentSlide >= a.slideCount - a.options.slidesToShow && !1 === a.options.centerMode ? (a.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : a.currentSlide >= a.slideCount - 1 && !0 === a.options.centerMode && (a.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), a.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
    }, b.prototype.updateDots = function () {
        var a = this;
        null !== a.$dots && (a.$dots.find("li").removeClass("slick-active").end(), a.$dots.find("li").eq(Math.floor(a.currentSlide / a.options.slidesToScroll)).addClass("slick-active"))
    }, b.prototype.visibility = function () {
        var a = this;
        a.options.autoplay && (document[a.hidden] ? a.interrupted = !0 : a.interrupted = !1)
    }, a.fn.slick = function () {
        var a, c, d = this,
            e = arguments[0],
            f = Array.prototype.slice.call(arguments, 1),
            g = d.length;
        for (a = 0; a < g; a++)
            if ("object" == typeof e || void 0 === e ? d[a].slick = new b(d[a], e) : c = d[a].slick[e].apply(d[a].slick, f), void 0 !== c) return c;
        return d
    }
}),
    function (a) {
        "function" == typeof define && define.amd ? define(["jquery"], a) : a("object" == typeof exports ? require("jquery") : window.jQuery || window.Zepto)
    }(function (a) {
        var b, c, d, e, f, g, h = "Close",
            i = "BeforeClose",
            j = "AfterClose",
            k = "BeforeAppend",
            l = "MarkupParse",
            m = "Open",
            n = "Change",
            o = "mfp",
            p = "." + o,
            q = "mfp-ready",
            r = "mfp-removing",
            s = "mfp-prevent-close",
            t = function () {},
            u = !!window.jQuery,
            v = a(window),
            w = function (a, c) {
                b.ev.on(o + a + p, c)
            },
            x = function (b, c, d, e) {
                var f = document.createElement("div");
                return f.className = "mfp-" + b, d && (f.innerHTML = d), e ? c && c.appendChild(f) : (f = a(f), c && f.appendTo(c)), f
            },
            y = function (c, d) {
                b.ev.triggerHandler(o + c, d), b.st.callbacks && (c = c.charAt(0).toLowerCase() + c.slice(1), b.st.callbacks[c] && b.st.callbacks[c].apply(b, a.isArray(d) ? d : [d]))
            },
            z = function (c) {
                return c === g && b.currTemplate.closeBtn || (b.currTemplate.closeBtn = a(b.st.closeMarkup.replace("%title%", b.st.tClose)), g = c), b.currTemplate.closeBtn
            },
            A = function () {
                a.magnificPopup.instance || (b = new t, b.init(), a.magnificPopup.instance = b)
            },
            B = function () {
                var a = document.createElement("p").style,
                    b = ["ms", "O", "Moz", "Webkit"];
                if (void 0 !== a.transition) return !0;
                for (; b.length;)
                    if (b.pop() + "Transition" in a) return !0;
                return !1
            };
        t.prototype = {
            constructor: t,
            init: function () {
                var c = navigator.appVersion;
                b.isLowIE = b.isIE8 = document.all && !document.addEventListener, b.isAndroid = /android/gi.test(c), b.isIOS = /iphone|ipad|ipod/gi.test(c), b.supportsTransition = B(), b.probablyMobile = b.isAndroid || b.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent), d = a(document), b.popupsCache = {}
            },
            open: function (c) {
                var e;
                if (!1 === c.isObj) {
                    b.items = c.items.toArray(), b.index = 0;
                    var g, h = c.items;
                    for (e = 0; e < h.length; e++)
                        if (g = h[e], g.parsed && (g = g.el[0]), g === c.el[0]) {
                            b.index = e;
                            break
                        }
                } else b.items = a.isArray(c.items) ? c.items : [c.items], b.index = c.index || 0; if (b.isOpen) return void b.updateItemHTML();
                b.types = [], f = "", c.mainEl && c.mainEl.length ? b.ev = c.mainEl.eq(0) : b.ev = d, c.key ? (b.popupsCache[c.key] || (b.popupsCache[c.key] = {}), b.currTemplate = b.popupsCache[c.key]) : b.currTemplate = {}, b.st = a.extend(!0, {}, a.magnificPopup.defaults, c), b.fixedContentPos = "auto" === b.st.fixedContentPos ? !b.probablyMobile : b.st.fixedContentPos, b.st.modal && (b.st.closeOnContentClick = !1, b.st.closeOnBgClick = !1, b.st.showCloseBtn = !1, b.st.enableEscapeKey = !1), b.bgOverlay || (b.bgOverlay = x("bg").on("click" + p, function () {
                    b.close()
                }), b.wrap = x("wrap").attr("tabindex", -1).on("click" + p, function (a) {
                    b._checkIfClose(a.target) && b.close()
                }), b.container = x("container", b.wrap)), b.contentContainer = x("content"), b.st.preloader && (b.preloader = x("preloader", b.container, b.st.tLoading));
                var i = a.magnificPopup.modules;
                for (e = 0; e < i.length; e++) {
                    var j = i[e];
                    j = j.charAt(0).toUpperCase() + j.slice(1), b["init" + j].call(b)
                }
                y("BeforeOpen"), b.st.showCloseBtn && (b.st.closeBtnInside ? (w(l, function (a, b, c, d) {
                    c.close_replaceWith = z(d.type)
                }), f += " mfp-close-btn-in") : b.wrap.append(z())), b.st.alignTop && (f += " mfp-align-top"), b.fixedContentPos ? b.wrap.css({
                    overflow: b.st.overflowY,
                    overflowX: "hidden",
                    overflowY: b.st.overflowY
                }) : b.wrap.css({
                    top: v.scrollTop(),
                    position: "absolute"
                }), (!1 === b.st.fixedBgPos || "auto" === b.st.fixedBgPos && !b.fixedContentPos) && b.bgOverlay.css({
                    height: d.height(),
                    position: "absolute"
                }), b.st.enableEscapeKey && d.on("keyup" + p, function (a) {
                    27 === a.keyCode && b.close()
                }), v.on("resize" + p, function () {
                    b.updateSize()
                }), b.st.closeOnContentClick || (f += " mfp-auto-cursor"), f && b.wrap.addClass(f);
                var k = b.wH = v.height(),
                    n = {};
                if (b.fixedContentPos && b._hasScrollBar(k)) {
                    var o = b._getScrollbarSize();
                    o && (n.marginRight = o)
                }
                b.fixedContentPos && (b.isIE7 ? a("body, html").css("overflow", "hidden") : n.overflow = "hidden");
                var r = b.st.mainClass;
                return b.isIE7 && (r += " mfp-ie7"), r && b._addClassToMFP(r), b.updateItemHTML(), y("BuildControls"), a("html").css(n), b.bgOverlay.add(b.wrap).prependTo(b.st.prependTo || a(document.body)), b._lastFocusedEl = document.activeElement, setTimeout(function () {
                    b.content ? (b._addClassToMFP(q), b._setFocus()) : b.bgOverlay.addClass(q), d.on("focusin" + p, b._onFocusIn)
                }, 16), b.isOpen = !0, b.updateSize(k), y(m), c
            },
            close: function () {
                b.isOpen && (y(i), b.isOpen = !1, b.st.removalDelay && !b.isLowIE && b.supportsTransition ? (b._addClassToMFP(r), setTimeout(function () {
                    b._close()
                }, b.st.removalDelay)) : b._close())
            },
            _close: function () {
                y(h);
                var c = r + " " + q + " ";
                if (b.bgOverlay.detach(), b.wrap.detach(), b.container.empty(), b.st.mainClass && (c += b.st.mainClass + " "), b._removeClassFromMFP(c), b.fixedContentPos) {
                    var e = {
                        marginRight: ""
                    };
                    b.isIE7 ? a("body, html").css("overflow", "") : e.overflow = "", a("html").css(e)
                }
                d.off("keyup" + p + " focusin" + p), b.ev.off(p), b.wrap.attr("class", "mfp-wrap").removeAttr("style"), b.bgOverlay.attr("class", "mfp-bg"), b.container.attr("class", "mfp-container"), b.st.showCloseBtn && (!b.st.closeBtnInside || !0 === b.currTemplate[b.currItem.type]) && b.currTemplate.closeBtn && b.currTemplate.closeBtn.detach(), b.st.autoFocusLast && b._lastFocusedEl && a(b._lastFocusedEl).focus(), b.currItem = null, b.content = null, b.currTemplate = null, b.prevHeight = 0, y(j)
            },
            updateSize: function (a) {
                if (b.isIOS) {
                    var c = document.documentElement.clientWidth / window.innerWidth,
                        d = window.innerHeight * c;
                    b.wrap.css("height", d), b.wH = d
                } else b.wH = a || v.height();
                b.fixedContentPos || b.wrap.css("height", b.wH), y("Resize")
            },
            updateItemHTML: function () {
                var c = b.items[b.index];
                b.contentContainer.detach(), b.content && b.content.detach(), c.parsed || (c = b.parseEl(b.index));
                var d = c.type;
                if (y("BeforeChange", [b.currItem ? b.currItem.type : "", d]), b.currItem = c, !b.currTemplate[d]) {
                    var f = !!b.st[d] && b.st[d].markup;
                    y("FirstMarkupParse", f), b.currTemplate[d] = !f || a(f)
                }
                e && e !== c.type && b.container.removeClass("mfp-" + e + "-holder");
                var g = b["get" + d.charAt(0).toUpperCase() + d.slice(1)](c, b.currTemplate[d]);
                b.appendContent(g, d), c.preloaded = !0, y(n, c), e = c.type, b.container.prepend(b.contentContainer), y("AfterChange")
            },
            appendContent: function (a, c) {
                b.content = a, a ? b.st.showCloseBtn && b.st.closeBtnInside && !0 === b.currTemplate[c] ? b.content.find(".mfp-close").length || b.content.append(z()) : b.content = a : b.content = "", y(k), b.container.addClass("mfp-" + c + "-holder"), b.contentContainer.append(b.content)
            },
            parseEl: function (c) {
                var d, e = b.items[c];
                if (e.tagName ? e = {
                    el: a(e)
                } : (d = e.type, e = {
                    data: e,
                    src: e.src
                }), e.el) {
                    for (var f = b.types, g = 0; g < f.length; g++)
                        if (e.el.hasClass("mfp-" + f[g])) {
                            d = f[g];
                            break
                        }
                    e.src = e.el.attr("data-mfp-src"), e.src || (e.src = e.el.attr("href"))
                }
                return e.type = d || b.st.type || "inline", e.index = c, e.parsed = !0, b.items[c] = e, y("ElementParse", e), b.items[c]
            },
            addGroup: function (a, c) {
                var d = function (d) {
                    d.mfpEl = this, b._openClick(d, a, c)
                };
                c || (c = {});
                var e = "click.magnificPopup";
                c.mainEl = a, c.items ? (c.isObj = !0, a.off(e).on(e, d)) : (c.isObj = !1, c.delegate ? a.off(e).on(e, c.delegate, d) : (c.items = a, a.off(e).on(e, d)))
            },
            _openClick: function (c, d, e) {
                if ((void 0 !== e.midClick ? e.midClick : a.magnificPopup.defaults.midClick) || !(2 === c.which || c.ctrlKey || c.metaKey || c.altKey || c.shiftKey)) {
                    var f = void 0 !== e.disableOn ? e.disableOn : a.magnificPopup.defaults.disableOn;
                    if (f)
                        if (a.isFunction(f)) {
                            if (!f.call(b)) return !0
                        } else if (v.width() < f) return !0;
                    c.type && (c.preventDefault(), b.isOpen && c.stopPropagation()), e.el = a(c.mfpEl), e.delegate && (e.items = d.find(e.delegate)), b.open(e)
                }
            },
            updateStatus: function (a, d) {
                if (b.preloader) {
                    c !== a && b.container.removeClass("mfp-s-" + c), !d && "loading" === a && (d = b.st.tLoading);
                    var e = {
                        status: a,
                        text: d
                    };
                    y("UpdateStatus", e), a = e.status, d = e.text, b.preloader.html(d), b.preloader.find("a").on("click", function (a) {
                        a.stopImmediatePropagation()
                    }), b.container.addClass("mfp-s-" + a), c = a
                }
            },
            _checkIfClose: function (c) {
                if (!a(c).hasClass(s)) {
                    var d = b.st.closeOnContentClick,
                        e = b.st.closeOnBgClick;
                    if (d && e) return !0;
                    if (!b.content || a(c).hasClass("mfp-close") || b.preloader && c === b.preloader[0]) return !0;
                    if (c === b.content[0] || a.contains(b.content[0], c)) {
                        if (d) return !0
                    } else if (e && a.contains(document, c)) return !0;
                    return !1
                }
            },
            _addClassToMFP: function (a) {
                b.bgOverlay.addClass(a), b.wrap.addClass(a)
            },
            _removeClassFromMFP: function (a) {
                this.bgOverlay.removeClass(a), b.wrap.removeClass(a)
            },
            _hasScrollBar: function (a) {
                return (b.isIE7 ? d.height() : document.body.scrollHeight) > (a || v.height())
            },
            _setFocus: function () {
                (b.st.focus ? b.content.find(b.st.focus).eq(0) : b.wrap).focus()
            },
            _onFocusIn: function (c) {
                if (c.target !== b.wrap[0] && !a.contains(b.wrap[0], c.target)) return b._setFocus(), !1
            },
            _parseMarkup: function (b, c, d) {
                var e;
                d.data && (c = a.extend(d.data, c)), y(l, [b, c, d]), a.each(c, function (c, d) {
                    if (void 0 === d || !1 === d) return !0;
                    if (e = c.split("_"), e.length > 1) {
                        var f = b.find(p + "-" + e[0]);
                        if (f.length > 0) {
                            var g = e[1];
                            "replaceWith" === g ? f[0] !== d[0] && f.replaceWith(d) : "img" === g ? f.is("img") ? f.attr("src", d) : f.replaceWith(a("<img>").attr("src", d).attr("class", f.attr("class"))) : f.attr(e[1], d)
                        }
                    } else b.find(p + "-" + c).html(d)
                })
            },
            _getScrollbarSize: function () {
                if (void 0 === b.scrollbarSize) {
                    var a = document.createElement("div");
                    a.style.cssText = "width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;", document.body.appendChild(a), b.scrollbarSize = a.offsetWidth - a.clientWidth, document.body.removeChild(a)
                }
                return b.scrollbarSize
            }
        }, a.magnificPopup = {
            instance: null,
            proto: t.prototype,
            modules: [],
            open: function (b, c) {
                return A(), b = b ? a.extend(!0, {}, b) : {}, b.isObj = !0, b.index = c || 0, this.instance.open(b)
            },
            close: function () {
                return a.magnificPopup.instance && a.magnificPopup.instance.close()
            },
            registerModule: function (b, c) {
                c.options && (a.magnificPopup.defaults[b] = c.options), a.extend(this.proto, c.proto), this.modules.push(b)
            },
            defaults: {
                disableOn: 0,
                key: null,
                midClick: !1,
                mainClass: "",
                preloader: !0,
                focus: "",
                closeOnContentClick: !1,
                closeOnBgClick: !0,
                closeBtnInside: !0,
                showCloseBtn: !0,
                enableEscapeKey: !0,
                modal: !1,
                alignTop: !1,
                removalDelay: 0,
                prependTo: null,
                fixedContentPos: "auto",
                fixedBgPos: "auto",
                overflowY: "auto",
                closeMarkup: '<button title="%title%" type="button" class="mfp-close">&#215;</button>',
                tClose: "Close (Esc)",
                tLoading: "Loading...",
                autoFocusLast: !0
            }
        }, a.fn.magnificPopup = function (c) {
            A();
            var d = a(this);
            if ("string" == typeof c)
                if ("open" === c) {
                    var e, f = u ? d.data("magnificPopup") : d[0].magnificPopup,
                        g = parseInt(arguments[1], 10) || 0;
                    f.items ? e = f.items[g] : (e = d, f.delegate && (e = e.find(f.delegate)), e = e.eq(g)), b._openClick({
                        mfpEl: e
                    }, d, f)
                } else b.isOpen && b[c].apply(b, Array.prototype.slice.call(arguments, 1));
            else c = a.extend(!0, {}, c), u ? d.data("magnificPopup", c) : d[0].magnificPopup = c, b.addGroup(d, c);
            return d
        };
        var C, D, E, F = "inline",
            G = function () {
                E && (D.after(E.addClass(C)).detach(), E = null)
            };
        a.magnificPopup.registerModule(F, {
            options: {
                hiddenClass: "hide",
                markup: "",
                tNotFound: "Content not found"
            },
            proto: {
                initInline: function () {
                    b.types.push(F), w(h + "." + F, function () {
                        G()
                    })
                },
                getInline: function (c, d) {
                    if (G(), c.src) {
                        var e = b.st.inline,
                            f = a(c.src);
                        if (f.length) {
                            var g = f[0].parentNode;
                            g && g.tagName && (D || (C = e.hiddenClass, D = x(C), C = "mfp-" + C), E = f.after(D).detach().removeClass(C)), b.updateStatus("ready")
                        } else b.updateStatus("error", e.tNotFound), f = a("<div>");
                        return c.inlineElement = f, f
                    }
                    return b.updateStatus("ready"), b._parseMarkup(d, {}, c), d
                }
            }
        });
        var H, I = "ajax",
            J = function () {
                H && a(document.body).removeClass(H)
            },
            K = function () {
                J(), b.req && b.req.abort()
            };
        a.magnificPopup.registerModule(I, {
            options: {
                settings: null,
                cursor: "mfp-ajax-cur",
                tError: '<a href="%url%">The content</a> could not be loaded.'
            },
            proto: {
                initAjax: function () {
                    b.types.push(I), H = b.st.ajax.cursor, w(h + "." + I, K), w("BeforeChange." + I, K)
                },
                getAjax: function (c) {
                    H && a(document.body).addClass(H), b.updateStatus("loading");
                    var d = a.extend({
                        url: c.src,
                        success: function (d, e, f) {
                            var g = {
                                data: d,
                                xhr: f
                            };
                            y("ParseAjax", g), b.appendContent(a(g.data), I), c.finished = !0, J(), b._setFocus(), setTimeout(function () {
                                b.wrap.addClass(q)
                            }, 16), b.updateStatus("ready"), y("AjaxContentAdded")
                        },
                        error: function () {
                            J(), c.finished = c.loadError = !0, b.updateStatus("error", b.st.ajax.tError.replace("%url%", c.src))
                        }
                    }, b.st.ajax.settings);
                    return b.req = a.ajax(d), ""
                }
            }
        });
        var L, M = function (c) {
            if (c.data && void 0 !== c.data.title) return c.data.title;
            var d = b.st.image.titleSrc;
            if (d) {
                if (a.isFunction(d)) return d.call(b, c);
                if (c.el) return c.el.attr(d) || ""
            }
            return ""
        };
        a.magnificPopup.registerModule("image", {
            options: {
                markup: '<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',
                cursor: "mfp-zoom-out-cur",
                titleSrc: "title",
                verticalFit: !0,
                tError: '<a href="%url%">The image</a> could not be loaded.'
            },
            proto: {
                initImage: function () {
                    var c = b.st.image,
                        d = ".image";
                    b.types.push("image"), w(m + d, function () {
                        "image" === b.currItem.type && c.cursor && a(document.body).addClass(c.cursor)
                    }), w(h + d, function () {
                        c.cursor && a(document.body).removeClass(c.cursor), v.off("resize" + p)
                    }), w("Resize" + d, b.resizeImage), b.isLowIE && w("AfterChange", b.resizeImage)
                },
                resizeImage: function () {
                    var a = b.currItem;
                    if (a && a.img && b.st.image.verticalFit) {
                        var c = 0;
                        b.isLowIE && (c = parseInt(a.img.css("padding-top"), 10) + parseInt(a.img.css("padding-bottom"), 10)), a.img.css("max-height", b.wH - c)
                    }
                },
                _onImageHasSize: function (a) {
                    a.img && (a.hasSize = !0, L && clearInterval(L), a.isCheckingImgSize = !1, y("ImageHasSize", a), a.imgHidden && (b.content && b.content.removeClass("mfp-loading"), a.imgHidden = !1))
                },
                findImageSize: function (a) {
                    var c = 0,
                        d = a.img[0],
                        e = function (f) {
                            L && clearInterval(L), L = setInterval(function () {
                                if (d.naturalWidth > 0) return void b._onImageHasSize(a);
                                c > 200 && clearInterval(L), c++, 3 === c ? e(10) : 40 === c ? e(50) : 100 === c && e(500)
                            }, f)
                        };
                    e(1)
                },
                getImage: function (c, d) {
                    var e = 0,
                        f = function () {
                            c && (c.img[0].complete ? (c.img.off(".mfploader"), c === b.currItem && (b._onImageHasSize(c), b.updateStatus("ready")), c.hasSize = !0, c.loaded = !0, y("ImageLoadComplete")) : (e++, e < 200 ? setTimeout(f, 100) : g()))
                        },
                        g = function () {
                            c && (c.img.off(".mfploader"), c === b.currItem && (b._onImageHasSize(c), b.updateStatus("error", h.tError.replace("%url%", c.src))), c.hasSize = !0, c.loaded = !0, c.loadError = !0)
                        },
                        h = b.st.image,
                        i = d.find(".mfp-img");
                    if (i.length) {
                        var j = document.createElement("img");
                        j.className = "mfp-img", c.el && c.el.find("img").length && (j.alt = c.el.find("img").attr("alt")), c.img = a(j).on("load.mfploader", f).on("error.mfploader", g), j.src = c.src, i.is("img") && (c.img = c.img.clone()), j = c.img[0], j.naturalWidth > 0 ? c.hasSize = !0 : j.width || (c.hasSize = !1)
                    }
                    return b._parseMarkup(d, {
                        title: M(c),
                        img_replaceWith: c.img
                    }, c), b.resizeImage(), c.hasSize ? (L && clearInterval(L), c.loadError ? (d.addClass("mfp-loading"), b.updateStatus("error", h.tError.replace("%url%", c.src))) : (d.removeClass("mfp-loading"), b.updateStatus("ready")), d) : (b.updateStatus("loading"), c.loading = !0, c.hasSize || (c.imgHidden = !0, d.addClass("mfp-loading"), b.findImageSize(c)), d)
                }
            }
        });
        var N, O = function () {
            return void 0 === N && (N = void 0 !== document.createElement("p").style.MozTransform), N
        };
        a.magnificPopup.registerModule("zoom", {
            options: {
                enabled: !1,
                easing: "ease-in-out",
                duration: 300,
                opener: function (a) {
                    return a.is("img") ? a : a.find("img")
                }
            },
            proto: {
                initZoom: function () {
                    var a, c = b.st.zoom,
                        d = ".zoom";
                    if (c.enabled && b.supportsTransition) {
                        var e, f, g = c.duration,
                            j = function (a) {
                                var b = a.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image"),
                                    d = "all " + c.duration / 1e3 + "s " + c.easing,
                                    e = {
                                        position: "fixed",
                                        zIndex: 9999,
                                        left: 0,
                                        top: 0,
                                        "-webkit-backface-visibility": "hidden"
                                    },
                                    f = "transition";
                                return e["-webkit-" + f] = e["-moz-" + f] = e["-o-" + f] = e[f] = d, b.css(e), b
                            },
                            k = function () {
                                b.content.css("visibility", "visible")
                            };
                        w("BuildControls" + d, function () {
                            if (b._allowZoom()) {
                                if (clearTimeout(e), b.content.css("visibility", "hidden"), a = b._getItemToZoom(), !a) return void k();
                                f = j(a), f.css(b._getOffset()), b.wrap.append(f), e = setTimeout(function () {
                                    f.css(b._getOffset(!0)), e = setTimeout(function () {
                                        k(), setTimeout(function () {
                                            f.remove(), a = f = null, y("ZoomAnimationEnded")
                                        }, 16)
                                    }, g)
                                }, 16)
                            }
                        }), w(i + d, function () {
                            if (b._allowZoom()) {
                                if (clearTimeout(e), b.st.removalDelay = g, !a) {
                                    if (!(a = b._getItemToZoom())) return;
                                    f = j(a)
                                }
                                f.css(b._getOffset(!0)), b.wrap.append(f), b.content.css("visibility", "hidden"), setTimeout(function () {
                                    f.css(b._getOffset())
                                }, 16)
                            }
                        }), w(h + d, function () {
                            b._allowZoom() && (k(), f && f.remove(), a = null)
                        })
                    }
                },
                _allowZoom: function () {
                    return "image" === b.currItem.type
                },
                _getItemToZoom: function () {
                    return !!b.currItem.hasSize && b.currItem.img
                },
                _getOffset: function (c) {
                    var d;
                    d = c ? b.currItem.img : b.st.zoom.opener(b.currItem.el || b.currItem);
                    var e = d.offset(),
                        f = parseInt(d.css("padding-top"), 10),
                        g = parseInt(d.css("padding-bottom"), 10);
                    e.top -= a(window).scrollTop() - f;
                    var h = {
                        width: d.width(),
                        height: (u ? d.innerHeight() : d[0].offsetHeight) - g - f
                    };
                    return O() ? h["-moz-transform"] = h.transform = "translate(" + e.left + "px," + e.top + "px)" : (h.left = e.left, h.top = e.top), h
                }
            }
        });
        var P = "iframe",
            Q = "//about:blank",
            R = function (a) {
                if (b.currTemplate[P]) {
                    var c = b.currTemplate[P].find("iframe");
                    c.length && (a || (c[0].src = Q), b.isIE8 && c.css("display", a ? "block" : "none"))
                }
            };
        a.magnificPopup.registerModule(P, {
            options: {
                markup: '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',
                srcAction: "iframe_src",
                patterns: {
                    youtube: {
                        index: "youtube.com",
                        id: "v=",
                        src: "//www.youtube.com/embed/%id%?autoplay=1"
                    },
                    vimeo: {
                        index: "vimeo.com/",
                        id: "/",
                        src: "//player.vimeo.com/video/%id%?autoplay=1"
                    },
                    gmaps: {
                        index: "//maps.google.",
                        src: "%id%&output=embed"
                    }
                }
            },
            proto: {
                initIframe: function () {
                    b.types.push(P), w("BeforeChange", function (a, b, c) {
                        b !== c && (b === P ? R() : c === P && R(!0))
                    }), w(h + "." + P, function () {
                        R()
                    })
                },
                getIframe: function (c, d) {
                    var e = c.src,
                        f = b.st.iframe;
                    a.each(f.patterns, function () {
                        if (e.indexOf(this.index) > -1) return this.id && (e = "string" == typeof this.id ? e.substr(e.lastIndexOf(this.id) + this.id.length, e.length) : this.id.call(this, e)), e = this.src.replace("%id%", e), !1
                    });
                    var g = {};
                    return f.srcAction && (g[f.srcAction] = e), b._parseMarkup(d, g, c), b.updateStatus("ready"), d
                }
            }
        });
        var S = function (a) {
                var c = b.items.length;
                return a > c - 1 ? a - c : a < 0 ? c + a : a
            },
            T = function (a, b, c) {
                return a.replace(/%curr%/gi, b + 1).replace(/%total%/gi, c)
            };
        a.magnificPopup.registerModule("gallery", {
            options: {
                enabled: !1,
                arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
                preload: [0, 2],
                navigateByImgClick: !0,
                arrows: !0,
                tPrev: "Previous (Left arrow key)",
                tNext: "Next (Right arrow key)",
                tCounter: "%curr% of %total%"
            },
            proto: {
                initGallery: function () {
                    var c = b.st.gallery,
                        e = ".mfp-gallery";
                    if (b.direction = !0, !c || !c.enabled) return !1;
                    f += " mfp-gallery", w(m + e, function () {
                        c.navigateByImgClick && b.wrap.on("click" + e, ".mfp-img", function () {
                            if (b.items.length > 1) return b.next(), !1
                        }), d.on("keydown" + e, function (a) {
                            37 === a.keyCode ? b.prev() : 39 === a.keyCode && b.next()
                        })
                    }), w("UpdateStatus" + e, function (a, c) {
                        c.text && (c.text = T(c.text, b.currItem.index, b.items.length))
                    }), w(l + e, function (a, d, e, f) {
                        var g = b.items.length;
                        e.counter = g > 1 ? T(c.tCounter, f.index, g) : ""
                    }), w("BuildControls" + e, function () {
                        if (b.items.length > 1 && c.arrows && !b.arrowLeft) {
                            var d = c.arrowMarkup,
                                e = b.arrowLeft = a(d.replace(/%title%/gi, c.tPrev).replace(/%dir%/gi, "left")).addClass(s),
                                f = b.arrowRight = a(d.replace(/%title%/gi, c.tNext).replace(/%dir%/gi, "right")).addClass(s);
                            e.click(function () {
                                b.prev()
                            }), f.click(function () {
                                b.next()
                            }), b.container.append(e.add(f))
                        }
                    }), w(n + e, function () {
                        b._preloadTimeout && clearTimeout(b._preloadTimeout), b._preloadTimeout = setTimeout(function () {
                            b.preloadNearbyImages(), b._preloadTimeout = null
                        }, 16)
                    }), w(h + e, function () {
                        d.off(e), b.wrap.off("click" + e), b.arrowRight = b.arrowLeft = null
                    })
                },
                next: function () {
                    b.direction = !0, b.index = S(b.index + 1), b.updateItemHTML()
                },
                prev: function () {
                    b.direction = !1, b.index = S(b.index - 1), b.updateItemHTML()
                },
                goTo: function (a) {
                    b.direction = a >= b.index, b.index = a, b.updateItemHTML()
                },
                preloadNearbyImages: function () {
                    var a, c = b.st.gallery.preload,
                        d = Math.min(c[0], b.items.length),
                        e = Math.min(c[1], b.items.length);
                    for (a = 1; a <= (b.direction ? e : d); a++) b._preloadItem(b.index + a);
                    for (a = 1; a <= (b.direction ? d : e); a++) b._preloadItem(b.index - a)
                },
                _preloadItem: function (c) {
                    if (c = S(c), !b.items[c].preloaded) {
                        var d = b.items[c];
                        d.parsed || (d = b.parseEl(c)), y("LazyLoad", d), "image" === d.type && (d.img = a('<img class="mfp-img" />').on("load.mfploader", function () {
                            d.hasSize = !0
                        }).on("error.mfploader", function () {
                            d.hasSize = !0, d.loadError = !0, y("LazyLoadError", d)
                        }).attr("src", d.src)), d.preloaded = !0
                    }
                }
            }
        });
        var U = "retina";
        a.magnificPopup.registerModule(U, {
            options: {
                replaceSrc: function (a) {
                    return a.src.replace(/\.\w+$/, function (a) {
                        return "@2x" + a
                    })
                },
                ratio: 1
            },
            proto: {
                initRetina: function () {
                    if (window.devicePixelRatio > 1) {
                        var a = b.st.retina,
                            c = a.ratio;
                        (c = isNaN(c) ? c() : c) > 1 && (w("ImageHasSize." + U, function (a, b) {
                            b.img.css({
                                "max-width": b.img[0].naturalWidth / c,
                                width: "100%"
                            })
                        }), w("ElementParse." + U, function (b, d) {
                            d.src = a.replaceSrc(d, c)
                        }))
                    }
                }
            }
        }), A()
    });