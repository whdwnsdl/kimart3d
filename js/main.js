/*! Shapespark - v1.0.0 - 2020-12-21
 * https://www.shapespark.com/
 * Copyright (c) 2020; */
! function (a, b, c, d) {
    "use strict";

    function e() {
        var d = b.navigator.language || b.navigator.userLanguage;
        d && 0 == d.indexOf("ja") && (a("#nav-jp").show(), -1 == c.cookie.indexOf("banner-jp=hide") && (a("#banner-jp-wrapper").show(), a("#banner-jp-stay").on("click touch", function (b) {
            c.cookie = "banner-jp=hide; max-age=2592000; samesite=strict", a("#banner-jp").hide(), b.preventDefault()
        })))
    }

    function f() {
        a(".testimonials").animate({}, function () {
            a(this).slick({
                arrows: !0,
                dots: !1,
                infinite: !0,
                slidesToShow: 1,
                adaptiveHeight: !0,
                prevArrow: '<button type="button" class="slick-prev"><i class="icon-left"></i></button>',
                nextArrow: '<button type="button" class="slick-next"><i class="icon-right"></i></button>',
                responsive: [{
                    breakpoint: 576,
                    settings: {
                        arrows: !1,
                        dots: !0
                    }
                }]
            })
        })
    }

    function g() {
        a(".portfolio-slider").animate({}, function () {
            var b = a(this);
            b.slick({
                arrows: !1,
                centerMode: !0,
                centerPadding: "390px",
                slidesToShow: 2,
                swipe: !0,
                swipeToSlide: !0,
                responsive: [{
                    breakpoint: 1681,
                    settings: {
                        centerMode: !0,
                        centerPadding: "260px",
                        slidesToShow: 2
                    }
                }, {
                    breakpoint: 1367,
                    settings: {
                        centerMode: !0,
                        centerPadding: "110px",
                        slidesToShow: 2
                    }
                }, {
                    breakpoint: 1281,
                    settings: {
                        centerMode: !0,
                        centerPadding: "70px",
                        slidesToShow: 2
                    }
                }, {
                    breakpoint: 992,
                    settings: {
                        centerMode: !0,
                        centerPadding: "100px",
                        slidesToShow: 2
                    }
                }, {
                    breakpoint: 768,
                    settings: {
                        centerMode: !0,
                        centerPadding: "50px",
                        slidesToShow: 2
                    }
                }, {
                    breakpoint: 576,
                    settings: {
                        centerMode: !0,
                        centerPadding: "20px",
                        slidesToShow: 1
                    }
                }]
            }), a(".portfolio-slider-nav .slick-prev").on("click touch", function () {
                b.slick("slickPrev")
            }), a(".portfolio-slider-nav .slick-next").on("click touch", function () {
                b.slick("slickNext")
            })
        })
    }

    function h() {
        0 !== a(".portfolio-item--thumbs").length && (a(".portfolio-item--content").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: !0,
            prevArrow: '<button type="button" class="slick-prev"><i class="icon-left"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="icon-right"></i></button>',
            fade: !0,
            asNavFor: ".portfolio-item--thumbs",
            responsive: [{
                breakpoint: 576,
                settings: {
                    arrows: !1
                }
            }]
        }), a(".portfolio-item--thumbs").slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            asNavFor: ".portfolio-item--content",
            centerMode: !1,
            dots: !1,
            arrows: !1,
            focusOnSelect: !0,
            variableWidth: !0,
            infinite: !1,
            responsive: [{
                breakpoint: 576,
                settings: {
                    centerMode: !0,
                    centerPadding: "15px",
                    slidesToShow: 3,
                    arrows: !1,
                    infinite: !0
                }
            }]
        }).on("afterChange", function (b, c) {
            "iframe-3d" == c.$slides[c.currentSlide].firstChild.children[0].className ? a(".see-in-3d").hide() : a(".see-in-3d").show()
        }), a(".portfolio-item--footer").on("click touch", ".btn", function (b) {
            b.preventDefault();
            var c = a(".portfolio-item--thumbs img").length - 1;
            a(".portfolio-item--thumbs").slick("slickGoTo", c)
        }), a(b).on("resize orientationchange", function () {
            var b = a(".portfolio-item--thumbs").slick("slickCurrentSlide");
            a(".portfolio-item--content").slick("resize"), a(".portfolio-item--thumbs").slick("resize"), setTimeout(function () {
                a(".portfolio-item--thumbs").slick("slickGoTo", b)
            }, 900)
        }))
    }

    function i(b) {
        a.magnificPopup.open({
            items: {
                src: b
            },
            type: "ajax",
            tLoading: "",
            fixedContentPos: !1,
            showCloseBtn: !1,
            callbacks: {
                ajaxContentAdded: function () {
                    h(), a(".portfolio-item--header .portfolio-navigation").on("click touch", ".mfp-close", function (b) {
                        b.preventDefault(), a.magnificPopup.close()
                    })
                },
                beforeOpen: function () {
                    a("body").addClass("overflow-hidden"), a("html").addClass("overflow-hidden")
                },
                close: function () {
                    a("body").removeClass("overflow-hidden"), a("html").removeClass("overflow-hidden")
                }
            }
        })
    }

    function j(b) {
        a.magnificPopup.open({
            tLoading: "",
            showCloseBtn: !1,
            fixedContentPos: !1,
            items: {
                src: b
            },
            type: "ajax",
            ajax: {
                settings: {
                    type: "html"
                }
            }
        })
    }
    objectFitImages(), a(c).ready(function () {
        e(), f(), g();
        var b = 0;
        a(c).mousedown(function () {
            b = 0, a(c).mousemove(function () {
                b = 1
            })
        }), a(c).on("click touch", ".portfolio-item .portfolio-lightbox", function (c) {
            c.preventDefault(), 0 == b && i(a(this).attr("data-href"))
        }), a(c).on("click touch", ".portfolio-navigation .portfolio-lightbox", function (b) {
            b.preventDefault(), j(a(this).attr("data-href"))
        })
    }), a(b).scroll(function () {}), a(b).load(function () {
        a(b).trigger("scroll").trigger("resize")
    }), a(b).on("resize orientationchange", function () {})
}(jQuery, window, document);